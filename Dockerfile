####################################################################################################
#
# Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
# The following Code is licensed under the GNU General Public License (GPL) v3.0.
# https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
#
# Corresponding Person:
#     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
#
# Contributors:
#     Leif-Kristof Schultz <contact@lui-studio.net>
#     Frederic Bauer
#     Janne Wernecken
#
####################################################################################################

# arguments
ARG BASE_IMAGE_VERSION=14.2.0

# stage 1: build
FROM trion/ng-cli:${BASE_IMAGE_VERSION} AS builder
WORKDIR /app

COPY ./ ./

RUN yarn install
RUN yarn build --configuration=production --base-href "./" --deploy-url "./"

# stage 2: runtime
FROM nginx:1.23.2-alpine AS runtime

COPY --from=builder /app/dist/trj-viewer-angular /usr/share/nginx/html
COPY .gitlab/ci-cd/assets/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
