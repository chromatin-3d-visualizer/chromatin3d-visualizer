# Chromatin 3D Visualizer

The _Chromatin 3D Visualizer_ is a web application for viewing chromatin chains in a 3D space in the browser. A chromatin chain is a biological structure formed by DNA and histones. The application allows viewing chromatin chains (with Cohesin/CTCF), navigating/moving around the chain, viewing the chain as a PBC container, coloring single or all histones and much more!

This project is a part of [Bioinformatics, University of Applied Sciences Stralsund](https://bioinformatics.hochschule-stralsund.de/).

<br />
<br />

## Try It Out!

This web application can be used in different ways.

A detailed help for the use of the application and the scope of functions can be found [here](https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/wikis/howto/README).

### Live Demo (Recommended)

An online demonstration is directly available, which allows using the application without installing any other software. You can find a stable version at:
https://wedemann-group.gitlab.io/chromatin3d-visualizer/

> **Note:** There are no usage restrictions, since everything is calculated and stored in the user's browser!

<br />

<!--
### Offline Application (OPTIONAL)

**Optional:** If desired, the application can also be downloaded. The advantage of downloading is that no internet connection is required to run the application. However, there are limitations with the loading bars, which means that the loading bars are not displayed correctly and the application appears to "freeze" more often, although it is working in the background. The application can be downloaded under the ["Releases" on GitLab](https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/releases).
-->

### Docker (OPTIONAL)

Optionally, this application can also be used offline with Docker. The image is based on `nginx:1.23.2-alpine` which makes its total size less than 50 MB. The following command downloads the image and starts a container. Afterwards the application is reachable at http://localhost:1337/.

```cli
docker run --rm -p 1337:80 registry.gitlab.com/wedemann-group/chromatin3d-visualizer:latest
```

A list of all possible tags (versions) can be found in the [container registry of this repository](https://gitlab.com/wedemann-group/chromatin3d-visualizer/container_registry).

> **Note:** Mounting volumes is not necessary since everything is stored locally.

<br />
<br />

## Developer Guide

This project uses **Angular 15** with **Angular Material** as frontend framework, **Yarn** as package manager, **ESLint** and **Stylelint** as linting tools, and **Prettier** as code formatter.
To run the application, it is required that **Node.js** (>= v14, see Angular documentation) and **Yarn** are installed on the development environment machine.

### Development environment (watch on changes):

```shell
# install dependencies via yarn
yarn install

# start angular server (watch)
yarn start

# navigate to http://localhost:1337/
```

### Production environment (static html)

```shell
# install dependencies via yarn
yarn install

yarn build --configuration=production --base-href ./ --deploy-url ./
```

**Note:** Since this project uses WebWorkers, a web server is required to run the application.

### Contributing

While contributing to this project, make sure that ESLint, Stylelint and Prettier are enabled in your IDE.

<br />
<br />

## Contributors

The application is maintained by [Bioinformatics, University of Applied Sciences Stralsund](https://bioinformatics.hochschule-stralsund.de/). The following people have contributed to this project:

- Leif-Kristof Schultz
- Frederic Bauer
- Janne Wernecken

<br />
<br />
 
## Papers

Further information on the subject is presented in the following papers:

- [Michael-Christian Mörl, Tilo Zülske, Robert Schöpflin, Gero Wedemann. Data formats for modelling the spatial structure of chromatin based on experimental positions of nucleosomes[J]. AIMS Biophysics, 2019, 6(3): 83-98. doi: 10.3934/biophy.2019.3.83](https://www.aimspress.com/article/10.3934/biophy.2019.3.83)
- [René Stehr, Nick Kepper, Karsten Rippe, Gero Wedemann. The effect of the internucleosomal interaction on the folding of the chromatin fiber. Biophys. J., 95(8), 3677-3691, 2008. VRML-models](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2553136/)
- [Oliver Müller, Nick Kepper, Robert Schöpflin, Ramona Ettig, Karsten Rippe, Gero Wedemann: Changing chromatin fiber conformation by nucleosome repositioning. Biophysical Journal, 107 (9), 2141-2150, 2014.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4223204/)
- [Katharina Brandstetter, Tilo Zülske, David Hörl,Heinrich Leonhardt,Gero Wedemann, Hartmann Hartz: Differences in nanoscale organization of regulatory active and inactive human chromatin. Biophysical Journal, 2022, Vol 121(6), 977-990](https://pubmed.ncbi.nlm.nih.gov/35150617/)

<br />
<br />

## LICENSE

See the [LICENSE](LICENSE) file for licensing information as it pertains to files in this repository.
