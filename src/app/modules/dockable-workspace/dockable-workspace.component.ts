/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  AfterViewInit,
  Component,
  HostListener,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { ResizedEvent } from 'angular-resize-event';
import { LayoutConfig } from 'golden-layout';
import { SubscribeEvents } from '../../core/classes/subscribe-events';
import { DockableAreaComponent } from '../../shared/components/dockable-area/dockable-area.component';
import { WorkspaceService } from '../../core/services/workspace.service';
import { MenuStripService } from '../../core/services/menu-strip.service';
import { DebugService } from '../../core/services/debug.service';
import { LOCAL_STORAGE_LAYOUT } from '../../shared/local-storage-keys.constants';
import { defaultLayout } from './layouts';
import { AssetsService } from '../../core/services/assets/assets.service';
import { NearestFilter, RepeatWrapping } from 'three';
import {
  BEAD_TEXTURE_REPEAT,
  THREE_JS_DEFAULT_FONT_NAME_BOLD,
  THREE_JS_DEFAULT_FONT_PATH_BOLD,
  THREE_JS_TEXTURE_NAME_BEAD,
  THREE_JS_TEXTURE_NAME_BTN_XR_CONF_CHANGE,
  THREE_JS_TEXTURE_PATH_BEAD,
  THREE_JS_TEXTURE_PATH_BTN_XR_CONF_CHANGE,
} from '../../core/services/assets/constants';
import { trigger } from '@angular/animations';
import { FADE_ANIMATION } from 'src/app/shared/animations/fade-in-out.animation';

@Component({
  selector: 'trj-dockable-workspace',
  templateUrl: './dockable-workspace.component.html',
  styleUrls: ['./dockable-workspace.component.scss'],
  animations: [trigger('openClose', FADE_ANIMATION)],
})
export class DockableWorkspaceComponent
  extends SubscribeEvents
  implements AfterViewInit, OnDestroy
{
  public defaultLayout: LayoutConfig = defaultLayout;
  public cachedLayout: LayoutConfig | undefined;

  @ViewChild(DockableAreaComponent)
  private _dockingSuite: DockableAreaComponent | undefined;

  constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _menuStripService: MenuStripService,
    private readonly _debugService: DebugService,
    public readonly assetsService: AssetsService
  ) {
    super();

    this.prepareApp();
  }

  @HostListener('window:beforeunload')
  saveLayout(): boolean {
    try {
      localStorage.setItem(
        LOCAL_STORAGE_LAYOUT,
        JSON.stringify(this._dockingSuite?.getLayout())
      );
    } catch (e) {}

    return true;
  }

  ngAfterViewInit(): void {
    this._dockingSuite?.setSize(window.innerWidth, window.innerHeight);
  }

  workspaceSizeChanged(event: ResizedEvent): void {
    this._dockingSuite?.setSize(event.newRect.width, event.newRect.height);
  }

  ngOnDestroy(): void {
    this.saveLayout();
    this.unsubscribeListeners();
  }

  private async prepareApp(): Promise<void> {
    await this.loadAssets();

    this._debugService.afterViewInit();
    this.loadLayout();

    this.subscribes.push(
      this._menuStripService.resetLayout$.subscribe(() => {
        this._dockingSuite?.setLayout(this.defaultLayout);
      })
    );
    this.subscribes.push(
      this._debugService.saveLayout$.subscribe(() => {
        this.saveLayout();
      })
    );
  }

  private async loadAssets(): Promise<void> {
    const promises = [
      this.assetsService.getOrLoadFont(
        THREE_JS_DEFAULT_FONT_PATH_BOLD,
        THREE_JS_DEFAULT_FONT_NAME_BOLD
      ),
      this.assetsService.getOrLoadTexture(
        THREE_JS_TEXTURE_PATH_BEAD,
        THREE_JS_TEXTURE_NAME_BEAD
      ),
      this.assetsService.getOrLoadTexture(
        THREE_JS_TEXTURE_PATH_BTN_XR_CONF_CHANGE,
        THREE_JS_TEXTURE_NAME_BTN_XR_CONF_CHANGE
      ),
    ];

    for (const promise of promises) await promise;

    const text = this.assetsService.getTexture(THREE_JS_TEXTURE_NAME_BEAD);
    text.minFilter = NearestFilter;
    text.magFilter = NearestFilter;
    text.wrapS = text.wrapT = RepeatWrapping;
    text.repeat.set(BEAD_TEXTURE_REPEAT, BEAD_TEXTURE_REPEAT);
  }

  private loadLayout(): void {
    const cachedLayout = localStorage.getItem(LOCAL_STORAGE_LAYOUT);
    if (cachedLayout) {
      try {
        this.cachedLayout = JSON.parse(cachedLayout);
      } catch (e) {
        console.warn('JSON format of the cached layout is invalid!');
      }
    }
  }
}
