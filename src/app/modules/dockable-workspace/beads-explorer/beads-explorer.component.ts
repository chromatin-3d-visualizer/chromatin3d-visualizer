/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { WorkspaceService } from '../../../core/services/workspace.service';
import { BeadEntry } from './models/bead-entry';
import { DEFAULT_COLOR_HISTONE_OCTAMER } from '../../../shared/color.constants';
import ChangeableColorWithConfigColor from '../../../core/services/colors/changeable-color-with-config-color';
import { BeadsExplorerBase } from './beads-explorer-base';
import {
  aOptionalRGBToHexCSSFriendly,
  colorToArgb,
} from '../../../shared/utilities/color.utility';
import { BeadGroupEntry } from './models/bead-group-entry';
import { convert2DArrayTo1D } from '../../../shared/utilities/array.utility';
import { Configuration } from '../../../shared/models/configuration.model';

@Component({
  selector: 'trj-beads-explorer',
  templateUrl: './beads-explorer.component.html',
  styleUrls: ['./beads-explorer.component.scss'],
})
export class BeadsExplorerComponent
  extends BeadsExplorerBase<BeadEntry | BeadGroupEntry>
  implements AfterViewInit, OnDestroy
{
  public readonly displayedColumns: string[] = [
    'select',
    'displayIndex',
    'displayName',
    'color',
  ];
  public readonly dataSource = new MatTableDataSource<
    BeadEntry | BeadGroupEntry
  >([]);
  public readonly selection = new SelectionModel<BeadEntry | BeadGroupEntry>(
    true,
    []
  );

  public selectionColor = DEFAULT_COLOR_HISTONE_OCTAMER;

  @ViewChild(MatPaginator)
  public paginator: MatPaginator | undefined;

  protected set setBeadsFromTrajectoryFunction(
    func: (config: Configuration | undefined) => void
  ) {
    this._setBeadsFromTrajectory = func;
  }

  constructor(
    private readonly workspaceService: WorkspaceService,
    private readonly color: ChangeableColorWithConfigColor
  ) {
    super();

    this.defaultColor = [color.color];

    this.subscribes.push(
      workspaceService.config$.subscribe((config) => {
        if (!config || !workspaceService.trajectory) {
          this.dataSource.data = [];
          return;
        }

        this._setBeadsFromTrajectory(config?.config);
      })
    );

    this.subscribes.push(
      color.color$.subscribe((col) => {
        const oldColor = [...this.defaultColor];
        this.defaultColor = [col];

        const newData = this.dataSource.data;
        for (const data of newData.filter((d) => oldColor.includes(d.color)))
          data.color = col;

        this.dataSource.data = newData;
      })
    );
  }

  static isBeadGroupEntry(
    entry: BeadEntry | BeadGroupEntry
  ): entry is BeadGroupEntry {
    return (<BeadGroupEntry>entry).indices != undefined;
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  ngAfterViewInit(): void {
    // setTimeout fixes NG0100: ExpressionChangedAfterItHasBeenCheckedError
    setTimeout(() => {
      this.afterInit();

      if (
        this.workspaceService.trajectory &&
        this.workspaceService.config?.config
      )
        this._setBeadsFromTrajectory(this.workspaceService.config.config);
    });
  }

  changeSelectionColor(col: string, bead: BeadEntry): void {
    const beads = [...this.selection.selected];
    if (beads.length === 0) beads.push(bead);

    beads.forEach((s) => (s.color = col));
    this.setConfigSpecificColors(
      col,
      convert2DArrayTo1D<number>(
        beads.map((e) =>
          BeadsExplorerComponent.isBeadGroupEntry(e) ? e.indices : [e.index]
        )
      )
    );
  }

  setSelectionColor(bead: BeadEntry): void {
    this.selectionColor = aOptionalRGBToHexCSSFriendly(colorToArgb(bead.color));
  }

  protected setConfigSpecificColors(
    color: string, // eslint-disable-line @typescript-eslint/no-unused-vars
    beadIndices: number[] // eslint-disable-line @typescript-eslint/no-unused-vars
  ): void {
    this.color.setConfigSpecificColors(color, beadIndices);
  }

  private _setBeadsFromTrajectory: (config: Configuration | undefined) => void =
    () => null;
}
