/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component } from '@angular/core';
import { BeadsExplorerComponent } from '../beads-explorer.component';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { ColorsService } from '../../../../core/services/colors/colors.service';
import { convertBeadsToBeadEntriesLinkerDNAVersion } from '../beads-explorer.helper';
import { ViewService } from '../../../../core/services/view/view.service';
import { PbcService } from '../../../../core/services/pbc.service';
import { getLinkerDNASegments } from '../../../../shared/utilities/bead.utility';
import { Configuration } from '../../../../shared/models/configuration.model';

@Component({
  selector: 'trj-beads-explorer-linker-dna',
  templateUrl: '../beads-explorer.component.html',
  styleUrls: [
    '../beads-explorer.component.scss',
    './beads-explorer-linker-dna.component.scss',
  ],
})
export class BeadsExplorerLinkerDnaComponent extends BeadsExplorerComponent {
  constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _colorsService: ColorsService,
    private readonly _viewService: ViewService,
    private readonly _pbcService: PbcService
  ) {
    super(_workspaceService, _colorsService.linkerDNAColors);

    super.setBeadsFromTrajectoryFunction =
      this.setBeadsFromTrajectory.bind(this);
  }

  private setBeadsFromTrajectory(config: Configuration | undefined): void {
    const beads = getLinkerDNASegments(config?.beads ?? [], []);

    this.entries = convertBeadsToBeadEntriesLinkerDNAVersion(
      beads,
      this._colorsService.linkerDNAColors
    );
  }
}
