/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

export const TAB_JSON_NAME_TRAJECTORY_VIEWER = 'trj-viewer';
export const TAB_JSON_NAME_EXPLORER_NUCLEOSOMES = 'bead-explorer-nucleosome';
export const TAB_JSON_NAME_EXPLORER_DNA_HELIX = 'bead-explorer-dna-helix';
export const TAB_JSON_NAME_EXPLORER_LINKER_DNA = 'bead-explorer-linker-dna';
export const TAB_JSON_NAME_EXPLORER_COHESIN = 'bead-explorer-cohesin';
export const TAB_JSON_NAME_VIEW_SETTINGS = 'view-settings';
export const TAB_JSON_NAME_CONFIGURATION_PLAYER = 'configuration-player';
export const TAB_JSON_NAME_PBC = 'pbc';
export const TAB_JSON_NAME_COHESIN_CTCF = 'cohesin';
export const TAB_JSON_NAME_CAMERA_SETTINGS = 'camera-settings';
export const TAB_JSON_NAME_DEBUG_SETTINGS = 'debug-settings';
export const TAB_JSON_NAME_FIBER_INFORMATION = 'fiber-info';
export const TAB_JSON_NAME_TOOL_COLOR_GRADIENT = 'tool-color-gradient';
export const TAB_JSON_NAME_TOOL_DISTANCE_MEASUREMENT =
  'tool-distance-measurement';
export const TAB_JSON_NAME_TOOL_DEFINED_CAMERA_POSITIONS =
  'tool-defined-camera-positions';
export const TAB_JSON_NAME_TOOL_CAMERA_ANIMATOR = 'tool-camera-animator';
export const TAB_JSON_NAME_SELECTION_INFORMATION = 'selection-info';

export const TAB_NAME_TRAJECTORY_VIEWER = 'Viewer';
export const TAB_NAME_EXPLORER_HISTONE_OCTAMER = 'Explorer: Histone Octamer';
export const TAB_NAME_EXPLORER_NUCLEOSOME_DNA_HELIX =
  'Explorer: Nucleosome DNA';
export const TAB_NAME_EXPLORER_LINKER_DNA = 'Explorer: Linker DNA';
export const TAB_NAME_EXPLORER_COHESIN = 'Explorer: Cohesin/CTCF';
export const TAB_NAME_VIEW_SETTINGS = 'View';
export const TAB_NAME_CONFIGURATION_PLAYER = 'Config Player';
export const TAB_NAME_PBC = 'PBC';
export const TAB_NAME_COHESIN_CTCF_INFORMATION = 'Cohesin/CTCF Info';
export const TAB_NAME_NAME_FIBER_INFORMATION = 'Fiber Info';
export const TAB_NAME_NAME_SELECTION_INFORMATION = 'Selection Info';
export const TAB_NAME_CAMERA_SETTINGS = 'Camera Settings';
export const TAB_NAME_DEBUG_SETTINGS = 'Debug Settings';
export const TAB_NAME_TOOL_COLOR_GRADIENT = 'Tool: Color Gradient';
export const TAB_NAME_TOOL_DISTANCE_MEASUREMENT = 'Tool: Distance Measurement';
export const TAB_NAME_TOOL_DEFINED_CAMERA_POSITIONS =
  'Tool: Defined Camera Pos';
export const TAB_NAME_NAME_TOOL_CAMERA_ANIMATOR = 'Tool: Camera Animator';

export const TAB_FULL_NAME_COHESIN_CTCF_INFORMATION =
  'Cohesin / CTCF Information';
export const TAB_FULL_NAME_FIBER_INFORMATION = 'Fiber Information';
export const TAB_FULL_NAME_SELECTION_INFORMATION = 'Selection Information';
export const TAB_FULL_NAME_CONFIGURATION_PLAYER = 'Configuration Player';
export const TAB_FULL_NAME_TOOL_DEFINED_CAMERA_POSITIONS =
  'Tool: Defined Camera Positions';
