/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable, NgZone, OnDestroy } from '@angular/core';
import {
  Camera,
  InstancedMesh,
  Intersection,
  Mesh,
  Object3D,
  Scene,
  Vector3,
  WebGLInfo,
} from 'three';
import { ViewService } from '../../../../core/services/view/view.service';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { ColorsService } from '../../../../core/services/colors/colors.service';
import { DebugService } from '../../../../core/services/debug.service';
import { SceneCameraHelper } from './scene-camera.helper';
import { StatsPanel } from '../objects/stats-panel.object';
import { SceneModule } from '../scene.module';
import InstancedObjectBuilder from '../beads/instanced-object.builder';
import { CohesinService } from '../../../../core/services/cohesin.service';
import { AssetsService } from '../../../../core/services/assets/assets.service';
import { SessionManagerService } from '../../../../core/services/session-mananger/session-manager.service';
import AbstractThreeJsSceneHelper from './abstract-three-js-scene.helper';
import { RaycastService } from '../../../../core/services/raycast.service';
import MultipleMeshBeadBuilder from '../beads/multiple-mesh-bead.builder';
import { ISelectable } from '../beads/selectable.interface';
import { convert2DArrayTo1D } from '../../../../shared/utilities/array.utility';
import { ISelectableGroupMesh } from '../beads/selectable-group-mesh.interface';
import { ISelectableInstancedMesh } from '../beads/selectable-instanced-mesh.interface';
import { DistanceMeasurementService } from '../../../../core/services/distance-measurement.service';
import DistanceMeasurementInfoManager from '../objects/threejs/managers/distance-measurement/distance-measurement-info.manager';
import { ICameraSynchronizer } from '../objects/threejs/managers/camera-synchronizer.interface';
import {
  isCameraSynchronizer,
  isConfigChangedListener,
  isQualityLevelListener,
  isServiceListener,
} from '../objects/threejs/utility';
import { THREE_JS_DEFAULT_FONT_NAME_BOLD } from '../../../../core/services/assets/constants';
import DebugManager from '../objects/threejs/managers/debug/debug.manager';
import { PbcService } from '../../../../core/services/pbc.service';
import { IConfigChangedListener } from '../objects/threejs/managers/config-changed-listener.interface';
import { ParsedConfiguration } from '../../../../shared/parsers/models/parsed-configuration.model';
import PbcManager from '../objects/threejs/managers/pbc/pbc.manager';
import BeadManager from '../objects/threejs/managers/bead/bead.manager';
import { IQualityLevelListener } from '../objects/threejs/quality-level-listener.interface';
import { isSelectable } from '../objects/utility';
import FiberLightsManager from '../objects/threejs/managers/fiber-lights/fiber-lights.manager';
import { IServiceListener } from '../objects/threejs/service-listener.interface';
import AnimationVisualizerManager from '../objects/threejs/managers/animation/animation-visualizer.manager';

@Injectable({
  providedIn: SceneModule,
})
export class SceneHelper
  extends AbstractThreeJsSceneHelper
  implements OnDestroy
{
  private _beadManager: BeadManager | undefined;

  public get rendererInfo(): WebGLInfo | undefined {
    return this.renderer?.info;
  }

  public get sceneCamera(): Camera | undefined {
    return this.camera;
  }

  public constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _colorService: ColorsService,
    private readonly _ngZone: NgZone,
    private readonly _viewService: ViewService,
    private readonly _debugService: DebugService,
    private readonly _sceneCameraHelper: SceneCameraHelper,
    private readonly _statsPanel: StatsPanel,
    private readonly _cohesinService: CohesinService,
    private readonly _assetsService: AssetsService,
    private readonly _sessionService: SessionManagerService,
    private readonly _rayCastService: RaycastService,
    private readonly _distanceMeasurementService: DistanceMeasurementService,
    private readonly _pbcService: PbcService
  ) {
    super(_ngZone);
  }

  public getSelectableObjects(): Object3D[] {
    const result: Object3D[] = [];
    for (const builder of this.getSelectableBuilders()) {
      if (builder instanceof InstancedObjectBuilder) result.push(builder.mesh);
      else if (builder instanceof MultipleMeshBeadBuilder)
        result.push(...builder.meshes);
    }

    return result;
  }

  public toggleSelection(userSelection: Intersection): void {
    if (!(userSelection.object instanceof Mesh)) return;

    const mesh = userSelection.object;

    if (mesh instanceof InstancedMesh) {
      if (userSelection.instanceId == undefined) return;

      this.getBuilderForInstancedMeshSelection(mesh)?.toggleSelection(
        userSelection.instanceId
      );
    } else {
      this.getBuilderForNonInstanceMeshSelection(mesh)?.toggleSelection(mesh);
    }

    this._workspaceService.selectionTexts = convert2DArrayTo1D(
      this.getSelectableBuilders()
        .map((s) => s.getSelectionTexts())
        .filter((s) => s.length > 0)
    );
  }

  public setSelection(intersection: Intersection): void {
    this.clearSelections(false);
    this.toggleSelection(intersection);
  }

  public clearSelections(clearText = true): void {
    this.getSelectableBuilders().forEach((b) => b.clearSelection());
    if (clearText) this._workspaceService.selectionTexts = [];
  }

  public ngOnDestroy(): void {
    this.unsubscribeListeners();

    this.cancelAnimation();
    this.disposeCurrentScene();

    this._statsPanel.destroy();
    this.clearManagers();
    this.clearScene();
  }

  public createScene(canvas: HTMLCanvasElement): void {
    this.initManagers();

    if (this._debugService.debugMode) {
      this._statsPanel.createStats();
    }

    this.scene = super.createDefaultScene(
      canvas,
      window.innerWidth,
      window.innerHeight
    );
    this._sceneCameraHelper.init(this.scene, canvas);

    this.camera = this._sceneCameraHelper.createCamera();
    this.scene.add(this.camera);

    this.scene.add(...this.managers.map((m) => m.getGroup()));

    this.rerenderScene();
  }

  public resize(width: number, height: number): void {
    this._sceneCameraHelper.setAspect(width, height);

    if (!this.renderer) return;
    this.renderer.setSize(width, height);
  }

  public async makeScreen(width: number, height: number): Promise<Blob> {
    return new Promise((resolve, reject) => {
      if (!this.camera || !this.renderer || !this.scene) {
        reject();
        return;
      }

      const oldSize = {
        width: this.renderer.domElement.width,
        height: this.renderer.domElement.height,
      };

      this._sceneCameraHelper.setAspect(width, height);
      this.renderer.setSize(width, height);

      this.renderer.render(this.scene, this.camera);

      this.renderer.domElement.toBlob(
        (blob) => {
          // restore old size
          this._sceneCameraHelper.setAspect(oldSize.width, oldSize.height);
          this.renderer?.setSize(oldSize.width, oldSize.height);

          if (!blob) return;

          // return screenshot
          resolve(blob);
        },
        'image/png',
        100
      );
    });
  }

  public getScene(): Scene | undefined {
    return this.scene;
  }

  public clearDistanceMeasurementLines(): void {
    this.removeManagers(
      this.managers.filter((m) => m instanceof DistanceMeasurementInfoManager)
    );
    this._rayCastService.existsTempLines$$.next(false);
  }

  public addToolDistanceMeasurementLine(
    startPoint: Vector3,
    endPoint: Vector3
  ): void {
    this.addManager(
      new DistanceMeasurementInfoManager(
        this._distanceMeasurementService,
        this._assetsService.getFont(THREE_JS_DEFAULT_FONT_NAME_BOLD),
        startPoint,
        endPoint
      )
    );

    this._rayCastService.existsTempLines$$.next(true);
  }

  public pauseScene(): void {
    this.cancelAnimation();
    this.unsubscribeListeners();
    (<IServiceListener[]>(
      (<unknown>this.managers.filter((m) => isServiceListener(m)))
    )).forEach((m) => m.unsubscribeListeners());
  }

  public resumeScene(): void {
    this.subscribeListeners();
    (<IServiceListener[]>(
      (<unknown>this.managers.filter((m) => isServiceListener(m)))
    )).forEach((m) => m.subscribeListeners());
    this.animate();
  }

  protected subscribeListeners(): void {
    this.subscribes.push(
      this._viewService.dispose$.subscribe(() => {
        this.disposeCurrentScene();
      }),
      this._sessionService.qualityProfile$.subscribe((qualityProfile) => {
        (<IQualityLevelListener[]>(
          (<unknown>this.managers.filter((m) => isQualityLevelListener(m)))
        )).forEach((m) => m.qualityLevelChanged(qualityProfile.level));

        this.handleConfigurationChanged(null); // dispose ALL
        this.rerenderScene(); // recreate the complete scene
      })
    );

    this.subscribes.push(
      this._viewService.rerender$.subscribe(() => {
        // while close/open an open it is possible that the threejs scene doesnt exit in the moment, so ignore the event. the scene will create in "createScene"
        // this condition is not strictly necessary here, since this is checked at the latest when is drawing. However, this routine resolves other routines. Therefore it makes sense to abort as soon as possible.
        this.rerenderScene();
      })
    );

    this.subscribes.push(
      this._rayCastService.clearSelections$.subscribe(() =>
        this.clearSelections()
      ),
      this._distanceMeasurementService.isDistanceMeasurement$.subscribe(
        (val) => {
          if (!val) return;

          this.clearDistanceMeasurementLines();
        }
      ),
      this._rayCastService.clearDistanceMeasurementLines$$.subscribe(() =>
        this.clearDistanceMeasurementLines()
      )
    );

    this.subscribes.push(
      this._colorService.selectionColor.color$.subscribe((color) => {
        this.getSelectableBuilders().forEach((b) => b.setSelectionColor(color));
      })
    );
  }

  protected render(): void {
    this.frameId = requestAnimationFrame(() => {
      this.render();
      this._sceneCameraHelper.render();

      this._statsPanel.stats?.update();
    });

    if (!this.renderer || !this.scene || !this.camera) return;

    this._viewService.cameraRotation$$.next(this.camera.rotation);
    this._viewService.cameraUpVector$$.next(this.camera.up);
    (<ICameraSynchronizer[]>(
      (<unknown>this.managers.filter((m) => isCameraSynchronizer(m)))
    )).forEach((m) => m.cameraUpdated(this.camera!));

    this.renderer.render(this.scene, this.camera);
  }

  private initManagers(): void {
    this._beadManager = new BeadManager(
      this._workspaceService,
      this._colorService,
      this._viewService,
      this._cohesinService,
      this._assetsService,
      this._sessionService
    );

    this.managers.push(
      new FiberLightsManager(this._viewService),
      new DebugManager(
        this._workspaceService,
        this._viewService,
        this._debugService,
        this._pbcService
      ),
      new AnimationVisualizerManager(this._viewService, this._debugService),
      new PbcManager(this._pbcService, this._workspaceService),
      this._beadManager
    );
  }

  private disposeCurrentScene(): void {
    (<IConfigChangedListener[]>(
      (<unknown>this.managers.filter((m) => isConfigChangedListener(m)))
    )).forEach((m) => m.configChanged(null));

    this.clearDistanceMeasurementLines();
  }

  private rerenderScene(): void {
    this.handleConfigurationChanged(this._workspaceService.config);
  }

  private handleConfigurationChanged(config: ParsedConfiguration | null): void {
    console.debug(0, this._workspaceService.renderCount);
    this.clearDistanceMeasurementLines();

    (<IConfigChangedListener[]>(
      (<unknown>this.managers.filter((m) => isConfigChangedListener(m)))
    )).forEach((m) => m.configChanged(config));
  }

  private getBuilderForInstancedMeshSelection(
    selection: InstancedMesh
  ): ISelectableInstancedMesh | undefined {
    if (!this._beadManager) return undefined;

    const instancedMeshBuilders: InstancedObjectBuilder<any, any>[] = [];
    instancedMeshBuilders.push(...this._beadManager.multipleUseBuilders);
    instancedMeshBuilders.push(
      ...(this._beadManager.oneTimeUseBuilders.filter(
        (builder) => builder instanceof InstancedObjectBuilder
      ) as InstancedObjectBuilder<any, any>[])
    );

    return instancedMeshBuilders.find((builder) => builder.mesh === selection);
  }

  private getBuilderForNonInstanceMeshSelection(
    selection: Mesh
  ): ISelectableGroupMesh | undefined {
    if (!this._beadManager) return undefined;

    const multiMeshBeadBuilders: MultipleMeshBeadBuilder<any, any, any, any>[] =
      [
        ...(this._beadManager.oneTimeUseBuilders.filter(
          (builder) => builder instanceof MultipleMeshBeadBuilder
        ) as MultipleMeshBeadBuilder<any, any, any, any>[]),
      ];

    return multiMeshBeadBuilders.find((builder) =>
      builder.meshes.find((mesh) => mesh === selection)
    );
  }

  private getSelectableBuilders(): ISelectable[] {
    if (!this._beadManager) return [];

    return <ISelectable[]>(
      [
        ...this._beadManager.multipleUseBuilders,
        ...this._beadManager.oneTimeUseBuilders,
      ].filter((b) => isSelectable(b))
    );
  }
}
