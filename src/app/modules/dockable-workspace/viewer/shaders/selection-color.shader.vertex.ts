/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { MATERIAL_ATTRIBUTE_NAME_SELECTED } from '../beads/models.variables';

/**
 * The main function with all "includes" was provided by ThreeJS in the GitHub repository. However, this shader had to be slightly adapted
 * to implement the functionality that individual beads can be made invisible (related #147). The changes have been marked with "// ADDED"
 * or "// CHANGED". All unmarked lines are from the ThreeJS GitHub repository.
 * @source https://github.com/mrdoob/three.js/blob/44b8fa7b452dd0d291b9b930fdfc5721cb6ebee9/src/renderers/shaders/ShaderLib/meshphong_vert.glsl.js
 */
export const VERTEX_PHONG_SHADER = `
#define CUSTOM_SHADER_2

varying vec3 vViewPosition;

#include <common>
#include <uv_pars_vertex>
#include <uv2_pars_vertex>
#include <displacementmap_pars_vertex>
#include <envmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <shadowmap_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>

uniform float ${MATERIAL_ATTRIBUTE_NAME_SELECTED}; // ADDED

uniform vec3 selectionColor; // ADDED
uniform vec3 vertexColor; // ADDED

void main() {
    #include <uv_vertex>
    #include <uv2_vertex>
    #include <color_vertex>

    #include <beginnormal_vertex>
    #include <morphnormal_vertex>
    #include <skinbase_vertex>
    #include <skinnormal_vertex>
    #include <defaultnormal_vertex>
    #include <normal_vertex>

    #include <begin_vertex>
    #include <morphtarget_vertex>
    #include <skinning_vertex>
    #include <displacementmap_vertex>
    #include <project_vertex>
    #include <logdepthbuf_vertex>
    #include <clipping_planes_vertex>

    vViewPosition = - mvPosition.xyz;

    #include <worldpos_vertex>
    #include <envmap_vertex>
    #include <shadowmap_vertex>
    #include <fog_vertex>

    if(abs(${MATERIAL_ATTRIBUTE_NAME_SELECTED} - 1.0) <= 0.001) { // ADDED
      vColor = selectionColor; // ADDED
    }
    else {
      vColor = vertexColor;
    }
}
`;
