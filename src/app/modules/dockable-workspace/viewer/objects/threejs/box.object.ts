/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { BoxGeometry, Mesh, MeshStandardMaterial } from 'three';
import { MeshStandardMaterialParameters } from 'three/src/materials/MeshStandardMaterial';
import ThreeJsObject from './three-js.object';

export default class BoxObject extends ThreeJsObject<
  BoxGeometry,
  MeshStandardMaterial
> {
  public readonly geometry: BoxGeometry;
  public readonly material: MeshStandardMaterial;
  public readonly mesh: Mesh;

  public constructor(parameters?: MeshStandardMaterialParameters) {
    super();

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial(parameters);
    this.mesh = this.generateMesh();
  }

  protected generateGeometry(): BoxGeometry {
    return new BoxGeometry(1, 1, 1);
  }

  protected generateMaterial(
    parameters?: MeshStandardMaterialParameters
  ): MeshStandardMaterial {
    return new MeshStandardMaterial({
      color: 0x900000,
      ...parameters,
    });
  }

  protected generateMesh(): Mesh {
    return new Mesh(this.geometry, this.material);
  }
}
