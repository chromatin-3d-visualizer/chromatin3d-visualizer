/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { BoxGeometry, DoubleSide, MeshStandardMaterial } from 'three';
import Manager from '../manager';
import BoxObject from '../../box.object';
import { PbcService } from '../../../../../../../core/services/pbc.service';
import { WorkspaceService } from '../../../../../../../core/services/workspace.service';
import {
  colorToArgb,
  rgbToHex,
} from '../../../../../../../shared/utilities/color.utility';
import {
  DEFAULT_COLOR_MICROSCOPE_SLIDE_BOX,
  DEFAULT_COLOR_PBC_CONTAINER,
} from '../../../../../../../shared/color.constants';
import { IConfigChangedListener } from '../config-changed-listener.interface';
import { ParsedConfiguration } from '../../../../../../../shared/parsers/models/parsed-configuration.model';
import { IServiceListener } from '../../service-listener.interface';

/**
 * Defines a value by which the size of the container is reduced at Y and Z to suppress flickering.
 */
const SUPPRESS_FLICKER_SIZE = 1;

/**
 * Defines a value by which the position of the container is reduced/extended (depending on the index) at X to suppress flickering.
 */
const SUPPRESS_FLICKER_POSITION = 1;

export default class PbcManager
  extends Manager<BoxObject, BoxGeometry, MeshStandardMaterial>
  implements IConfigChangedListener, IServiceListener
{
  private readonly _pbcContainer: BoxObject;
  private readonly _microscopeSlide: BoxObject;

  public constructor(
    private readonly _pbcService: PbcService,
    private readonly _workspaceService: WorkspaceService
  ) {
    super();

    this._pbcContainer = this.generatePBCContainer();
    this._microscopeSlide = this.generateMicroscopeSlide();

    this.setPBCContainerSize(this._pbcService.getContainerSize());
    this.setSizeAndPositionFromMicroscopeSlide();

    this.objects.push(this._pbcContainer, this._microscopeSlide);

    this.subscribeListeners();

    this.initGroupFromObjects();

    this.group.skipExportWhenInvisible = true;
    this.objects.forEach((o) => (o.mesh.skipExportWhenInvisible = true));
  }

  public subscribeListeners(): void {
    this.subscribes.push(
      this._pbcService.pbcContainerVisibility$.subscribe((vis) => {
        this._pbcContainer.mesh.visible = this._pbcService.pbcMode && vis;
      }),
      this._pbcService.pbcContainerColor$.subscribe((color) => {
        this._pbcContainer.changeColorFromString(color);
      }),
      this._pbcService.microscopeSlideEnableState$.subscribe(() =>
        this.setVisibleStateFromMicroscopeSlide()
      ),
      this._pbcService.microscopeSlideBoxVisibility$.subscribe(() =>
        this.setVisibleStateFromMicroscopeSlide()
      ),
      this._pbcService.microscopeSlideBoxColor$.subscribe((color) => {
        this._microscopeSlide.changeColorFromString(color);
      }),
      this._pbcService.microscopeSlideCutSize$.subscribe(() => {
        this.setSizeAndPositionFromMicroscopeSlide();
      }),
      this._pbcService.microscopeSlideIndex$.subscribe(() => {
        this.setSizeAndPositionFromMicroscopeSlide();
      })
    );
  }

  public unsubscribeListeners(): void {
    super.unsubscribeListeners();
  }

  public configChanged(config: ParsedConfiguration | null): void {
    this.setPBCContainerSize(this._pbcService.getContainerSize());
    this.setSizeAndPositionFromMicroscopeSlide();

    const pbcCondition =
      !!config?.config && // -1 if trj is unloaded
      this._pbcService.getContainerSizeFromConfig(config.config) > 0 && // has pbc
      this._pbcService.pbcMode; // pbc is enabled

    this._pbcContainer.mesh.visible =
      pbcCondition && this._pbcService.pbcContainerVisibleState; // pbc container is enabled

    this._microscopeSlide.mesh.visible =
      pbcCondition && // pbc is enabled
      this._pbcService.microscopeSlideEnableState && // microscope is enabled
      this._pbcService.microscopeSlideBoxVisibleState; // microscope container is enabled
  }

  private generatePBCContainer(): BoxObject {
    const colorComponents = colorToArgb(DEFAULT_COLOR_PBC_CONTAINER);
    const obj = new BoxObject({
      transparent: true,
      color: rgbToHex(colorComponents), // remove alpha because threejs doesn't support alpha
      side: DoubleSide,
      depthWrite: true,
      depthTest: true,
      opacity: colorComponents.a,
    });

    obj.mesh.renderOrder = 5;
    obj.mesh.visible =
      this._workspaceService.isConfigurationLoaded() &&
      this._pbcService.pbcMode &&
      this._pbcService.pbcContainerVisibleState;
    return obj;
  }

  private generateMicroscopeSlide(): BoxObject {
    const colorComponents = colorToArgb(DEFAULT_COLOR_MICROSCOPE_SLIDE_BOX);
    const obj = new BoxObject({
      transparent: true,
      color: rgbToHex(colorComponents), // remove alpha because threejs doesn't support alpha
      side: DoubleSide,
      depthWrite: true,
      depthTest: true,
      opacity: colorComponents.a,
    });

    obj.mesh.renderOrder = 4;
    obj.mesh.visible =
      this._workspaceService.isConfigurationLoaded() &&
      this._pbcService.pbcMode &&
      this._pbcService.microscopeSlideEnableState &&
      this._pbcService.microscopeSlideBoxVisibleState;
    return obj;
  }

  private setPBCContainerSize(size: number): void {
    const halfBoxSize = size / 2;
    this._pbcContainer.mesh.scale.set(size, size, size);
    this._pbcContainer.mesh.position.set(halfBoxSize, halfBoxSize, halfBoxSize);
  }

  private setSizeAndPositionFromMicroscopeSlide(): void {
    this.changeSizeFromMicroscopeSlide(
      this._pbcService.getContainerSize(),
      this._pbcService.microscopeSlideCutSize,
      this._pbcService.microscopeSlideIndex
    );
  }

  private setVisibleStateFromMicroscopeSlide(): void {
    this._microscopeSlide.mesh.visible =
      this._pbcService.pbcMode &&
      this._pbcService.microscopeSlideEnableState &&
      this._pbcService.microscopeSlideBoxVisibleState;
  }

  private changeSizeFromMicroscopeSlide(
    size: number,
    cutSize: number,
    index: number
  ): void {
    const maxIndex = this._pbcService.getMicroscopeSlideMaxIndex();
    const halfBoxSize = size / 2;
    let x = index * cutSize + cutSize / 2;

    if (index == maxIndex) {
      cutSize = size - cutSize * index;
      x = size - cutSize / 2;
    }

    if (index == 0) x += SUPPRESS_FLICKER_POSITION;
    else if (index == maxIndex) x -= SUPPRESS_FLICKER_POSITION;

    this._microscopeSlide.mesh.scale.set(
      cutSize,
      size - SUPPRESS_FLICKER_SIZE,
      size - SUPPRESS_FLICKER_SIZE
    );
    this._microscopeSlide.mesh.position.set(x, halfBoxSize, halfBoxSize);
  }
}
