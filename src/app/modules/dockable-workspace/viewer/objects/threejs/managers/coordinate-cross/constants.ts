/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Vector3 } from 'three';

export const SCALE_ARROW = 0.605;
export const SCALE_SPHERE = 1.0;

export const TUBULAR_SEGMENTS = 1;
export const THICKNESS = 0.8;
export const RADIUS_SEGMENTS = 11;

export const CYLINDER_RADIUS_BOTTOM = 1.2;
export const CYLINDER_HEIGHT = 2;
export const CYLINDER_RADIUS_SEGMENTS = 9;

export const AXIS_LABEL_POSITION = 6.25;

export const AXIS_LENGTH = 4;

export const AXIS_X = new Vector3(AXIS_LENGTH, 0, 0);
export const AXIS_Y = new Vector3(0, AXIS_LENGTH, 0);
export const AXIS_Z = new Vector3(0, 0, AXIS_LENGTH);

export const AXIS_COLOR_X = '#ff0000';
export const AXIS_COLOR_Z = '#00ff00';
export const AXIS_COLOR_Y = '#0000ff';
