/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  BoxGeometry,
  DoubleSide,
  MeshStandardMaterial,
  SphereGeometry,
  Vector3,
} from 'three';
import Manager from '../manager';
import SphereObject from '../../sphere.object';
import BoxObject from '../../box.object';
import { ViewService } from '../../../../../../../core/services/view/view.service';
import { DebugService } from '../../../../../../../core/services/debug.service';
import { PbcService } from '../../../../../../../core/services/pbc.service';
import { IConfigChangedListener } from '../config-changed-listener.interface';
import { ParsedConfiguration } from '../../../../../../../shared/parsers/models/parsed-configuration.model';
import { WorkspaceService } from '../../../../../../../core/services/workspace.service';
import { IServiceListener } from '../../service-listener.interface';

const SPHERE_COLOR = '#900000';
const SPHERE_SIZE = 10;

export default class DebugManager
  extends Manager<
    SphereObject | BoxObject,
    SphereGeometry | BoxGeometry,
    MeshStandardMaterial
  >
  implements IConfigChangedListener, IServiceListener
{
  private readonly _centerOfMass: SphereObject;
  private readonly _fiberBox: BoxObject;

  public constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _viewService: ViewService,
    private readonly _debugService: DebugService,
    private readonly _pbcService: PbcService
  ) {
    super();

    this._centerOfMass = this.generateCenterOfMass();
    this._fiberBox = this.generateFiberBox();

    this.objects.push(this._centerOfMass, this._fiberBox);

    this.subscribeListeners();

    this.initGroupFromObjects();

    this.group.skipExportWhenInvisible = true;
    this.objects.forEach((o) => (o.mesh.skipExportWhenInvisible = true));
  }

  public subscribeListeners(): void {
    this.subscribes.push(
      this._debugService.showCenterOfMass$$.subscribe((isVis) => {
        this.setCenterOfMassSize();
        this._centerOfMass.mesh.visible = isVis;
      }),

      this._debugService.showFiberBox$$.subscribe((isVis) => {
        this.setFiberBoxSize();
        this._fiberBox.mesh.visible = isVis;
      }),

      this._pbcService.pbcMode$.subscribe(() => {
        this.setCenterOfMassSize();
        this.setFiberBoxSize();
      })
    );
  }

  public unsubscribeListeners(): void {
    super.unsubscribeListeners();
  }

  public configChanged(config: ParsedConfiguration | null): void {
    if (!config?.config) {
      this.group.visible = false;
      return;
    }

    this.setCenterOfMassSize();
    this.setFiberBoxSize();
    this._centerOfMass.mesh.visible =
      this._debugService.showCenterOfMass$$.value;
    this._fiberBox.mesh.visible = this._debugService.showFiberBox$$.value;
    this.group.visible = true;
  }

  private generateCenterOfMass(): SphereObject {
    const obj = new SphereObject(5, 5, {
      transparent: true,
      side: DoubleSide,
      depthWrite: true,
      depthTest: true,
      wireframe: true,
      color: SPHERE_COLOR,
    });
    obj.setScale(new Vector3(SPHERE_SIZE, SPHERE_SIZE, SPHERE_SIZE));
    obj.mesh.visible =
      this._workspaceService.isConfigurationLoaded() &&
      this._debugService.debugMode &&
      this._debugService.showCenterOfMass$$.value;

    return obj;
  }

  private generateFiberBox(): BoxObject {
    const obj = new BoxObject({
      transparent: true,
      side: DoubleSide,
      depthWrite: true,
      depthTest: true,
      wireframe: true,
      color: SPHERE_COLOR,
    });
    obj.mesh.visible =
      this._workspaceService.isConfigurationLoaded() &&
      this._debugService.debugMode &&
      this._debugService.showFiberBox$$.value;

    return obj;
  }

  private setCenterOfMassSize(): void {
    const info =
      this._viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();
    this._centerOfMass.mesh.position.set(
      info.centerX,
      info.centerY,
      info.centerZ
    );
  }

  private setFiberBoxSize(): void {
    const info =
      this._viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();

    if (info.width < 0 || info.height < 0 || info.depth < 0) {
      this._fiberBox.mesh.visible = false;
      return;
    }

    this._fiberBox.setScaleFromXYZ(info.width, info.height, info.depth);
    this._fiberBox.mesh.position.set(info.centerX, info.centerY, info.centerZ);
  }
}
