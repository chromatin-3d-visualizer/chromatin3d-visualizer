/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  DoubleSide,
  InstancedMesh,
  MeshStandardMaterial,
  Object3D,
  SphereGeometry,
} from 'three';
import SimpleManager from '../simple-manager';
import { ViewService } from '../../../../../../../core/services/view/view.service';
import { IServiceListener } from '../../service-listener.interface';
import { DebugService } from '../../../../../../../core/services/debug.service';
import { DefinedCamera } from '../../../../../tools/tool-defined-camera-positions/models/defined-camera.model';

const RENDERED_SIZE = 1000;

export default class AnimationVisualizerManager
  extends SimpleManager<InstancedMesh>
  implements IServiceListener
{
  private readonly _sphere = new SphereGeometry(1, 5, 5);
  private readonly _material = new MeshStandardMaterial({
    transparent: true,
    side: DoubleSide,
    depthWrite: true,
    depthTest: true,
    wireframe: true,
    color: '#000000',
  });
  private readonly _mesh = new InstancedMesh(
    this._sphere,
    this._material,
    RENDERED_SIZE
  );

  private readonly _temp = new Object3D();

  public constructor(
    private readonly _viewService: ViewService,
    private readonly _debugService: DebugService
  ) {
    super();

    this.reposition(this._viewService.definedCameras);
    this.subscribeListeners();
    this.initGroupFromObjects();
  }

  public subscribeListeners(): void {
    this.subscribes.push(
      this._viewService.definedCameras$.subscribe((cameras) =>
        this.reposition(cameras)
      ),
      this._debugService.showAnimationKeyframes$$.subscribe(() =>
        this.reposition(this._viewService.definedCameras)
      )
    );
  }

  public unsubscribeListeners(): void {
    super.unsubscribeListeners();
  }

  protected initGroupFromObjects(): void {
    this.group.add(this._mesh);
  }

  private reposition(cameras: DefinedCamera[]): void {
    cameras.forEach((c, i) => {
      this._temp.clear();
      this._temp.position.set(
        c.camera.position.x,
        c.camera.position.y,
        c.camera.position.z
      );
      this._temp.updateMatrix();
      this._mesh.setMatrixAt(i, this._temp.matrix);
    });

    this._mesh.count = cameras.length;
    this._mesh.instanceMatrix.needsUpdate = true;
    this._mesh.visible =
      this._debugService.debugMode &&
      this._debugService.showAnimationKeyframes$$.value;
  }
}
