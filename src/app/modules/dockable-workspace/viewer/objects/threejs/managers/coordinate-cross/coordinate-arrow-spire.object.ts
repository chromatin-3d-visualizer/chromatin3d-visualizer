/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  ColorRepresentation,
  CylinderGeometry,
  DoubleSide,
  Vector3,
} from 'three';
import CylinderObject from '../../cylinder.object';
import {
  CYLINDER_HEIGHT,
  CYLINDER_RADIUS_BOTTOM,
  CYLINDER_RADIUS_SEGMENTS,
  SCALE_ARROW,
} from './constants';
import { convertDegreeToRadians } from '../../../../../../../shared/utilities/three.js.utility';

export default class CoordinateArrowSpireObject extends CylinderObject {
  private readonly _orientation: Vector3;
  private readonly _rotation: Vector3;

  public constructor(
    orientation: Vector3,
    color: ColorRepresentation,
    rotation: Vector3
  ) {
    super(
      {
        radiusTop: 0,
        radiusBottom: CYLINDER_RADIUS_BOTTOM,
        height: CYLINDER_HEIGHT,
        radialSegments: CYLINDER_RADIUS_SEGMENTS,
        heightSegments: 1,
        openEnded: false,
      },
      {
        transparent: true,
        color: color,
        side: DoubleSide,
        depthWrite: true,
        depthTest: true,
        wireframe: false,
      }
    );

    this._orientation = orientation;
    this._rotation = rotation;

    // @ts-ignore
    this.geometry = this.generateGeometry();
    // @ts-ignore
    this.mesh = this.generateMesh();
  }

  protected generateGeometry(): CylinderGeometry {
    const geometry = super.generateGeometry();

    if (!this._orientation || !this._rotation) return geometry;

    if (this._rotation.x)
      geometry.rotateX(convertDegreeToRadians(this._rotation.x));
    if (this._rotation.y)
      geometry.rotateY(convertDegreeToRadians(this._rotation.y));
    if (this._rotation.z)
      geometry.rotateZ(convertDegreeToRadians(this._rotation.z));

    const translateVec = this._orientation
      .clone()
      .multiplyScalar(SCALE_ARROW)
      .multiplyScalar(1.8);
    geometry.translate(translateVec.x, translateVec.y, translateVec.z);
    return geometry;
  }
}
