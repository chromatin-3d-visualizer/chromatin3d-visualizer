/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import InstancedObjectBuilder from '../../../../beads/instanced-object.builder';
import { OneTimeBuilder } from '../../../../../../../shared/models/one-time-builder.model';
import { MultipleBuilderResult } from '../../../../../../../shared/models/multiple-builder-result.model';
import { Configuration } from '../../../../../../../shared/models/configuration.model';
import BuilderGetter from '../../../../../../../shared/builders/builder-getter';
import { IChangeableColors } from '../../../../beads/changeable-colors.interface';
import { ColorBead } from '../../../../../../../core/services/colors/models/color-bead.model';
import { EMaterialType } from '../../../../../../../shared/models/material-type.enum';
import { IChangeableMaterial } from '../../../../beads/changeable-material.interface';
import LinkerDNAApproxBuilder from '../../../../beads/linker-dna-approx.builder';
import Bead from '../../../../../../../shared/models/bead.model';
import {
  getBuilderInstance,
  setConfigSpecificColors,
} from '../../../../../../../shared/utilities/bead-builder.utility';
import { ELinkerDNABuilderType } from '../../../../../../../core/services/session-mananger/models/linker-dna-builder-type.enum';
import { getBasePairs } from '../../../../../../../shared/builders/base-pair';
import DoubleHelixBasePairBuilder from '../../../../beads/double-helix-base-pair.builder';
import ChangeableColorWithConfigColor from '../../../../../../../core/services/colors/changeable-color-with-config-color';
import CohesinHeadBuilder from '../../../../beads/cohesin-head.builder';
import Manager from '../manager';
import { WorkspaceService } from '../../../../../../../core/services/workspace.service';
import { ColorsService } from '../../../../../../../core/services/colors/colors.service';
import { ViewService } from '../../../../../../../core/services/view/view.service';
import { CohesinService } from '../../../../../../../core/services/cohesin.service';
import { AssetsService } from '../../../../../../../core/services/assets/assets.service';
import { IConfigChangedListener } from '../config-changed-listener.interface';
import { ParsedConfiguration } from '../../../../../../../shared/parsers/models/parsed-configuration.model';
import { IQualityLevelListener } from '../../quality-level-listener.interface';
import QualityLevel from '../../../../../../../core/services/session-mananger/models/quality-level.model';
import { SessionManagerService } from '../../../../../../../core/services/session-mananger/session-manager.service';
import { isSelectable } from '../../../utility';
import { THREE_JS_TEXTURE_NAME_BEAD } from '../../../../../../../core/services/assets/constants';
import { IServiceListener } from '../../service-listener.interface';

export default class BeadManager
  extends Manager<any>
  implements IConfigChangedListener, IQualityLevelListener, IServiceListener
{
  private _multipleUseBuilders: InstancedObjectBuilder<any, any>[] = [];
  private _oneTimeUseBuilders: OneTimeBuilder[] = [];
  private _qualityLevel: QualityLevel;

  public get multipleUseBuilders(): InstancedObjectBuilder<any, any>[] {
    return this._multipleUseBuilders;
  }

  public get oneTimeUseBuilders(): OneTimeBuilder[] {
    return this._oneTimeUseBuilders;
  }

  public constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _colorService: ColorsService,
    private readonly _viewService: ViewService,
    private readonly _cohesinService: CohesinService,
    private readonly _assetsService: AssetsService,
    private readonly _sessionService: SessionManagerService
  ) {
    super();

    this._qualityLevel = this._sessionService.qualityLevel;

    this.subscribeListeners();
  }

  public subscribeListeners(): void {
    this.setColorListenerEvents();

    this.subscribes.push(
      this._viewService.revisibility$.subscribe(() => {
        this.revisibilityBeads();
      }),

      this._viewService.materialType$.subscribe((material) =>
        this.changeMaterial(material)
      )
    );
  }

  public unsubscribeListeners(): void {
    super.unsubscribeListeners();
  }

  public configChanged(config: ParsedConfiguration | null): void {
    // configNumber < 0 means that no configuration/trj is loaded (perhaps the user has unload the trajectory)
    if (!config?.config) {
      this.disposeCurrentScene();
    } else {
      // linker DNA must dispose always, because the linker DNA is no instanced mesh
      this.disposeOneTimeUseBuilders();

      this.createOrRepositionModel(config.config);
      this.createOrRepositionBasePairs();
    }
  }

  public qualityLevelChanged(quality: QualityLevel): void {
    this._qualityLevel = quality;
  }

  protected handleMultipleUseBuilders(builders: MultipleBuilderResult[]): void {
    // remove all instances of inactive builders
    for (const builder of builders.filter((b) => !b.isActive)) {
      let builderInstance: InstancedObjectBuilder<any, any> | undefined =
        undefined;

      while (
        (builderInstance = this.getBuilderFromType(builder.builder) as
          | InstancedObjectBuilder<any, any>
          | undefined)
      ) {
        const indexOfBuilder = this._multipleUseBuilders.findIndex(
          (b) => b === builderInstance
        );
        this.group.remove(builderInstance.mesh);
        this._multipleUseBuilders.splice(indexOfBuilder, 1);
      }
    }

    for (const builder of builders.filter((b) => b.isActive)) {
      this.reuseOrRecreateMultipleBuilder(builder);
    }
  }

  protected handleOneTimeUseBuilders(currentConfig: Configuration): void {
    const oneTimeBuilderStart = this._oneTimeUseBuilders.length;

    this.initOneTimeBuilders(currentConfig);

    for (const builder of [...this._oneTimeUseBuilders].splice(
      oneTimeBuilderStart
    )) {
      if (isSelectable(builder))
        builder.setSelectionColor(this._colorService.selectionColor.color);

      if (builder instanceof InstancedObjectBuilder) {
        builder.setUpFirstStyle(
          this._colorService.cohesinRingAndHingeColors.color
        );
        setConfigSpecificColors(
          builder,
          this._colorService.cohesinRingAndHingeColors.configSpecificColors
        );
        this.group.add(builder.mesh);
      } else {
        setConfigSpecificColors(
          builder,
          this._colorService.linkerDNAColors.configSpecificColors
        );
        this.group.add(...builder.meshes);
      }
    }
  }

  private setColorListenerEvents(): void {
    const toSubscribe = BuilderGetter.getBuilderColorSubscriberMap(
      this._colorService
    );

    for (const toSub of toSubscribe) {
      this.subscribes.push(
        toSub.toSubscribe.color$.subscribe((color) => {
          const builders = this.getBuilderFromTypes(toSub.builder);
          builders.forEach((b) =>
            b.changeDefaultColor(
              color,
              toSub.toSubscribe.configSpecificColors.map((cb) => cb.index)
            )
          );
        }),
        toSub.toSubscribe.configSpecificColors$.subscribe((colorBeads) => {
          this.setConfigSpecificColors(toSub.builder, colorBeads);
        })
      );
    }
  }

  private initOneTimeBuilders(currentConfig: Configuration): void {
    this._oneTimeUseBuilders.push(
      ...BuilderGetter.getOneTimeBuilders(
        currentConfig,
        this._colorService,
        this._viewService,
        this._cohesinService,
        this._qualityLevel,
        this._assetsService.getTexture(THREE_JS_TEXTURE_NAME_BEAD)
      )
    );
  }

  private disposeCurrentScene(): void {
    while (this._multipleUseBuilders.length > 0) {
      const builder = this._multipleUseBuilders.splice(0, 1)[0];
      this.destroySingleMultipleUseBuilder(builder);
    }

    this.disposeOneTimeUseBuilders();
  }

  /**
   * There are special builders that have to be created over and over again. For example, the linker DNS cannot be represented as an
   * InstancedBuffer element because it is too different. For the Cohesin Head, the radius or thickness may change. For this reason, they
   * have to be recreated every time the configuration changes or PBC mode is (de)activated.
   * @private
   */
  private disposeOneTimeUseBuilders(): void {
    while (this._oneTimeUseBuilders.length > 0) {
      const builder = this._oneTimeUseBuilders.splice(0, 1)[0];
      if (builder instanceof InstancedObjectBuilder) {
        this.destroySingleMultipleUseBuilder(builder);
      } else {
        this.group.remove(...builder.meshes);
      }
      builder.destroy();
    }
  }

  private getBuilderFromTypes<T extends IChangeableColors>(
    type: new (...param: any[]) => T
  ): IChangeableColors[] {
    return [...this._multipleUseBuilders, ...this._oneTimeUseBuilders].filter(
      (b) => b instanceof type
    ) as IChangeableColors[];
  }

  private getBuilderFromType<T extends IChangeableColors>(
    type: new (...param: any[]) => T
  ): IChangeableColors | undefined {
    const builders = this.getBuilderFromTypes<T>(type);
    if (builders.length > 0) return builders[0];

    return undefined;
  }

  private revisibilityBeads(): void {
    for (const builder of [
      ...this._multipleUseBuilders,
      ...this._oneTimeUseBuilders,
    ]) {
      builder.setSpecificVisibility(this._viewService.invisibleIDs, false);
    }
  }

  private changeMaterial(material: EMaterialType): void {
    for (const changeableMaterial of <IChangeableMaterial[]>(
      [...this._multipleUseBuilders, ...this._oneTimeUseBuilders].filter(
        (b) => (<IChangeableMaterial>b).changeMaterialTo
      )
    )) {
      changeableMaterial.changeMaterialTo(material);
    }
  }

  private reuseOrRecreateMultipleBuilder(builder: MultipleBuilderResult): void {
    const existBuilder = this.getBuilderFromType(builder.builder) as
      | InstancedObjectBuilder<any, any>
      | undefined;

    // if existBuilder.renderSize < builder.renderSize means that threeJS must draw more objects than currently exist
    //   => dispose of old meshes and create new ones
    //   => otherwise reuse the mesh
    if (existBuilder && existBuilder.renderSize >= builder.renderSize) {
      if (existBuilder instanceof LinkerDNAApproxBuilder)
        existBuilder.repositionLinker(
          builder.param as Bead[],
          this._viewService.linkerDNASplitter
        );

      existBuilder.reposition(builder.param);
      this.setMultipleBuilderStyle(existBuilder, builder.colorClass);

      return;
    }

    if (existBuilder) {
      this.destroySingleMultipleUseBuilder(existBuilder);
      this._multipleUseBuilders = this._multipleUseBuilders.filter(
        (b) => b !== existBuilder
      );
    }

    const newBuilder = getBuilderInstance(
      builder,
      this._assetsService.getTexture(THREE_JS_TEXTURE_NAME_BEAD),
      this._qualityLevel,
      this._viewService.linkerDNASplitter
    );
    this.setMultipleBuilderStyle(newBuilder, builder.colorClass);

    this.group.add(newBuilder.mesh);
    this._multipleUseBuilders.push(newBuilder);
  }

  private createOrRepositionBasePairs(): void {
    if (
      this._qualityLevel.linkerDNABuilder !== ELinkerDNABuilderType.DOUBLE_HELIX
    )
      return;

    const baseBindingBeads = getBasePairs(
      this._multipleUseBuilders,
      this._oneTimeUseBuilders
    );
    this.reuseOrRecreateMultipleBuilder({
      builder: DoubleHelixBasePairBuilder,
      colorClass: this._colorService.basePairColors,
      param: baseBindingBeads,
      renderSize: baseBindingBeads.length,
      isActive: true,
    });
  }

  private createOrRepositionModel(config: Configuration): void {
    if (!this._workspaceService.trajectory) return;

    const builders = BuilderGetter.getMultipleUseBuilders(
      config,
      this._colorService,
      this._qualityLevel
    );

    this.handleMultipleUseBuilders(builders);
    this.handleOneTimeUseBuilders(config);
    this.changeMaterial(this._viewService.materialType);
  }

  private setMultipleBuilderStyle(
    builder: InstancedObjectBuilder<any, any> | LinkerDNAApproxBuilder,
    colorClass: ChangeableColorWithConfigColor
  ): void {
    // unsure why, but base pairs showed that the colors have to be set again if new MeshCount is greater than the old one

    if (builder instanceof CohesinHeadBuilder)
      builder.setHeadSize(this._cohesinService.getCohesinHeadRadius());
    builder.setUpFirstStyle(colorClass.color);
    builder.setSelectionColor(this._colorService.selectionColor.color);
    setConfigSpecificColors(builder, colorClass.configSpecificColors);
  }

  private setConfigSpecificColors<T extends IChangeableColors>(
    type: new (...param: any[]) => T,
    colorBeads: ColorBead[]
  ): void {
    const builders = this.getBuilderFromTypes(type);

    builders.forEach((b) => setConfigSpecificColors(b, colorBeads));
  }

  private destroySingleMultipleUseBuilder(
    builder: InstancedObjectBuilder<any, any>
  ): void {
    this.group.remove(builder.mesh);
    builder.destroy();
  }
}
