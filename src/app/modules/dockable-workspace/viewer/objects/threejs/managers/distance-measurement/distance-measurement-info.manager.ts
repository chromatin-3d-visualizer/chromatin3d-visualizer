/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Camera, Mesh, MeshPhongMaterial, Vector3 } from 'three';
import { Font } from 'three/examples/jsm/loaders/FontLoader';
import { LineMaterial } from 'three/examples/jsm/lines/LineMaterial';
import { LineGeometry } from 'three/examples/jsm/lines/LineGeometry';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { Line2 } from 'three/examples/jsm/lines/Line2';
import Manager from '../manager';
import { ICameraSynchronizer } from '../camera-synchronizer.interface';
import TextObject from '../../text.object';
import {
  UNIT,
  USER_FLOATING_DECIMAL_PLACES,
} from '../../../../../../../constants';
import LineObject from '../../line.object';
import { DistanceMeasurementService } from '../../../../../../../core/services/distance-measurement.service';
import { IServiceListener } from '../../service-listener.interface';

const TEXT_DEPTH_SIZE = 0.025;
const TEXT_SIZE_FACTOR = 0.025;
const DISTANCE_TO_CAMERA_FACTOR = 0.9;

export default class DistanceMeasurementInfoManager
  extends Manager<
    LineObject | TextObject,
    LineGeometry | TextGeometry,
    LineMaterial | MeshPhongMaterial,
    Line2 | Mesh
  >
  implements ICameraSynchronizer, IServiceListener
{
  private readonly _font: Font;
  private readonly _lineStart: Vector3;
  private readonly _lineEnd: Vector3;
  private readonly _lineMiddlePoint: Vector3;

  private readonly _text: TextObject;

  public constructor(
    private readonly _distanceMeasurementService: DistanceMeasurementService,
    font: Font,
    startPoint: Vector3,
    endPoint: Vector3
  ) {
    super();

    this._font = font;
    this._lineStart = startPoint;
    this._lineEnd = endPoint;
    this._lineMiddlePoint = startPoint
      .clone()
      .add(endPoint.clone().sub(startPoint).divideScalar(2.0));

    this._text = this.generateDistanceText(
      this._distanceMeasurementService.textColor,
      this._distanceMeasurementService.textSize
    );

    this.objects.push(
      this._text,
      this.generateDistanceLine(
        this._distanceMeasurementService.lineColor,
        this._distanceMeasurementService.lineWidth
      )
    );

    this.subscribeListeners();

    this.initGroupFromObjects();

    this.group.skipExportWhenInvisible = true;
    this.objects.forEach((o) => (o.mesh.skipExportWhenInvisible = true));
  }

  public subscribeListeners(): void {
    this.subscribes.push(
      this._distanceMeasurementService.lineColor$.subscribe((col) =>
        this.changeLineColor(col)
      ),
      this._distanceMeasurementService.lineWidth$.subscribe((width) =>
        this.changeLineWidth(width)
      ),
      this._distanceMeasurementService.textColor$.subscribe((col) =>
        this._text.changeColorFromString(col)
      ),
      this._distanceMeasurementService.textSize$.subscribe((size) => {
        size *= TEXT_SIZE_FACTOR;
        this._text.setScale(new Vector3(size, size, TEXT_DEPTH_SIZE));
      })
    );
  }

  public unsubscribeListeners(): void {
    super.unsubscribeListeners();
  }

  public cameraUpdated(camera: Camera): void {
    // Actually, the following code should prevent the text from being IN the line and thus partially truncated.
    // However, the text can be placed so that it is drawn ABOVE the other stuff. This seems to make the most sense for
    // displaying such data.
    //
    //       this._lineMiddlePoint.clone()
    //       .add(
    //         camera.position
    //           .clone()
    //           .sub(this._lineMiddlePoint)
    //           .normalize()
    //           .multiplyScalar(2)
    //

    const vecToCamera = this._lineMiddlePoint
      .clone()
      .add(
        camera.position
          .clone()
          .sub(this._lineMiddlePoint)
          .multiplyScalar(DISTANCE_TO_CAMERA_FACTOR)
      );

    this._text.setRotationToCameraPosition(camera.position);
    this._text.mesh.rotation.copy(camera.rotation);

    this._text.mesh.position.copy(vecToCamera);
  }

  public changeLineColor(color: string): void {
    (<LineObject[]>this.objects.filter((o) => o instanceof LineObject)).forEach(
      (l) => l.changeColorFromString(color)
    );
  }

  public changeLineWidth(width: number): void {
    (<LineObject[]>this.objects.filter((o) => o instanceof LineObject)).forEach(
      (l) => l.changeLineWidth(width)
    );
  }

  private generateDistanceText(color: string, size: number): TextObject {
    const diff = this._lineStart.distanceTo(this._lineEnd);
    const diffText = `${diff.toFixed(USER_FLOATING_DECIMAL_PLACES)} ${UNIT}`;

    const obj = new TextObject(this._font, this._lineMiddlePoint, diffText);
    obj.changeColorFromString(color);
    size *= TEXT_SIZE_FACTOR;
    obj.setScale(new Vector3(size, size, TEXT_DEPTH_SIZE));
    obj.geometry.center();

    return obj;
  }

  private generateDistanceLine(color: string, width: number): LineObject {
    return new LineObject([this._lineStart, this._lineEnd], color, width);
  }
}
