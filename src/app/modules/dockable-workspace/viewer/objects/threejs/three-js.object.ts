/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { BufferGeometry, Material, Mesh, ShaderMaterial, Vector3 } from 'three';
import { Geometry } from 'three/examples/jsm/deprecated/Geometry';
import { MeshBasicMaterial } from 'three/src/materials/MeshBasicMaterial';
import { Object3D } from 'three/src/core/Object3D';
import { RGBAColor } from '../../../../../shared/models/rgba-color.model';
import {
  colorToArgb,
  rgbToHex,
} from '../../../../../shared/utilities/color.utility';

export default abstract class ThreeJsObject<
  TGeometry extends Geometry | BufferGeometry = BufferGeometry,
  TMaterial extends Material = ShaderMaterial,
  TMesh extends Mesh | Object3D = Mesh
> {
  public abstract readonly geometry: TGeometry;
  public abstract readonly material: TMaterial;
  public abstract readonly mesh: TMesh;

  private static isMeshBasicMaterial(
    material: any
  ): material is MeshBasicMaterial {
    return material['color'];
  }

  public changeColorFromString(color: string): void {
    this.changeColor(colorToArgb(color));
  }

  public changeColor(color: RGBAColor): void {
    if (!ThreeJsObject.isMeshBasicMaterial(this.material))
      throw new Error('Invalid material!');

    this.material.color.set(rgbToHex(color));
    this.material.opacity = color.a;
    this.material.needsUpdate = true;
  }

  public setScale(scaleVec: Vector3): void {
    this.mesh.scale.copy(scaleVec);
  }

  public setScaleFromXYZ(x: number, y: number, z: number): void {
    this.mesh.scale.set(x, y, z);
  }

  protected readonly renderOrder: number = 0;

  protected abstract generateGeometry(): TGeometry;
  protected abstract generateMaterial(): TMaterial;
  protected abstract generateMesh(): TMesh;
}
