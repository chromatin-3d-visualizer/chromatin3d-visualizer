/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Mesh, MeshStandardMaterial, TubeGeometry } from 'three';
import { Vector3 } from 'three/src/math/Vector3';
import { Curve } from 'three/src/extras/core/Curve';
import { MeshStandardMaterialParameters } from 'three/src/materials/MeshStandardMaterial';
import ThreeJsObject from './three-js.object';

type TubeParameters = {
  path: Curve<Vector3>;
  tubularSegments: number;
  radius: number;
  radiusSegments: number;
  closed: boolean;
};

export default class TubeObject extends ThreeJsObject<
  TubeGeometry,
  MeshStandardMaterial
> {
  public readonly geometry: TubeGeometry;
  public readonly material: MeshStandardMaterial;
  public readonly mesh: Mesh;

  private readonly _path: Curve<Vector3>;
  private readonly _tubularSegments: number;
  private readonly _radius: number;
  private readonly _radiusSegments: number;
  private readonly _closed: boolean;

  public constructor(
    parameters: TubeParameters,
    materialParams?: MeshStandardMaterialParameters
  ) {
    super();

    this._path = parameters.path;
    this._tubularSegments = parameters.tubularSegments;
    this._radius = parameters.radius;
    this._radiusSegments = parameters.radiusSegments;
    this._closed = parameters.closed;

    this.geometry = this.generateGeometry();
    this.material = this.generateMaterial(materialParams);
    this.mesh = this.generateMesh();
  }

  protected generateGeometry(): TubeGeometry {
    return new TubeGeometry(
      this._path,
      this._tubularSegments,
      this._radius,
      this._radiusSegments,
      this._closed
    );
  }

  protected generateMaterial(
    parameters?: MeshStandardMaterialParameters
  ): MeshStandardMaterial {
    return new MeshStandardMaterial({
      color: 0x900000,
      ...parameters,
    });
  }

  protected generateMesh(): Mesh {
    return new Mesh(this.geometry, this.material);
  }
}
