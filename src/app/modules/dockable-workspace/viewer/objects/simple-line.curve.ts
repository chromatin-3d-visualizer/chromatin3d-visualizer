/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Curve, Vector3 } from 'three';

export default class SimpleLineCurve extends Curve<Vector3> {
  private _orientionVector: Vector3;

  public constructor(orientionVector: Vector3 | undefined = undefined) {
    super();

    if (!orientionVector) orientionVector = new Vector3(1, 0, 0);

    this._orientionVector = orientionVector.clone();
    // this._orientionVector.normalize();
  }

  public getPoint(t: number): Vector3 {
    return this._orientionVector.clone().multiplyScalar(t);
  }
}
