/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { ViewerComponent } from './viewer.component';
import { WebXRViewButtonComponent } from './objects/webxr-view-button/webxr-view-button.component';
import { SharedModule } from '../../../shared/shared.module';
import { GLTFExportHandlerService } from './handlers/gltf-export-handler.service';
import { SceneModule } from './scene.module';

@NgModule({
  declarations: [ViewerComponent, WebXRViewButtonComponent],
  providers: [],
  imports: [
    CommonModule,
    SceneModule,
    MatButtonModule,
    SharedModule,
    RouterModule,
  ],
  exports: [ViewerComponent],
  bootstrap: [ViewerComponent],
})
export class ViewerModule {
  public constructor(
    // Injection of handlers so that they are used in the angular application (and thus callable)
    private readonly _gltfExportHandlerService: GLTFExportHandlerService // ------ end
  ) {}
}
