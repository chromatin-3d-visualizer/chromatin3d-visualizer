/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Vector2 } from 'three';
import { SceneHelper } from './helpers/scene.helper';
import { BaseTabComponentDirective } from '../../../shared/directives/base-tab-component.directive';
import { ColorsService } from '../../../core/services/colors/colors.service';
import { CoordinateCrossHelper } from './helpers/coordinate-cross.helper';
import { ViewService } from '../../../core/services/view/view.service';
import { WorkspaceService } from '../../../core/services/workspace.service';
import { ComponentContainer } from 'golden-layout';
import { downloadBlob } from '../../../shared/utilities/window.utility';
import { MenuStripService } from '../../../core/services/menu-strip.service';
import { GITLAB_REPO_ID_C3D_VISUALIZER } from '../../../constants';
import { RaycastService } from '../../../core/services/raycast.service';
import { ToolDistanceMeasurement2DHelper } from './helpers/tool-distance-measurement2-d-helper.service';
import { DistanceMeasurementService } from '../../../core/services/distance-measurement.service';

const MOUSE_EVENT_LEFT_MOUSE_BUTTON_ID = 1;

@Component({
  selector: 'trj-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss'],
})
export class ViewerComponent
  extends BaseTabComponentDirective
  implements OnInit, OnDestroy
{
  public readonly GITLAB_REPO_ID = GITLAB_REPO_ID_C3D_VISUALIZER;
  public readonly GITLAB_REPO_REF = 'master';
  public readonly GITLAB_REPO_FILE = 'mock_data/shortTestTrajectory.trj';

  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas: ElementRef<HTMLCanvasElement> | undefined;

  @ViewChild('coordinateCross', { static: true })
  public coordinateCross: ElementRef<HTMLCanvasElement> | undefined;

  @ViewChild('tmpDistanceMeasurement2D', { static: true })
  public tmpDistanceMeasurement2D: ElementRef<HTMLCanvasElement> | undefined;

  public backColor: string;

  private _isMouseDownForSecondPoint$$ = new BehaviorSubject<boolean>(false);
  private _suppressDistanceMovement = false;

  public isMouseDownForSecondPoint$ =
    this._isMouseDownForSecondPoint$$.asObservable();

  public get isMouseDownForSecondPoint(): boolean {
    return this._isMouseDownForSecondPoint$$.value;
  }

  public set isMouseDownForSecondPoint(val: boolean) {
    if (this._isMouseDownForSecondPoint$$.value === val) return;

    this._isMouseDownForSecondPoint$$.next(val);
  }

  constructor(
    @Inject(BaseTabComponentDirective.GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN)
    private readonly _container: ComponentContainer,
    private readonly _viewerService: SceneHelper,
    private readonly _coordinateCross: CoordinateCrossHelper,
    private readonly _toolDistanceMeasurement2DHelper: ToolDistanceMeasurement2DHelper,
    private readonly _colorsService: ColorsService,
    private readonly _menuStripService: MenuStripService,
    public readonly distanceMeasurementService: DistanceMeasurementService,
    public readonly raycastService: RaycastService,
    public readonly viewService: ViewService,
    public readonly workspaceService: WorkspaceService
  ) {
    super();

    this.subscribes.push(
      this._colorsService.backColor.color$.subscribe((col) => {
        this.backColor = col;
      }),
      this._menuStripService.makeScreen$.subscribe(async () =>
        downloadBlob(
          await this._viewerService.makeScreen(1920, 1080),
          `screenshot_${ViewerComponent.getCurrentDate()}.png`
        )
      )
    );

    this._container.addEventListener('resize', () => this.setViewerSize());

    this.backColor = this._colorsService.backColor.color;
  }

  private static getCurrentDate(): string {
    const currentDate = new Date();
    const dashNumbers = [
      currentDate.getFullYear(),
      currentDate.getMonth() + 1, // starts by 0...
      currentDate.getDate(),
    ].map((n) => n.toString().padStart(2, '0'));
    const doublePointNumbers = [
      currentDate.getHours(),
      currentDate.getMinutes(),
      currentDate.getSeconds(),
    ].map((n) => n.toString().padStart(2, '0'));

    return `${dashNumbers.join('-')}_${doublePointNumbers.join('')}`;
  }

  ngOnInit(): void {
    console.log('INIT VIEWER THREEJS');
    if (!this.rendererCanvas || !this.rendererCanvas.nativeElement) return;

    this._viewerService.createScene(this.rendererCanvas.nativeElement);
    this._viewerService.animate();

    this.initCoordinateCross();
    this.initToolDistanceMeasurement2DHelper();

    this.setViewerSize();
  }

  ngOnDestroy(): void {
    console.log('DEST VIEWER THREEJS');
    this.unsubscribeListeners();
    this._viewerService.ngOnDestroy();
    this._coordinateCross.ngOnDestroy();
    this._toolDistanceMeasurement2DHelper.ngOnDestroy();
  }

  setViewerSize(): void {
    this._viewerService.resize(this._container.width, this._container.height);
    this._toolDistanceMeasurement2DHelper.resize(
      this._container.width,
      this._container.height
    );
  }

  onDoubleClickOnViewer(clickEvent: MouseEvent): void {
    const camera = this._viewerService.sceneCamera;

    if (!camera) return;

    const intersection = this.raycastService.getObjectFromLine(
      this.normalizeMouseCoordinates(clickEvent.offsetX, clickEvent.offsetY),
      this._viewerService.getSelectableObjects(),
      camera
    );

    if (!intersection) return;

    if (clickEvent.shiftKey) this._viewerService.toggleSelection(intersection);
    else this._viewerService.setSelection(intersection);
  }

  setToolDistanceMeasurementFirstPoint(clickEvent: MouseEvent): void {
    if (!this.distanceMeasurementService.isDistanceMeasurement) return;

    const camera = this._viewerService.sceneCamera;

    if (!camera) return;

    const intersection = this.raycastService.getObjectFromLine(
      this.normalizeMouseCoordinates(clickEvent.offsetX, clickEvent.offsetY),
      this._viewerService.getSelectableObjects(),
      camera
    );

    this.isMouseDownForSecondPoint = !!intersection;
    this._suppressDistanceMovement = !intersection;

    if (!intersection) {
      this.distanceMeasurementService.firstPoint = null;
      return;
    }

    const point = new Vector2(clickEvent.offsetX, clickEvent.offsetY);
    this.distanceMeasurementService.firstPoint = {
      ...intersection,
      point2D: point,
    };
    this._toolDistanceMeasurement2DHelper.setPoint(point, point);
  }

  setToolDistanceMeasurementSecondPoint(clickEvent: MouseEvent): void {
    if (!this.distanceMeasurementService.isDistanceMeasurement) return;

    this._viewerService.clearSelections(true);

    if (!this.distanceMeasurementService.firstPoint) return;

    this.isMouseDownForSecondPoint = false;

    const camera = this._viewerService.sceneCamera;

    if (!camera) return;

    const intersection = this.raycastService.getObjectFromLine(
      this.normalizeMouseCoordinates(clickEvent.offsetX, clickEvent.offsetY),
      this._viewerService.getSelectableObjects(),
      camera
    );

    if (intersection) {
      this.distanceMeasurementService.secondPoint = {
        ...intersection,
        point2D: new Vector2(clickEvent.offsetX, clickEvent.offsetY),
      };

      this._viewerService.addToolDistanceMeasurementLine(
        this.distanceMeasurementService.firstPoint.point,
        this.distanceMeasurementService.secondPoint.point
      );
    } else {
      this.distanceMeasurementService.secondPoint = null;
    }
  }

  setToolDistanceMeasurementMovement(clickEvent: MouseEvent): void {
    if (
      !this.distanceMeasurementService.isDistanceMeasurement ||
      (clickEvent.buttons === MOUSE_EVENT_LEFT_MOUSE_BUTTON_ID &&
        !this.distanceMeasurementService.firstPoint)
    )
      return;

    const camera = this._viewerService.sceneCamera;

    if (!camera) return;

    if (
      this.isMouseDownForSecondPoint &&
      this.distanceMeasurementService.firstPoint
    )
      this._toolDistanceMeasurement2DHelper.setPoint(
        this.distanceMeasurementService.firstPoint.point2D,
        new Vector2(clickEvent.offsetX, clickEvent.offsetY)
      );

    const intersection = this.raycastService.getObjectFromLine(
      this.normalizeMouseCoordinates(clickEvent.offsetX, clickEvent.offsetY),
      this._viewerService.getSelectableObjects(),
      camera
    );

    if (!intersection) {
      this._viewerService.clearSelections();
    } else {
      this._viewerService.setSelection(intersection);
    }
  }

  private initCoordinateCross(): void {
    if (!this.coordinateCross || !this.coordinateCross.nativeElement) return;

    this._coordinateCross.createScene(this.coordinateCross.nativeElement);
    this._coordinateCross.animate();
  }

  private initToolDistanceMeasurement2DHelper(): void {
    if (
      !this.tmpDistanceMeasurement2D ||
      !this.tmpDistanceMeasurement2D.nativeElement
    )
      return;

    this._toolDistanceMeasurement2DHelper.createScene(
      this.tmpDistanceMeasurement2D.nativeElement
    );
    this._toolDistanceMeasurement2DHelper.animate();
  }

  // Taken off https://threejs.org/docs/#api/en/core/Raycaster
  private normalizeMouseCoordinates(clientX: number, clientY: number): Vector2 {
    // calculate pointer position in normalized device coordinates
    // (-1 to +1) for both components

    return new Vector2(
      (clientX / this._container.width) * 2 - 1,
      -(clientY / this._container.height) * 2 + 1
    );
  }
}
