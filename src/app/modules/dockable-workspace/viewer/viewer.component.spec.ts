/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { LayoutConfig } from 'golden-layout';
import { ViewerComponent } from './viewer.component';
import { SceneModule } from './scene.module';
import { WorkspaceService } from '../../../core/services/workspace.service';
import { WorkspaceService as MockWorkspaceService } from '../../../core/services/__mocks__/workspace.service';
import {
  APPLICATION_IMPORTS,
  MATERIAL_IMPORTS,
  WORKSPACE_IMPORTS,
} from '../../../core/services/__mocks__/imports';
import { DockingSuiteHelperService } from '../../../core/services/docking-suite-helper/docking-suite-helper.service';
import { layout } from './__mocks__/layout';
import { WebXRViewButtonComponent } from './objects/webxr-view-button/webxr-view-button.component';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader';

@Component({
  template: `
    <trj-dockable-area [fallbackLayout]="defaultLayout"></trj-dockable-area>
  `,
})
class WrapperComponent {
  public defaultLayout = layout as LayoutConfig;
}

describe('ViewerComponent', () => {
  let component: WrapperComponent;
  let fixture: ComponentFixture<WrapperComponent>;

  beforeEach(async () => {
    spyOn(FontLoader.prototype, 'load');
    await TestBed.configureTestingModule({
      declarations: [
        WrapperComponent,
        ViewerComponent,
        WebXRViewButtonComponent,
      ],
      imports: [
        ...MATERIAL_IMPORTS,
        ...WORKSPACE_IMPORTS,
        ...APPLICATION_IMPORTS,
        SceneModule,
        RouterTestingModule,
      ],
      providers: [
        DockingSuiteHelperService,
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
