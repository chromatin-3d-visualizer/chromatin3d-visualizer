/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  InstancedMesh,
  LineCurve3,
  ShaderMaterial,
  Texture,
  TubeGeometry,
  Vector3,
} from 'three';
import InstancedBeadBuilder from './instanced-bead.builder';
import { DEFAULT_COLOR_HISTONE_OCTAMER } from '../../../../shared/color.constants';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import { IDoubleHelixBasePairData } from '../../../../core/services/session-mananger/models/double-helix-base-pair-data.interface';
import { BASE_PAIR_THICKNESS } from './models.variables';
import BasePairObject from './base-pair-object.model';

const MESH_NAME = 'Base Pair-Mesh';

export default class DoubleHelixBasePairBuilder extends InstancedBeadBuilder<
  BasePairObject,
  IDoubleHelixBasePairData,
  TubeGeometry,
  ShaderMaterial
> {
  public geometry: TubeGeometry;
  public material: ShaderMaterial;
  public mesh: InstancedMesh<TubeGeometry, ShaderMaterial>;

  protected defaultColor = DEFAULT_COLOR_HISTONE_OCTAMER;

  public constructor(
    objects: BasePairObject[],
    texture: Texture,
    qualityLevel: IDoubleHelixBasePairData,
    renderSize = -1
  ) {
    super(objects, texture, qualityLevel, renderSize);

    this.geometry = this.generateGeometry();
    this.material = this.generateShaderMaterial();
    this.mesh = this.generateMesh();

    this.changeableMaterials.push(
      { type: EMaterialType.NORMAL, material: this.material },
      {
        type: EMaterialType.WITH_TEXTURE,
        material: this.generateShaderMaterialWithTexture(),
      }
    );
  }

  public getSelectionTexts(): string[] {
    return [];
  }

  protected generateGeometry(): TubeGeometry {
    const geometry = new TubeGeometry(
      new LineCurve3(new Vector3(0, 0, 0), new Vector3(0, 1, 0)),
      this.qualityLevel.doubleHelixBasePairTubularSegments,
      BASE_PAIR_THICKNESS,
      this.qualityLevel.doubleHelixBasePairRadiusSegments
    );

    this.addAttributesToGeometry(geometry);
    return geometry;
  }

  protected generateMesh(): InstancedMesh<TubeGeometry, ShaderMaterial> {
    const mesh = super.generateMesh();
    mesh.renderOrder = 1;
    mesh.name = MESH_NAME;
    return mesh;
  }

  protected setSingleInstance(index: number): void {
    const bead = this.beads[index];

    const startPoint = bead.point;
    const endPoint = bead.counterPoint;
    const seg = startPoint.clone().sub(endPoint);

    this.tmpScratchObject3D.clear();
    this.tmpScratchObject3D.position.set(
      startPoint.x,
      startPoint.y,
      startPoint.z
    );
    this.tmpScratchObject3D.lookAt(endPoint);
    this.tmpScratchObject3D.rotateX(Math.PI / 2);
    this.tmpScratchObject3D.scale.set(1, seg.length(), 1);
    this.tmpScratchObject3D.updateMatrix();
    this.mesh.setMatrixAt(index, this.tmpScratchObject3D.matrix);
    // this.bufferAttrInvisible?.setX(index, 0);
  }
}
