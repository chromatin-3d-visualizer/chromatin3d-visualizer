/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { CatmullRomCurve3, Vector3 } from 'three';
import { getOrthogonalVectorFromVector } from '../../../../shared/utilities/vector.utility';
import {
  DOUBLE_HELIX_DEGREES_PER_STEP,
  DOUBLE_HELIX_LENGTH_OF_ROUND,
  DOUBLE_HELIX_POINTS_PER_ROUND,
  DOUBLE_HELIX_RADIUS,
} from './models.variables';

type Vectors = {
  segment: Vector3;
  segmentNormalize: Vector3;
  segmentTNormalize: Vector3;
  segmentLength: number;
};

export default class DoubleHelixCurve {
  private readonly _vectors: Vectors[] = [];

  private readonly _curveLength: number;
  private readonly _inputPoints: Vector3[];

  public constructor(points: Vector3[], smooth = false) {
    if (points.length < 2) throw new Error('Not enough points!');
    if (points.length < 3) points.push(points[1]);
    this._inputPoints = points;
    this._curveLength = this.setVectorsAndGetLength();

    if (smooth) {
      this._vectors.splice(0);
      points = new CatmullRomCurve3(points).getPoints(
        Math.max(1, Math.trunc(this._curveLength))
      );

      this._inputPoints = points;
      this._curveLength = this.setVectorsAndGetLength();
    }
  }

  private static getSegTFromLastSegmentVector(
    segT: Vector3,
    lastSegment: Vector3,
    lastLastSegment: Vector3 | undefined,
    vectors: Vectors
  ): void {
    const degree = segT.angleTo(lastSegment);
    if (Math.abs(degree - DOUBLE_HELIX_DEGREES_PER_STEP) > 0.01) {
      let toDegree = DOUBLE_HELIX_DEGREES_PER_STEP - degree;

      if (DOUBLE_HELIX_DEGREES_PER_STEP < degree)
        toDegree = degree - DOUBLE_HELIX_DEGREES_PER_STEP;

      segT.applyAxisAngle(vectors.segmentNormalize, toDegree);
    }

    if (lastLastSegment && Math.abs(segT.angleTo(lastLastSegment)) < 0.4) {
      segT.applyAxisAngle(
        vectors.segmentNormalize,
        DOUBLE_HELIX_DEGREES_PER_STEP * 2
      );
    }
  }

  public getCurvePoints(startDegree = 0): Vector3[] {
    const newCurvePoints: Vector3[] = [];

    const rounds = Math.ceil(this._curveLength / DOUBLE_HELIX_LENGTH_OF_ROUND);
    const points = rounds * DOUBLE_HELIX_POINTS_PER_ROUND;
    const curveLengthPerStep = this._curveLength / points;
    let currentCurveLength = 0;
    let curveSegmentIndex = 0;
    let curveLengthStart = 0;

    let lastSegment;
    let lastLastSegment;
    for (let i = 0; i < points; ++i) {
      const vectors = this._vectors[curveSegmentIndex];
      const segT = vectors.segmentTNormalize.clone();
      segT.applyAxisAngle(
        vectors.segmentNormalize,
        DOUBLE_HELIX_DEGREES_PER_STEP * i + startDegree
      );

      if (lastSegment) {
        DoubleHelixCurve.getSegTFromLastSegmentVector(
          segT,
          lastSegment,
          lastLastSegment,
          vectors
        );
      }

      let currentHeightPosition = currentCurveLength - curveLengthStart;

      lastLastSegment = lastSegment;
      lastSegment = segT;

      newCurvePoints.push(
        this._inputPoints[curveSegmentIndex]
          .clone()
          .add(
            vectors.segmentNormalize
              .clone()
              .multiply(
                new Vector3(
                  currentHeightPosition,
                  currentHeightPosition,
                  currentHeightPosition
                )
              )
          )
          .add(segT)
      );

      currentCurveLength += curveLengthPerStep;
      while (
        curveSegmentIndex < this._vectors.length - 1 &&
        curveLengthStart + this._vectors[curveSegmentIndex].segmentLength <
          currentCurveLength
      ) {
        {
          curveLengthStart += this._vectors[curveSegmentIndex].segmentLength;
          ++curveSegmentIndex;
        }
      }
    }

    return newCurvePoints;
  }

  private setVectorsAndGetLength(): number {
    let curveLength = 0;
    for (let i = 1; i < this._inputPoints.length; ++i) {
      const segmentVector = this._inputPoints[i]
        .clone()
        .sub(this._inputPoints[i - 1]);
      const segmentNormalize = segmentVector.clone().normalize();

      const segmentT = getOrthogonalVectorFromVector(segmentVector).normalize();
      segmentT.multiply(
        new Vector3(
          DOUBLE_HELIX_RADIUS,
          DOUBLE_HELIX_RADIUS,
          DOUBLE_HELIX_RADIUS
        )
      );
      this._vectors.push({
        segment: segmentVector,
        segmentNormalize: segmentNormalize,
        segmentTNormalize: segmentT,
        segmentLength: segmentVector.length(),
      });
      curveLength += segmentVector.length();
    }

    return curveLength;
  }
}
