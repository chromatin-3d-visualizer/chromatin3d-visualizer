/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { InstancedMesh, ShaderMaterial, SphereGeometry, Texture } from 'three';
import InstancedBeadBuilder from './instanced-bead.builder';
import { DEFAULT_COLOR_COHESIN_HEAD } from '../../../../shared/color.constants';
import CohesinHeadBead from '../../../../shared/models/cohesin-head-bead.model';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import { ICohesinHeadData } from '../../../../core/services/session-mananger/models/cohesin-head-data.interface';
import {
  BEAD_USER_NAME_COHESIN_HEAD,
  BEAD_USER_NAME_COHESIN_PREFIX,
} from '../../../../constants';

const MESH_NAME = 'Cohesin-Head-Mesh';

export default class CohesinHeadBuilder extends InstancedBeadBuilder<
  CohesinHeadBead,
  ICohesinHeadData,
  SphereGeometry,
  ShaderMaterial
> {
  private _radius = 1;

  public readonly geometry: SphereGeometry;
  public readonly material: ShaderMaterial;
  public readonly mesh: InstancedMesh<SphereGeometry, ShaderMaterial>;

  protected readonly defaultColor = DEFAULT_COLOR_COHESIN_HEAD;

  public constructor(
    heads: CohesinHeadBead[],
    texture: Texture,
    qualityLevel: ICohesinHeadData,
    renderSize = -1
  ) {
    super(heads, texture, qualityLevel, renderSize);

    this.geometry = this.generateGeometry();
    this.material = this.generateShaderMaterial();
    this.mesh = this.generateMesh();

    this.changeableMaterials.push(
      { type: EMaterialType.NORMAL, material: this.material },
      {
        type: EMaterialType.WITH_TEXTURE,
        material: this.generateShaderMaterialWithTexture(),
      }
    );
  }

  public getSelectionTexts(): string[] {
    return this.getSelectionInfosFromBeads(
      this.beads,
      `${BEAD_USER_NAME_COHESIN_PREFIX} ${BEAD_USER_NAME_COHESIN_HEAD}`
    );
  }

  public setHeadSize(headRadius: number): void {
    this._radius = headRadius;
  }

  protected generateGeometry(): SphereGeometry {
    const geometry = new SphereGeometry(
      1,
      this.qualityLevel.cohesinHeadWidthSegments,
      this.qualityLevel.cohesinHeadHeightSegments
    );

    this.addAttributesToGeometry(geometry);
    return geometry;
  }

  protected generateMesh(): InstancedMesh<SphereGeometry, ShaderMaterial> {
    const mesh = super.generateMesh();
    mesh.name = MESH_NAME;
    return mesh;
  }

  protected setSingleInstance(index: number): void {
    const head = this.beads[index];
    this.tmpScratchObject3D.clear();
    this.tmpScratchObject3D.position.set(
      head.drawPosition.x,
      head.drawPosition.y,
      head.drawPosition.z
    );
    this.tmpScratchObject3D.scale.set(this._radius, this._radius, this._radius);
    this.tmpScratchObject3D.updateMatrix();
    this.mesh.setMatrixAt(index, this.tmpScratchObject3D.matrix);
  }
}
