/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { BufferGeometry, Color, Material, Mesh, ShaderMaterial } from 'three';
import Bead from '../../../../shared/models/bead.model';
import { INucleosomeDNAData } from '../../../../core/services/session-mananger/models/nucleosome-dna-data.interface';
import LinkerDNABuilder from './linker-dna.builder';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import { RGBAColor } from '../../../../shared/models/rgba-color.model';
import {
  aOptionalRGBToHex,
  colorToArgb,
} from '../../../../shared/utilities/color.utility';
import { MATERIAL_ATTRIBUTE_NAME_SELECTED } from './models.variables';
import { EPS_IS_ZERO } from '../../../../constants';
import { IChangeableMaterial } from './changeable-material.interface';
import { ISelectable } from './selectable.interface';
import { isShaderWithUniforms } from '../../../../shared/utilities/three.js.utility';

export default abstract class LinkerDNAMultipleMaterialsBuilder<
    TBead extends Bead,
    TQuality extends INucleosomeDNAData,
    TGeometry extends BufferGeometry,
    TMaterial extends Material & { color?: Color }
  >
  extends LinkerDNABuilder<TBead, TQuality, TGeometry, TMaterial>
  implements IChangeableMaterial, ISelectable
{
  protected readonly changeableMaterials: {
    type: EMaterialType;
    material: TMaterial;
  }[][] = [];

  public toggleSelection(meshSelection: Mesh): void {
    const selectedIndex = this.meshes.findIndex((m) => m === meshSelection);
    if (selectedIndex < 0) return;

    for (const material of this.changeableMaterials[selectedIndex]) {
      if (!isShaderWithUniforms(material.material)) continue;

      const isAlreadySelected =
        Math.abs(
          material.material.uniforms[MATERIAL_ATTRIBUTE_NAME_SELECTED].value -
            1.0
        ) <= EPS_IS_ZERO;
      material.material.uniforms[MATERIAL_ATTRIBUTE_NAME_SELECTED].value =
        isAlreadySelected ? 0.0 : 1.0;
    }
  }

  public clearSelection(): void {
    for (const changeableMaterialInfo of this.changeableMaterials) {
      for (const material of changeableMaterialInfo) {
        if (!isShaderWithUniforms(material.material)) continue;

        material.material.uniforms[
          MATERIAL_ATTRIBUTE_NAME_SELECTED
        ].value = 0.0;
      }
    }
  }

  public setSelectionColor(col: string): void {
    const colorComponents = colorToArgb(col);
    const colorWithoutAlpha = new Color(
      aOptionalRGBToHex({ ...colorComponents, a: undefined })
    );
    for (const changeableMaterialInfo of this.changeableMaterials) {
      for (const material of changeableMaterialInfo) {
        if (!isShaderWithUniforms(material.material)) continue;

        material.material.uniforms.selectionColor.value = colorWithoutAlpha;
      }
    }
  }

  protected changeVisibilityOpacityAndColorFromIndex(
    index: number,
    colorComponents: RGBAColor,
    colorWithoutAlpha: string,
    changeColor: boolean = false
  ): void {
    const colWithoutAlpha = new Color(colorWithoutAlpha);
    for (const material of this.changeableMaterials[index].map(
      (m) => m.material
    )) {
      if (changeColor && isShaderWithUniforms(material))
        material.uniforms.vertexColor.value.set(colWithoutAlpha);
      if (material instanceof ShaderMaterial)
        material.uniforms.opacity.value = colorComponents.a;
      material.opacity = colorComponents.a;
      material.visible = this.isVisible(index, material);
    }
  }

  protected setVisibilityOfMaterialFromIndex(
    index: number,
    isVis: boolean
  ): void {
    for (const material of this.changeableMaterials[index].map(
      (m) => m.material
    )) {
      material.visible = isVis;
    }
  }

  public abstract changeMaterialTo(to: EMaterialType): void;
}
