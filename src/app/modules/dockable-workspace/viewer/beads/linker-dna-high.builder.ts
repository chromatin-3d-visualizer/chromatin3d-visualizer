/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  Mesh,
  ShaderMaterial,
  Texture,
  TubeGeometry,
  UniformsUtils,
  Vector3,
} from 'three';
import { NUCLEOSOME_DNA_THICKNESS } from './models.variables';
import Bead from '../../../../shared/models/bead.model';
import { DEFAULT_COLOR_LINKER_DNA } from '../../../../shared/color.constants';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import { INucleosomeDNAData } from '../../../../core/services/session-mananger/models/nucleosome-dna-data.interface';
import LinkerDNAMultipleMaterialsBuilder from './linker-dna-multiple-materials.builder';

const MESH_NAME = 'Linker DNA High-Mesh';

export default class LinkerDNAHighBuilder extends LinkerDNAMultipleMaterialsBuilder<
  Bead,
  INucleosomeDNAData,
  TubeGeometry,
  ShaderMaterial
> {
  private readonly _texture: Texture;

  public constructor(
    beads: Bead[],
    qualityLevel: INucleosomeDNAData,
    color: string,
    splicer: number[],
    texture: Texture
  ) {
    super(beads, qualityLevel, splicer);
    this._texture = texture;

    if (!color) color = DEFAULT_COLOR_LINKER_DNA;

    this.calculateCurves();
    this.generateMeshes(color);
  }

  public changeMaterialTo(to: EMaterialType): void {
    for (let i = 0; i < this.meshes.length; ++i) {
      const newMaterial = this.changeableMaterials[i].find(
        (m) => m.type === to
      );
      if (newMaterial) this.meshes[i].material = newMaterial.material;
    }
  }

  protected generateMaterial(color: string): ShaderMaterial {
    const shaderStuff = LinkerDNAHighBuilder.getMaterialParameters(color);
    return new ShaderMaterial({
      ...shaderStuff.param,
      uniforms: shaderStuff.uniforms,
    });
  }

  protected generateMaterialWithTexture(color: string): ShaderMaterial {
    const shaderStuff = LinkerDNAHighBuilder.getMaterialParameters(color);
    shaderStuff.uniforms = UniformsUtils.merge([
      shaderStuff.uniforms,
      {
        map: { type: 't', value: this._texture },
      },
    ]);
    return new ShaderMaterial({
      ...shaderStuff.param,
      uniforms: shaderStuff.uniforms,
      defines: {
        USE_MAP: '',
        USE_UV: '',
      },
    });
  }

  protected generateGeometry(curvePoints: Vector3[]): TubeGeometry {
    return new TubeGeometry(
      this.generateCurveFromPoints(curvePoints),
      (curvePoints.length - 1) * this.qualityLevel.linkerDNASmoothing,
      NUCLEOSOME_DNA_THICKNESS,
      this.qualityLevel.nucleosomeDNARadiusSegments,
      false
    );
  }

  protected generateMeshes(color: string): void {
    for (const curvePoints of this.curves) {
      const geometry = this.generateGeometry(curvePoints);
      const material = this.generateMaterial(color);
      const mesh = this.generateMesh(geometry, material);

      this.geometries.push(geometry);
      this.materials.push(material);
      this.meshes.push(mesh);

      this.changeableMaterials.push([
        { type: EMaterialType.NORMAL, material },
        {
          type: EMaterialType.WITH_TEXTURE,
          material: this.generateMaterialWithTexture(color),
        },
      ]);
    }
  }

  protected generateMesh(
    geometry: TubeGeometry,
    material: ShaderMaterial
  ): Mesh {
    const mesh = new Mesh(geometry, material);
    mesh.renderOrder = 0;
    mesh.name = `${MESH_NAME} (${this.meshes.length + 1})`;
    return mesh;
  }
}
