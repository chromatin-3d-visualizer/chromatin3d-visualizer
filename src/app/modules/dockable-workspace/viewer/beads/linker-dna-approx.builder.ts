/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Bead from '../../../../shared/models/bead.model';
import { INucleosomeDNAData } from '../../../../core/services/session-mananger/models/nucleosome-dna-data.interface';
import {
  Box3,
  CylinderGeometry,
  InstancedMesh,
  Object3D,
  Quaternion,
  ShaderMaterial,
  Texture,
  Vector3,
} from 'three';
import { DEFAULT_COLOR_LINKER_DNA } from '../../../../shared/color.constants';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import NucleosomeBead from '../../../../shared/models/nucleosome-bead.model';
import DNABead from '../../../../shared/models/dna-bead.model';
import LinkerDNACurveApproxBuilder from './linker-dna-curve-approx.builder';
import InstancedModelBuilder from './instanced-model.builder';
import {
  MATERIAL_ATTRIBUTE_NAME_SELECTED,
  NUCLEOSOME_DNA_THICKNESS,
} from './models.variables';
import { BEAD_USER_NAME_LINKER_DNA } from '../../../../constants';

export default class LinkerDNAApproxBuilder extends InstancedModelBuilder<
  any,
  INucleosomeDNAData,
  CylinderGeometry,
  ShaderMaterial
> {
  public mesh: InstancedMesh<CylinderGeometry, ShaderMaterial>;
  public geometry: CylinderGeometry;
  public material: ShaderMaterial;
  protected defaultColor = DEFAULT_COLOR_LINKER_DNA;

  private readonly _geometryBoundingBox: Box3;
  private readonly _curves: Vector3[][] = [];
  private readonly _idsPerGroup: number[][] = [];
  private readonly _idsPerSegment: {
    startBeadID: number;
    endBeadID: number;
  }[] = [];
  private readonly _beads: Bead[] = [];
  private readonly _dnaBeads: DNABead[] = [];
  private _linkerDNACurveBuilder!: LinkerDNACurveApproxBuilder;
  private _drawnCurves = 0;

  public constructor(
    beads: Bead[],
    texture: Texture,
    qualityLevel: INucleosomeDNAData,
    renderSize: number,
    splicer: number[]
  ) {
    super(
      beads.filter(
        (bead) => bead instanceof DNABead || bead instanceof NucleosomeBead
      ),
      texture,
      qualityLevel,
      renderSize
    );

    this.repositionLinker(beads, splicer);
    this.geometry = this.generateGeometry();
    this.material = this.generateShaderMaterial();
    this.mesh = this.generateMesh();
    this.geometry.computeBoundingBox();
    this._geometryBoundingBox = this.geometry.boundingBox!;

    this.changeableMaterials.push(
      { type: EMaterialType.NORMAL, material: this.material },
      {
        type: EMaterialType.WITH_TEXTURE,
        material: this.generateShaderMaterialWithTexture(),
      }
    );
  }

  private static getDrawPosition(curvePoints: Vector3[]): Vector3 {
    // Determine difference between start and end point and then half it to get the middle point
    const drawPoint: Vector3 = curvePoints[curvePoints.length - 1]
      .clone()
      .sub(curvePoints[0]);
    drawPoint.divideScalar(2);

    return drawPoint.add(curvePoints[0]);
  }

  private static determineConnectionVector(curvePoints: Vector3[]): Vector3 {
    // Determine direction starting from the end point towards the start
    return curvePoints[0].clone().sub(curvePoints[curvePoints.length - 1]);
  }

  private static getRotationToAlignToAxis(connectionAxis: Vector3): Quaternion {
    const desiredAxis: Vector3 = connectionAxis.clone().normalize();

    // It has to be assumed, that the cylinder is positioned upright and facing up the Y-Axis
    const objectRotateNormal = new Vector3(0, 1, 0);

    return new Quaternion().setFromUnitVectors(objectRotateNormal, desiredAxis);
  }

  private static determineScalingFactors(
    connectionVector: Vector3,
    sizeOfModel: Vector3
  ): Vector3 {
    const scaleY = connectionVector.length() + 0.5 / sizeOfModel.y;
    // Only the height should be affected, since the cylinder is rotated such that its height equates to the length of the connection
    return new Vector3(1, scaleY, 1);
  }

  public setUpFirstStyle(defaultColor: string): void {
    if (!defaultColor) defaultColor = this.defaultColor;
    if (!this.bufferAttrColors) throw new Error('Invalid buffer colors!');
    if (!this.bufferAttrInvisible) throw new Error('Invalid buffer colors!');

    for (let i = 0; i < this.beads.length; ++i) {
      this.setSingleInstance(i);

      const correctedIndices: number[] = [];
      const indexOffset: number = this.getIndexOffset(i);
      for (let l = 0; l < this._curves[i].length - 1; ++l) {
        correctedIndices.push(l + indexOffset);
      }

      this.setConfigSpecificColors(correctedIndices, defaultColor);
    }

    this.mesh.instanceMatrix.needsUpdate = true;
    this.bufferAttrColors.needsUpdate = true;
  }

  public override reposition(): void {
    this._drawnCurves = 0;
    this.beads.forEach((_, index) => this.setSingleInstance(index));

    this.mesh.count = this._drawnCurves;
    this.mesh.instanceMatrix.needsUpdate = true;
  }

  public override setConfigSpecificColors(
    indices: number[],
    color: string
  ): void {
    const grpIndices: number[] = [];
    for (const index of indices) {
      const bead = this._dnaBeads[index];
      if (!bead) continue;

      const grpIndex = this._idsPerGroup.findIndex((beadsIdsInGrp) =>
        beadsIdsInGrp.includes(bead.id)
      );
      if (grpIndex >= 0) grpIndices.push(grpIndex);
    }

    indices = [];
    for (const grpIndex of grpIndices) {
      const indexStart = this.getIndexOffset(grpIndex);
      const len = this._curves[grpIndex].length - 1;

      indices.push(...new Array(len).fill(0).map((_, i) => indexStart + i));
    }
    super.setConfigSpecificColors(indices, color);
  }

  public repositionLinker(beads: Bead[], splicer: number[]): void {
    this._curves.splice(0);
    this._idsPerGroup.splice(0);

    this._linkerDNACurveBuilder = new LinkerDNACurveApproxBuilder(
      this.qualityLevel
    );
    this._linkerDNACurveBuilder.calculateCurves(beads, splicer);
    this._curves.push(...this._linkerDNACurveBuilder.getCurves());
    this._idsPerGroup.push(...this._linkerDNACurveBuilder.getIDsPerGroup());
    this.beads = this._curves;
    this._beads.splice(0, this._beads.length, ...beads);
    this._dnaBeads.splice(
      0,
      this._dnaBeads.length,
      ...(<DNABead[]>beads.filter((b) => b instanceof DNABead))
    );
  }

  public getSelectionTexts(): string[] {
    const result: string[] = [];
    const attr = this.geometry.getAttribute(MATERIAL_ATTRIBUTE_NAME_SELECTED);
    for (let i = 0; i < attr.count; ++i) {
      const grpIndex = this.getGroupIndexFromMeshIndex(i);
      const selectionVal = attr.getX(i);
      if (selectionVal === 0.0) continue;

      result.push(
        `${BEAD_USER_NAME_LINKER_DNA} Index: ${grpIndex} (Bead IDs: ${this._idsPerGroup[
          grpIndex
        ]
          .filter(
            (b) =>
              !(this._beads.find((be) => be.id === b) instanceof NucleosomeBead)
          )
          .join(', ')})`
      );
    }

    return result;
  }

  protected setSpecificVisibilityFromEmptyList(isVis: boolean): void {
    if (!this.bufferAttrInvisible) return;

    const visibility: number = isVis ? 1 : 0;

    for (let index = 0; index < this.bufferAttrInvisible.count; ++index) {
      this.bufferAttrInvisible.setX(index, visibility);
    }
  }

  protected setSpecificVisibilityFromList(ids: number[], isVis: boolean): void {
    if (!this.bufferAttrInvisible) return;

    const bufferAttrCount = this.bufferAttrInvisible.count;
    const remainingIDs: number[] = [];
    const desiredVisibility = isVis ? 0 : 1;
    const inverseVisibility = !isVis ? 0 : 1;

    for (let i = 0; i < this._idsPerSegment.length; ++i) {
      const segment = this._idsPerSegment[i];
      const startBeadIndex = ids.indexOf(segment.startBeadID);
      const endBeadIndex = ids.indexOf(segment.endBeadID);

      if (startBeadIndex < 0 || endBeadIndex < 0) {
        remainingIDs.push(i);
        continue;
      }

      this.bufferAttrInvisible.setX(i, desiredVisibility);
    }

    for (const id of remainingIDs) {
      if (id >= bufferAttrCount) continue;
      this.bufferAttrInvisible.setX(id, inverseVisibility);
    }
  }

  protected generateGeometry(): CylinderGeometry {
    const geometry = new CylinderGeometry(
      1,
      1,
      NUCLEOSOME_DNA_THICKNESS,
      this.qualityLevel.nucleosomeDNARadiusSegments
    );

    this.addAttributesToGeometry(geometry);
    return geometry;
  }

  protected generateMesh(): InstancedMesh<CylinderGeometry, ShaderMaterial> {
    const mesh = super.generateMesh();
    mesh.renderOrder = 3;

    return mesh;
  }

  protected setSingleInstance(index: number): void {
    let curveOffset = this.getIndexOffset(index);

    const currentCurve = this._curves[index];

    for (let curIndex = 0; curIndex < currentCurve.length - 1; ++curIndex) {
      const currentPoints: [Vector3, Vector3] = [
        currentCurve[curIndex],
        currentCurve[curIndex + 1],
      ];
      const tmpScratchObject3D: Object3D<Event> = new Object3D<Event>();
      const drawPosition: Vector3 =
        LinkerDNAApproxBuilder.getDrawPosition(currentPoints);
      const connectionAxis: Vector3 =
        LinkerDNAApproxBuilder.determineConnectionVector(currentPoints);
      const rotation: Quaternion =
        LinkerDNAApproxBuilder.getRotationToAlignToAxis(connectionAxis);
      const scale: Vector3 = LinkerDNAApproxBuilder.determineScalingFactors(
        connectionAxis,
        this._geometryBoundingBox.getSize(new Vector3())
      );

      tmpScratchObject3D.position.set(
        drawPosition.x,
        drawPosition.y,
        drawPosition.z
      );
      tmpScratchObject3D.setRotationFromQuaternion(rotation);
      tmpScratchObject3D.scale.set(scale.x, scale.y, scale.z);

      tmpScratchObject3D.updateMatrix();
      this._idsPerSegment.push({
        startBeadID: this._idsPerGroup[index][curIndex],
        endBeadID: this._idsPerGroup[index][curIndex + 1],
      });
      this.mesh.setMatrixAt(curveOffset++, tmpScratchObject3D.matrix);
      ++this._drawnCurves;
    }
  }

  private getIndexOffset(index: number): number {
    let curveOffset: number = 0;
    for (let i = 0; i < index; ++i) {
      curveOffset += this._curves[i].length - 1;
    }

    return curveOffset;
  }

  private getGroupIndexFromMeshIndex(index: number): number {
    let curveOffset = 0;
    for (let i = 0; i < this._curves.length; ++i) {
      const currentCurve = this._curves[i];

      for (let curIndex = 0; curIndex < currentCurve.length - 1; ++curIndex) {
        if (curveOffset++ === index) return i;
      }
    }

    return -1;
  }
}
