/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  BufferGeometry,
  CatmullRomCurve3,
  Color,
  DoubleSide,
  Material,
  Mesh,
  Object3D,
  ShaderLib,
  ShaderMaterialParameters,
  UniformsUtils,
  Vector3,
} from 'three';
import { Curve } from 'three/src/extras/core/Curve';
import Bead from '../../../../shared/models/bead.model';
import MultipleMeshBeadBuilder from './multiple-mesh-bead.builder';
import { ColorBead } from '../../../../core/services/colors/models/color-bead.model';
import { findIndexAtPos } from '../../../../shared/utilities/array.utility';
import { INucleosomeDNAData } from '../../../../core/services/session-mananger/models/nucleosome-dna-data.interface';
import LinkerDNACurveBuilder from './linker-dna-curve.builder';
import {
  MATERIAL_ATTRIBUTE_NAME_SELECTED,
  MATERIAL_REFLECTIVITY,
  MATERIAL_SHININESS,
  MATERIAL_SPECULAR,
} from './models.variables';
import { isShaderWithUniforms } from '../../../../shared/utilities/three.js.utility';
import { BEAD_USER_NAME_LINKER_DNA } from '../../../../constants';
import NucleosomeBead from '../../../../shared/models/nucleosome-bead.model';
import { IUniform } from 'three/src/renderers/shaders/UniformsLib';
import {
  colorToArgb,
  rgbToHex,
} from '../../../../shared/utilities/color.utility';
import { DEFAULT_COLOR_SELECTION } from '../../../../shared/color.constants';
import { VERTEX_PHONG_SHADER } from '../shaders/selection-color.shader.vertex';

const MESH_NAME = 'Linker DNA-Mesh';

export default abstract class LinkerDNABuilder<
  TBead extends Bead,
  TQuality extends INucleosomeDNAData,
  TGeometry extends BufferGeometry,
  TMaterial extends Material & { color?: Color }
> extends MultipleMeshBeadBuilder<TBead, TQuality, TGeometry, TMaterial> {
  protected readonly curves: Vector3[][] = [];
  protected readonly tmpScratchObject3D = new Object3D();

  private readonly _splicer: number[] = [];
  private readonly _idsPerGroup: number[][] = [];
  private readonly _invisibleGrpIds: number[] = [];
  protected readonly _linkerDNACurveBuilder: LinkerDNACurveBuilder;
  protected readonly _indicesPerGroup: number[][] = [];

  protected constructor(
    beads: TBead[],
    qualityLevel: TQuality,
    splicer: number[]
  ) {
    super(beads, qualityLevel);

    this._linkerDNACurveBuilder = new LinkerDNACurveBuilder(qualityLevel);
    this._splicer = splicer;
  }

  protected static getMaterialParameters(color: string): {
    uniforms: { [uniform: string]: IUniform };
    param: ShaderMaterialParameters;
  } {
    const colorComponents = colorToArgb(color);
    const customUniforms = UniformsUtils.merge([
      ShaderLib.phong.uniforms,
      {
        // unlike the predefined shaders, you can't just take the attribute name and define the value behind it
        // for reference, see the setting of the default values in the source code:
        // https://github.com/mrdoob/three.js/blob/r68/src/renderers/shaders/ShaderLib.js#L595
        shininess: { type: 'f', value: MATERIAL_SHININESS },
        specular: { type: 'c', value: new Color(MATERIAL_SPECULAR) },
        reflectivity: { type: 'f', value: MATERIAL_REFLECTIVITY },
        selectionColor: {
          type: 'c',
          value: new Color(DEFAULT_COLOR_SELECTION),
        },
        vertexColor: {
          type: 'c',
          value: new Color(rgbToHex(colorComponents)),
        },
        [MATERIAL_ATTRIBUTE_NAME_SELECTED]: {
          type: 'f',
          value: 0.0,
        },
        opacity: {
          type: 'f',
          value: colorComponents.a,
        },
      },
    ]);
    return {
      uniforms: customUniforms,
      param: {
        vertexShader: VERTEX_PHONG_SHADER,
        fragmentShader: ShaderLib.phong.fragmentShader,
        lights: true,
        transparent: true,
        vertexColors: true,
        side: DoubleSide,
        opacity: colorComponents.a,
        visible: colorComponents.a > 0,
      },
    };
  }

  public getSelectionTexts(): string[] {
    return this.getSelectionsWithCustomPrefix(BEAD_USER_NAME_LINKER_DNA);
  }

  public setSpecificVisibility(ids: number[], isVis: boolean): void {
    this._invisibleGrpIds.splice(0);
    let groupIndex = -1;
    for (const groupBeadIDs of this._idsPerGroup) {
      ++groupIndex;
      if (groupIndex >= this.materials.length) return;

      const invisibleCounter = groupBeadIDs.filter(
        (id) => ids.indexOf(id) >= 0
      );

      if (groupIndex >= this.materials.length) return;

      const isMaterialVis = invisibleCounter.length === 0 ? !isVis : isVis; // no idea why negated statement...
      this.setVisibilityOfMaterialFromIndex(groupIndex, isMaterialVis);
      if (!isMaterialVis) this._invisibleGrpIds.push(groupIndex);
    }
  }

  public changeDefaultColor(
    color: string,
    indicesOfOwnColor: number[] = []
  ): void {
    super.changeDefaultColor(color, indicesOfOwnColor);
    this.changeColorOfFakeDNA();
  }

  public setConfigSpecificColors(indices: number[], color: string): void {
    const grpIndices = [];
    for (
      let grpIndex = 0;
      grpIndex < this._indicesPerGroup.length;
      ++grpIndex
    ) {
      const currentGrpIndices = this._indicesPerGroup[grpIndex];

      if (
        currentGrpIndices.find((index) => indices.includes(index)) != undefined
      )
        grpIndices.push(grpIndex);
    }

    super.setConfigSpecificColors(grpIndices, color);
    this.changeColorOfFakeDNA();
  }

  public setConfigSpecificColorsFromColorBeads(colorBeads: ColorBead[]): void {
    let colorBeadIndex = 0;
    let grpIndex = 0;
    let tempIndices: number[] = [];

    while (
      colorBeadIndex < colorBeads.length &&
      grpIndex < this._indicesPerGroup.length
    ) {
      const currentColorBead = colorBeads[colorBeadIndex];
      const currentGrpIndices = this._indicesPerGroup[grpIndex];
      const isInGrp =
        currentColorBead.index >= currentGrpIndices[0] &&
        currentColorBead.index <=
          currentGrpIndices[currentGrpIndices.length - 1];
      if (isInGrp) {
        tempIndices.push(currentColorBead.index);
        ++colorBeadIndex;
      } else {
        const grpNumbersInRange = currentGrpIndices.find(
          (i) => i >= currentColorBead.index
        );
        if (grpNumbersInRange != undefined) {
          ++colorBeadIndex;
        } else {
          this.setColorOfGroup(grpIndex, tempIndices, colorBeads);
          tempIndices = [];
          ++grpIndex;
        }
      }
    }

    this.setColorOfGroup(
      grpIndex < this._indicesPerGroup.length
        ? grpIndex
        : this._indicesPerGroup.length - 1,
      tempIndices,
      colorBeads
    );

    this.changeColorOfFakeDNA();
  }

  protected generateMeshes(color: string): void {
    for (const curvePoints of this.curves) {
      const geometry = this.generateGeometry(curvePoints);
      const material = this.generateMaterial(color);
      const mesh = this.generateMesh(geometry, material);

      this.geometries.push(geometry);
      this.materials.push(material);
      this.meshes.push(mesh);
    }
  }

  protected generateMesh(geometry: TGeometry, material: TMaterial): Mesh {
    const mesh = new Mesh(geometry, material);
    mesh.renderOrder = 0;
    mesh.name = `${MESH_NAME} (${this.meshes.length + 1})`;
    return mesh;
  }

  protected generateCurveFromPoints(curvePoints: Vector3[]): Curve<Vector3> {
    return new CatmullRomCurve3(curvePoints);
  }

  protected isVisible(index: number, material: TMaterial): boolean {
    return (
      !this._invisibleGrpIds.includes(index) && super.isVisible(index, material)
    );
  }

  protected setVisibilityOfMaterialFromIndex(
    index: number,
    isVis: boolean
  ): void {
    this.materials[index].visible = isVis;
  }

  protected calculateCurves(): void {
    this._linkerDNACurveBuilder.calculateCurves(this.beads, this._splicer);
    this.curves.push(...this._linkerDNACurveBuilder.getCurves());
    this._indicesPerGroup.push(
      ...this._linkerDNACurveBuilder.getIndicesPerGroup()
    );
    this._idsPerGroup.push(...this._linkerDNACurveBuilder.getIDsPerGroup());
  }

  protected getSelectionsWithCustomPrefix(prefix: string): string[] {
    const result: string[] = [];
    for (let i = 0; i < this.materials.length; ++i) {
      const material = this.materials[i];
      if (!isShaderWithUniforms(material)) continue;

      if (material.uniforms[MATERIAL_ATTRIBUTE_NAME_SELECTED].value !== 1.0)
        continue;

      result.push(
        `${prefix} Index: ${i} (Bead IDs: ${this._idsPerGroup[i]
          .filter(
            (b) =>
              !(this.beads.find((be) => be.id === b) instanceof NucleosomeBead)
          )
          .join(', ')})`
      );
    }

    return result;
  }

  /**
   * Changes the colors of the linker DNA, which does not really exist.
   *
   * In PBC mode, linker DNAs are partially split so that they are not stretched throughout the container.
   * This causes more Linker DNA objects to be present than actually exist in the model. In previous versions this was also displayed, i.e.
   * the Bead Explorer suddenly had more linker DNA than before when switching to PBC mode. However, this complicates the functionality for
   * coloring. Furthermore, this information itself is incorrect. For this reason it was removed and the new objects are now "hidden" from
   * the user.
   * @private
   */
  private changeColorOfFakeDNA(): void {
    let indexOfFakeNewLinkerDNA = -1;
    while (
      (indexOfFakeNewLinkerDNA = findIndexAtPos(
        this._indicesPerGroup,
        (i) => i.length === 0,
        indexOfFakeNewLinkerDNA + 1
      )) >= 0
    ) {
      // has no previous -> NOT STOP while, just skip - but should NOT happens
      if (indexOfFakeNewLinkerDNA === 0) continue;

      this.materials[indexOfFakeNewLinkerDNA].color =
        this.materials[indexOfFakeNewLinkerDNA - 1].color?.clone();
      this.materials[indexOfFakeNewLinkerDNA].opacity =
        this.materials[indexOfFakeNewLinkerDNA - 1].opacity;
      this.materials[indexOfFakeNewLinkerDNA].needsUpdate = true;
    }
  }

  private setColorOfGroup(
    grpIndex: number,
    tempIndices: number[],
    colorBeads: ColorBead[]
  ): void {
    if (tempIndices.length === 0) return;

    const colorBead = colorBeads.find((b) => b.index === tempIndices[0]);
    if (!colorBead) {
      console.warn(
        'The bead could not be found by which the group should be colored - this should not happen! As a result of the error, the coloring may be incorrect!'
      );
    } else {
      super.setConfigSpecificColors([grpIndex], colorBead.color);
    }
  }

  protected abstract generateMaterial(color: string): TMaterial;
  protected abstract generateGeometry(curvePoints: Vector3[]): TGeometry;
}
