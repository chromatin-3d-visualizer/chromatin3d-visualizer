/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Object3D, Vector3 } from 'three';
import CohesinHeadBead from '../../../../shared/models/cohesin-head-bead.model';
import DNABead from '../../../../shared/models/dna-bead.model';
import SoftwareNucleosomeBead from '../../../../shared/models/sw-nucleosome-bead.model';
import Bead from '../../../../shared/models/bead.model';
import { INucleosomeDNAData } from '../../../../core/services/session-mananger/models/nucleosome-dna-data.interface';
import DoubleHelixNucleosomeDNABuilder from './double-helix-nucleosome-dna.builder';
import { convertBufferAttrToVector3List } from '../../../../shared/utilities/three.js.utility';

export default class LinkerDNACurveBuilder {
  private _curves: Vector3[][] = [];
  private _indicesPerGroup: number[][] = [];
  private _idsPerGroup: number[][] = [];
  private readonly _qualityLevel: INucleosomeDNAData;
  private _currentNucIDs: { start: number; end: number } = {
    start: -1,
    end: -1,
  };
  private readonly _curvesNucIds: { start: number; end: number }[] = [];

  private readonly _tempIndices: number[] = [];
  private readonly _tempIDs: number[] = [];
  private readonly _tempPoints: Vector3[] = [];

  public constructor(qualityLevel: INucleosomeDNAData) {
    this._qualityLevel = qualityLevel;
  }

  public getCurves(): Vector3[][] {
    return this._curves;
  }

  public getIndicesPerGroup(): number[][] {
    return this._indicesPerGroup;
  }

  public getIDsPerGroup(): number[][] {
    return this._idsPerGroup;
  }

  public getCurvesNucIDs(): { start: number; end: number }[] {
    return this._curvesNucIds;
  }

  public calculateCurves(beads: Bead[], splicer: number[]): void {
    this.resetTempArrays();

    this._curves.splice(0);
    this._indicesPerGroup.splice(0);
    this._idsPerGroup.splice(0);
    const tempScratchObject3D = new Object3D();

    let beadIndex = 0;
    let dnaBeadIndex = 0;
    let lastPointsOfNuc: Vector3[] = [];
    const nucleosomeDNAGeo = this._qualityLevel.normalNucleosomeDNA;

    for (const bead of beads) {
      if (bead instanceof CohesinHeadBead) {
        ++beadIndex;
        continue;
      } else if (bead instanceof DNABead) {
        this._tempIndices.push(dnaBeadIndex++);
      }

      if (splicer.find((index) => index === beadIndex)) {
        this.addPointsToCurve();
        this.resetTempArrays(); // in case the tempPoints are lower than 1
        lastPointsOfNuc = [];
      }

      ++beadIndex;

      if (bead instanceof SoftwareNucleosomeBead) {
        const nucleosome: SoftwareNucleosomeBead = bead;
        const geometry = nucleosomeDNAGeo.clone();
        geometry.applyMatrix4(
          DoubleHelixNucleosomeDNABuilder.setRotation(
            nucleosome,
            tempScratchObject3D
          ).matrix
        );
        const vertices = convertBufferAttrToVector3List(
          geometry.getAttribute('position')
        );

        this._tempPoints.push(...this.getFirstPointsOfNuc(vertices));
        this._tempIDs.push(bead.id);
        this._currentNucIDs.end = beadIndex - 1;
        this.addPointsToCurve();
        this.resetTempArrays(); // in case the tempPoints are lower than 1
        lastPointsOfNuc = this.getLastPointsOfNuc(vertices);
        this._currentNucIDs.start = beadIndex - 1;
        // Since the nuc is present in two subsequent groups of linker dna, the id is added again
        this._tempIDs.push(bead.id);
        geometry.dispose();
      } else {
        this._tempPoints.push(...lastPointsOfNuc);
        this._tempIDs.push(bead.id);
        this._tempPoints.push(
          new Vector3(
            bead.drawPosition.x,
            bead.drawPosition.y,
            bead.drawPosition.z
          )
        );
        lastPointsOfNuc = [];
      }
    }

    this.addPointsToCurve();
  }

  protected getFirstPointsOfNuc(vertices: Vector3[]): Vector3[] {
    return [vertices[0], vertices[1]];
  }

  protected getLastPointsOfNuc(vertices: Vector3[]): Vector3[] {
    return [vertices[vertices.length - 2], vertices[vertices.length - 1]];
  }

  private resetTempArrays(): void {
    this._tempPoints.splice(0);
    this._tempIDs.splice(0);
    this._tempIndices.splice(0);
    this._currentNucIDs.start = -1;
    this._currentNucIDs.end = -1;
  }

  private addPointsToCurve(): void {
    if (this._tempPoints.length <= 1) return;

    this._curves.push([...this._tempPoints]);
    this._idsPerGroup.push([...this._tempIDs]);
    if (this._tempIndices.length > 0)
      this._indicesPerGroup.push([...this._tempIndices]);
    else this._indicesPerGroup.push([]);

    this._curvesNucIds.push({ ...this._currentNucIDs });
    this._currentNucIDs.start = -1;
    this._currentNucIDs.end = -1;

    this.resetTempArrays();
  }
}
