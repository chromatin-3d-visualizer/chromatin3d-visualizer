/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, OnDestroy } from '@angular/core';
import { EControlType } from '../viewer/models/control-type.enum';
import { ViewService } from '../../../core/services/view/view.service';
import { BaseTabComponentDirective } from '../../../shared/directives/base-tab-component.directive';
import { ECameraSetter } from '../viewer/models/camera-setter.enum';
import { WorkspaceService } from '../../../core/services/workspace.service';
import { getValueOfEnumKey } from '../../../shared/utilities/enum.utility';

@Component({
  selector: 'trj-camera-settings',
  templateUrl: './camera-settings.component.html',
  styleUrls: ['./camera-settings.component.scss'],
})
export class CameraSettingsComponent
  extends BaseTabComponentDirective
  implements OnDestroy
{
  public selectedControlType = EControlType.firstPersonPerspective.toString();
  public selectedCameraSetter = ECameraSetter.none.toString();

  public readonly CONTROL_TYPES = [
    {
      value: EControlType.firstPersonPerspective.toString(),
      text: 'First Person Perspective',
    },
    {
      value: EControlType.fixedCenter.toString(),
      text: 'Rotate Around The Center',
    },
  ];

  public readonly CAMERA_SETTER = [
    {
      value: ECameraSetter.none.toString(),
      text: 'None',
    },
    {
      value: ECameraSetter.toCenter.toString(),
      text: 'Set to Center of Mass',
    },
    {
      value: ECameraSetter.keepDiff.toString(),
      text: 'Keep Distance to Center',
    },
  ];

  private _ignoreControlTypeChangedEvent = false;
  private _ignoreCameraSetterTypeChangedEvent = false;

  constructor(
    private readonly _viewService: ViewService,
    public readonly workspaceService: WorkspaceService
  ) {
    super();

    this.subscribes.push(
      this._viewService.controls$.subscribe((type) => {
        if (this._ignoreControlTypeChangedEvent) {
          this._ignoreControlTypeChangedEvent = false;
          return;
        }

        this.selectedControlType = type.toString();
      })
    );

    this.subscribes.push(
      this._viewService.cameraSetType$.subscribe((setter) => {
        if (this._ignoreCameraSetterTypeChangedEvent) {
          this._ignoreCameraSetterTypeChangedEvent = false;
          return;
        }

        this.selectedCameraSetter = setter.toString();
      })
    );
  }

  changeControls(type: string): void {
    const enumVal = getValueOfEnumKey<EControlType>(EControlType, type);
    if (enumVal === undefined) {
      console.error('Invalid enum value for the control type');
      return;
    }

    this._ignoreControlTypeChangedEvent = true;
    this._viewService.setControlType(enumVal);
  }

  changeSetter(type: string): void {
    const enumVal = getValueOfEnumKey<ECameraSetter>(ECameraSetter, type);
    if (enumVal === undefined) {
      console.error('Invalid enum value for the control type');
      return;
    }

    this._ignoreCameraSetterTypeChangedEvent = true;
    this._viewService.setCameraSetterType(enumVal);
  }

  triggerSetCameraToCenterOfMass(): void {
    this._viewService.setCameraToCenterOfMass$.next(null);
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }
}
