/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { ColorPickerModule } from 'ngx-color-picker';
import { PbcComponent } from './pbc.component';
import { MicroscopeSlideComponent } from './microscope-slide/microscope-slide.component';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [PbcComponent, MicroscopeSlideComponent],
  imports: [
    CommonModule,
    SharedModule,
    MatCheckboxModule,
    ColorPickerModule,
    MatButtonModule,
    MatIconModule,
  ],
  exports: [PbcComponent],
})
export class PbcModule {}
