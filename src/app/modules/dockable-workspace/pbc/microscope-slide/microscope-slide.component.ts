/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Input, OnDestroy } from '@angular/core';
import { SubscribeEvents } from '../../../../core/classes/subscribe-events';
import { DEFAULT_COLOR_MICROSCOPE_SLIDE_BOX } from '../../../../shared/color.constants';
import { PbcService } from '../../../../core/services/pbc.service';
import { WorkspaceService } from '../../../../core/services/workspace.service';

@Component({
  selector: 'trj-microscope-slide[isSmall]',
  templateUrl: './microscope-slide.component.html',
  styleUrls: ['./microscope-slide.component.scss'],
})
export class MicroscopeSlideComponent
  extends SubscribeEvents
  implements OnDestroy
{
  public readonly DEFAULT_COLOR_MICROSCOPE_SLIDE_BOX =
    DEFAULT_COLOR_MICROSCOPE_SLIDE_BOX;

  @Input()
  public isSmall = false;

  private _maxIndex = 0;

  public get microscopeSlideMaxIndex(): number {
    return this._maxIndex;
  }

  constructor(
    public readonly pbcService: PbcService,
    private readonly _workspaceService: WorkspaceService
  ) {
    super();

    this.subscribes.push(
      this._workspaceService.config$.subscribe(() => {
        this._maxIndex = this.pbcService.getMicroscopeSlideMaxIndex();
      }),
      this.pbcService.microscopeSlideCutSize$.subscribe(
        () => (this._maxIndex = this.pbcService.getMicroscopeSlideMaxIndex())
      )
    );
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  changeMicroscopeSlideBoxColor(newColor: string): void {
    this.pbcService.microscopeSlideBoxColor = newColor;
  }

  setFirstIndex(): void {
    this.pbcService.microscopeSlideIndex = 0;
  }

  setPreviousIndex(): void {
    this.pbcService.microscopeSlideIndex -= 1;
  }

  setNextIndex(): void {
    this.pbcService.microscopeSlideIndex += 1;
  }

  setLastIndex(): void {
    this.pbcService.microscopeSlideIndex = this._maxIndex;
  }

  changeMicroscopeSlideCubeIndex(index: number | null): void {
    this.pbcService.microscopeSlideIndex = index ?? 0;
  }

  changeMicroscopeSlideCubeCutSize(size: number | null): void {
    this.pbcService.microscopeSlideCutSize = size ?? 0;
  }
}
