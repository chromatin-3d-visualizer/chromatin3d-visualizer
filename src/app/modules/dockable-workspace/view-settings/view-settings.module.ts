/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ColorPickerModule } from 'ngx-color-picker';
import { ViewSettingsComponent } from './view-settings.component';
import { ColorsComponent } from './colors/colors.component';
import { VisualizationComponent } from './visualization/visualization.component';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
    ViewSettingsComponent,
    ColorsComponent,
    VisualizationComponent,
  ],
  imports: [CommonModule, ColorPickerModule, SharedModule, MatCheckboxModule],
  exports: [ViewSettingsComponent],
})
export class ViewSettingsModule {}
