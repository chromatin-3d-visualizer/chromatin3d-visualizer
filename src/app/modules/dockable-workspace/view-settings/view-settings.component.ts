/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject } from '@angular/core';
import { BaseTabComponentDirective } from '../../../shared/directives/base-tab-component.directive';
import { ComponentContainer } from 'golden-layout';

@Component({
  selector: 'trj-view-settings',
  templateUrl: './view-settings.component.html',
  styleUrls: ['./view-settings.component.scss'],
})
export class ViewSettingsComponent extends BaseTabComponentDirective {
  constructor(
    @Inject(BaseTabComponentDirective.GOLDEN_LAYOUT_CONTAINER_INJECTION_TOKEN)
    protected readonly container: ComponentContainer
  ) {
    super();

    this.subscribeTabResizeForBreakpoints(container);
  }
}
