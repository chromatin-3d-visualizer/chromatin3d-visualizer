/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentItemConfig, LayoutConfig } from 'golden-layout';
import * as TabNames from './tab-names.constants';

export const defaultLayout: LayoutConfig = {
  settings: {
    responsiveMode: 'none',
  },
  dimensions: {
    minItemWidth: 200,
    minItemHeight: 150,
    headerHeight: 30,
  },
  root: {
    type: 'row',
    content: [
      {
        width: 15,
        type: 'column',
        content: [
          {
            type: 'stack',
            content: [
              {
                type: 'component',
                title: TabNames.TAB_NAME_VIEW_SETTINGS,
                componentType: TabNames.TAB_JSON_NAME_VIEW_SETTINGS,
              } as ComponentItemConfig,
              {
                type: 'component',
                title: TabNames.TAB_NAME_EXPLORER_HISTONE_OCTAMER,
                componentType: TabNames.TAB_JSON_NAME_EXPLORER_NUCLEOSOMES,
              } as ComponentItemConfig,
            ],
          },
          {
            title: TabNames.TAB_NAME_PBC,
            type: 'component',
            componentType: TabNames.TAB_JSON_NAME_PBC,
          },
        ],
      },
      {
        width: 55,
        title: TabNames.TAB_NAME_TRAJECTORY_VIEWER,
        type: 'component',
        componentType: TabNames.TAB_JSON_NAME_TRAJECTORY_VIEWER,
      } as ComponentItemConfig,
      {
        width: 20,
        type: 'column',
        content: [
          {
            height: 40,
            type: 'component',
            title: TabNames.TAB_NAME_CONFIGURATION_PLAYER,
            componentType: TabNames.TAB_JSON_NAME_CONFIGURATION_PLAYER,
          },
          {
            title: TabNames.TAB_NAME_CAMERA_SETTINGS,
            type: 'component',
            componentType: TabNames.TAB_JSON_NAME_CAMERA_SETTINGS,
          },
        ],
      },
    ],
  },
};
