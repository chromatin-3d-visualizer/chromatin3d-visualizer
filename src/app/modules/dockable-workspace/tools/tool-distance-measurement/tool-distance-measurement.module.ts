/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { ColorPickerModule } from 'ngx-color-picker';
import { ToolDistanceMeasurementComponent } from './tool-distance-measurement.component';
import { SharedModule } from '../../../../shared/shared.module';
import { VisualizationOptionsComponent } from './visualization-options/visualization-options.component';

@NgModule({
  declarations: [
    ToolDistanceMeasurementComponent,
    VisualizationOptionsComponent,
  ],
  imports: [CommonModule, MatButtonModule, ColorPickerModule, SharedModule],
  exports: [ToolDistanceMeasurementComponent],
})
export class ToolDistanceMeasurementModule {}
