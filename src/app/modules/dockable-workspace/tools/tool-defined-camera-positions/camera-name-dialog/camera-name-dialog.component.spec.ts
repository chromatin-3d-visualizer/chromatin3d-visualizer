/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CameraNameDialogComponent } from './camera-name-dialog.component';
import { MATERIAL_IMPORTS } from '../../../../../core/services/__mocks__/imports';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogData } from '../models/dialog-data.model';
import Spy = jasmine.Spy;
import createSpy = jasmine.createSpy;

describe('CameraNameDialogComponent', () => {
  let component: CameraNameDialogComponent;
  let fixture: ComponentFixture<CameraNameDialogComponent>;
  let dialogRef: { close: Spy<() => void> };

  beforeEach(async () => {
    dialogRef = { close: createSpy('close') };
    await TestBed.configureTestingModule({
      declarations: [CameraNameDialogComponent],
      imports: [...MATERIAL_IMPORTS],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: { name: 'test', takenNames: ['test 2'] } as DialogData,
        },
        {
          provide: MatDialogRef,
          useValue: dialogRef,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CameraNameDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
