/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { generateUUID } from 'three/src/math/MathUtils';
import { BaseTabComponentDirective } from '../../../../shared/directives/base-tab-component.directive';
import { DefinedCamera } from './models/defined-camera.model';
import { ViewService } from '../../../../core/services/view/view.service';
import { CameraNameDialogComponent } from './camera-name-dialog/camera-name-dialog.component';
import { DialogData } from './models/dialog-data.model';
import { ConfirmDialogComponent } from '../../../../shared/components/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogData } from '../../../../shared/components/confirm-dialog/models/confirm-dialog-data.model';
import { AnimationService } from '../../../../core/services/animation/animation.service';
import { getAnimationsFromDefinedCamera } from '../../../../shared/utilities/animation.utility';

const DIALOG_MAXIMUM_WIDTH = '550px';
const DIALOG_WIDTH = '50vw';

@Component({
  selector: 'trj-tool-defined-camera-positions',
  templateUrl: './tool-defined-camera-positions.component.html',
  styleUrls: ['./tool-defined-camera-positions.component.scss'],
})
export class ToolDefinedCameraPositionsComponent
  extends BaseTabComponentDirective
  implements OnDestroy
{
  public readonly cameras$ = new BehaviorSubject<DefinedCamera[]>([]);

  constructor(
    private readonly _viewService: ViewService,
    private readonly _animationService: AnimationService,
    private readonly _dialog: MatDialog
  ) {
    super();

    this.subscribes.push(
      this._viewService.definedCameras$.subscribe((cameras) => {
        this.cameras$.next(cameras.filter((c) => !c.wasGenerated));
      })
    );
  }

  setCamera(camera: DefinedCamera): void {
    this._viewService.cameraSetter$$.next(camera.camera);
  }

  addCamera(): void {
    const dialogRef = this._dialog.open(CameraNameDialogComponent, {
      disableClose: true,
      width: DIALOG_WIDTH,
      maxWidth: DIALOG_MAXIMUM_WIDTH,
      data: {
        name: null,
        takenNames: this.cameras$.value.map((c) => c.name),
      } as DialogData,
    });

    dialogRef
      .afterClosed()
      .pipe(filter((r) => r && r.name))
      .subscribe((result: DialogData) => {
        this._viewService.cameraGetter$$.next((camera) =>
          this.addCameraToList({
            name: result.name!,
            camera,
            id: generateUUID(),
            wasGenerated: false,
          })
        );
      });
  }

  renameCamera(camera: DefinedCamera): void {
    const dialogRef = this._dialog.open(CameraNameDialogComponent, {
      disableClose: true,
      width: DIALOG_WIDTH,
      maxWidth: DIALOG_MAXIMUM_WIDTH,
      data: {
        name: camera.name,
        takenNames: this.cameras$.value.map((c) => c.name),
      } as DialogData,
    });

    dialogRef
      .afterClosed()
      .pipe(filter((r) => r && r.name))
      .subscribe((result: DialogData) => {
        camera.name = result.name!;
        this._viewService.setDefinedCameras(this.cameras$.value);
      });
  }

  removeCamera(camera: DefinedCamera): void {
    const usedInAnimations = getAnimationsFromDefinedCamera(
      camera.id,
      this._animationService.animations
    );

    let text = `Do you really want to remove the defined camera position "${camera.name}"?`;

    if (usedInAnimations.length > 0) {
      if (usedInAnimations.length === 1) {
        text += `<br /><br /><strong>Attention:</strong> The camera point is used in the animation "${usedInAnimations[0].name}".`;
      } else {
        text += `<br /><br /><strong>Attention:</strong> The camera point is used in ${usedInAnimations.length} animations.`;
      }
    }

    const dialogRef = this._dialog.open(ConfirmDialogComponent, {
      disableClose: true,
      width: DIALOG_WIDTH,
      maxWidth: DIALOG_MAXIMUM_WIDTH,
      data: {
        title: 'Are you sure?',
        text,
        buttonPositiveColor: 'warn',
      } as ConfirmDialogData,
    });

    dialogRef
      .afterClosed()
      .pipe(filter((r) => r))
      .subscribe(() => {
        this._viewService.setDefinedCameras(
          this.cameras$.value.filter((c) => c !== camera)
        );
      });
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  private addCameraToList(cameraObject: DefinedCamera): void {
    this._viewService.setDefinedCameras([...this.cameras$.value, cameraObject]);
  }
}
