/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolCameraAnimatorComponent } from './tool-camera-animator.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { SharedModule } from '../../../../shared/shared.module';
import { PlayerComponent } from './player/player.component';
import { EditorComponent } from './editor/editor.component';
import { NameDialogComponent } from './name-dialog/name-dialog.component';
import { KeyframeCreationDialogComponent } from './editor/keyframe-creation-dialog/keyframe-creation-dialog.component';
import { MatMenuModule } from '@angular/material/menu';
import { RotationGeneratorComponent } from './editor/rotation-generator/rotation-generator.component';

@NgModule({
  declarations: [
    ToolCameraAnimatorComponent,
    PlayerComponent,
    EditorComponent,
    NameDialogComponent,
    KeyframeCreationDialogComponent,
    RotationGeneratorComponent,
  ],
  imports: [
    CommonModule,
    DragDropModule,
    MatIconModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    SharedModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    MatMenuModule,
    ReactiveFormsModule,
    MatCheckboxModule,
  ],
  providers: [],
  exports: [ToolCameraAnimatorComponent],
})
export class ToolCameraAnimatorModule {}
