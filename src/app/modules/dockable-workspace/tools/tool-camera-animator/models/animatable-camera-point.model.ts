/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Euler, Vector3 } from 'three';

export class AnimatableCameraPoint {
  private _position: Vector3;
  private _upVector: Vector3;
  private _rotation: Euler;
  private _targetPosition: Vector3;

  public get position(): Vector3 {
    return this._position;
  }

  public get upVector(): Vector3 {
    return this._upVector;
  }

  public get rotation(): Euler {
    return this._rotation;
  }

  public get targetPosition(): Vector3 {
    return this._targetPosition;
  }

  public constructor(
    pos: Vector3,
    upVec: Vector3,
    rot: Euler,
    targetPos: Vector3
  ) {
    this._position = pos.clone();
    this._upVector = upVec.clone();
    this._rotation = rot.clone();
    this._targetPosition = targetPos.clone();
  }

  public divideScalar(factor: number): this {
    if (factor === 0) {
      console.warn('Invalid divide scalar');
      return this;
    }

    this._position.divideScalar(factor);
    this._upVector.divideScalar(factor);
    this._rotation.set(
      this._rotation.x / factor,
      this._rotation.y / factor,
      this._rotation.z / factor
    );
    this._targetPosition.divideScalar(factor);

    return this;
  }

  public multiplyScalar(factor: number): this {
    this._position.multiplyScalar(factor);
    this._upVector.multiplyScalar(factor);
    this._rotation.set(
      this._rotation.x * factor,
      this._rotation.y * factor,
      this._rotation.z * factor
    );
    this._targetPosition.multiplyScalar(factor);

    return this;
  }

  public sub(item: AnimatableCameraPoint): this {
    this._position.sub(item._position);
    this._upVector.sub(item._upVector);
    this._rotation.set(
      this._rotation.x - item._rotation.x,
      this._rotation.y - item._rotation.y,
      this._rotation.z - item._rotation.z
    );
    this._targetPosition.sub(item._targetPosition);

    return this;
  }

  public add(item: AnimatableCameraPoint): this {
    this._position.add(item._position);
    this._upVector.add(item._upVector);
    this._rotation.set(
      this._rotation.x + item._rotation.x,
      this._rotation.y + item._rotation.y,
      this._rotation.z + item._rotation.z
    );
    this._targetPosition.add(item._targetPosition);

    return this;
  }

  public clone(): AnimatableCameraPoint {
    return new AnimatableCameraPoint(
      this._position,
      this._upVector,
      this._rotation,
      this._targetPosition
    );
  }
}
