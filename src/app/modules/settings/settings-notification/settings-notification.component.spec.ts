/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MATERIAL_IMPORTS } from '../../../core/services/__mocks__/imports';
import { SettingsNotificationComponent } from './settings-notification.component';
import { By } from '@angular/platform-browser';
import { MatCheckbox } from '@angular/material/checkbox';
import { SettingsService } from '../../../core/services/settings/settings.service';
import DateDiff from 'date-diff';

describe('SettingsNotificationComponent', () => {
  let component: SettingsNotificationComponent;
  let fixture: ComponentFixture<SettingsNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SettingsNotificationComponent],
      imports: [...MATERIAL_IMPORTS],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should enable update notifications', () => {
    const spy = spyOn(SettingsService, 'writeLastVisitDateToLocalStorage');

    const cb = fixture.debugElement.query(By.directive(MatCheckbox));
    cb.triggerEventHandler('change', { checked: true });
    fixture.detectChanges();

    expect(spy.calls.count()).toEqual(1);

    const time = spy.calls.argsFor(0);
    console.log(time[0]);
    expect(Math.abs(new DateDiff(time[0], new Date()).days())).toBeLessThan(1);
  });

  it('should disable update notifications', () => {
    const spy = spyOn(SettingsService, 'writeLastVisitDateToLocalStorage');

    const cb = fixture.debugElement.query(By.directive(MatCheckbox));
    cb.triggerEventHandler('change', { checked: false });
    fixture.detectChanges();

    expect(spy.calls.count()).toEqual(1);

    const time = spy.calls.argsFor(0);
    expect(Math.abs(new DateDiff(time[0], new Date()).days())).toBeGreaterThan(
      5
    );
  });
});
