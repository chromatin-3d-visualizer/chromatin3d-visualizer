/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component } from '@angular/core';
import { SettingsService } from '../../../core/services/settings/settings.service';

/**
 * Specifies the number of years that should add to the current date.
 * This affects a workaround to hide the notification.
 */
const HIDE_UPDATE_NOTIFICATION_FUTURE_YEARS = 100; // in years

@Component({
  selector: 'trj-settings-notification',
  templateUrl: './settings-notification.component.html',
  styleUrls: [
    '../settings-base.component.scss',
    './settings-notification.component.scss',
  ],
})
export class SettingsNotificationComponent {
  public showUpdateNotification = true;

  constructor() {
    this.showUpdateNotification =
      !SettingsService.getLastVisitDateFromLocalStorage().isInFuture;
  }

  changeShowUpdateNotification(isChecked: boolean): void {
    if (isChecked) {
      const currentDate = new Date();
      SettingsService.writeLastVisitDateToLocalStorage(currentDate);
    } else {
      const futureDate = new Date();
      futureDate.setFullYear(
        futureDate.getFullYear() + HIDE_UPDATE_NOTIFICATION_FUTURE_YEARS
      );
      SettingsService.writeLastVisitDateToLocalStorage(futureDate);
    }
  }
}
