/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { SettingsComponent } from './settings.component';
import {
  APPLICATION_IMPORTS,
  MATERIAL_IMPORTS,
} from '../../core/services/__mocks__/imports';
import { SettingsViewComponent } from './settings-view/settings-view.component';
import { SettingsPerformanceComponent } from './settings-performance/settings-performance.component';
import { SettingsBehaviourComponent } from './settings-behaviour/settings-behaviour.component';
import { SettingsNotificationComponent } from './settings-notification/settings-notification.component';
import { CategoryComponent } from './settings-performance/category/category.component';
import { CategoryItemComponent } from './settings-performance/category/category-item/category-item.component';
import { CategoryItemNumberComponent } from './settings-performance/category/category-item/category-item-number/category-item-number.component';
import { CategoryItemOptionComponent } from './settings-performance/category/category-item/category-item-option/category-item-option.component';
import { WorkspaceService } from '../../core/services/workspace.service';
import { WorkspaceService as MockWorkspaceService } from '../../core/services/__mocks__/workspace.service';
import { SettingsComponentService } from './settings-component.service';
import { AngularSplitModule } from 'angular-split';
import { SettingsPreviewComponent } from './settings-performance/settings-preview/settings-preview.component';
import { AssetsService } from '../../core/services/assets/assets.service';
import { AssetsService as MockAssetsService } from '../../core/services/__mocks__/assets.service';
import { SettingsAppearanceComponent } from './settings-appearance/settings-appearance.component';
import { SettingsExportComponent } from './settings-export/settings-export.component';

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        SettingsComponent,
        SettingsViewComponent,
        SettingsPerformanceComponent,
        SettingsAppearanceComponent,
        SettingsBehaviourComponent,
        SettingsNotificationComponent,
        SettingsExportComponent,
        CategoryComponent,
        CategoryItemComponent,
        CategoryItemNumberComponent,
        CategoryItemOptionComponent,
        SettingsPreviewComponent,
      ],
      imports: [
        ...APPLICATION_IMPORTS,
        ...MATERIAL_IMPORTS,
        AngularSplitModule,
      ],
      providers: [
        SettingsComponentService,
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: AssetsService,
          useClass: MockAssetsService,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
