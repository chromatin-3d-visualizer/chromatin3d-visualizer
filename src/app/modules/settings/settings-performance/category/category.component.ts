/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Category } from '../models/category.model';
import EditableQualityLevelParameters from '../../../../core/services/session-mananger/models/editable-quality-level-parameters.model';
import * as Names from 'src/app/core/services/session-mananger/constants';

@Component({
  selector: 'trj-category[categories][profile]',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent {
  @Input()
  public categories: Category[] = [];

  @Input()
  public profile!: EditableQualityLevelParameters;

  @Output()
  public profilePropertyChange = new EventEmitter<never>();

  public readonly CATEGORY_DESCRIPTION: { [key: string]: string } = {
    [Names.CATEGORY_NAME_LINKER_DNA]: 'Settings for the DNA',
    [Names.CATEGORY_NAME_COHESIN]:
      'Settings for cohesin head, linker and hinge',
    [Names.CATEGORY_NAME_HISTONE_OCTAMER]: 'Settings for histone octamer',
  };
}
