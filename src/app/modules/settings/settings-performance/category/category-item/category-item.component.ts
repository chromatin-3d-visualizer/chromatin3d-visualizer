/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { Property } from '../../models/property.model';
import {
  DECORATOR_CUSTOM_CHANGEABLE_CONDITION,
  DECORATOR_CUSTOM_CHANGEABLE_NUMBER,
  DECORATOR_CUSTOM_CHANGEABLE_OPTION,
  DECORATOR_CUSTOM_DISPLAY_NAME,
} from '../../../../../shared/decorators/constants';
import { Decorator } from '../../../../../shared/decorators/models/decorator.model';
import { DecoratorValueCondition } from '../../../../../shared/decorators/models/decorator-value-condition.model';
import { DecoratorValueOption } from '../../../../../shared/decorators/models/decorator-value-option.model';
import { DecoratorValueNumberRange } from '../../../../../shared/decorators/models/decorator-value-number-range.model';

@Component({
  selector: 'trj-category-item[item][selectedValue]',
  templateUrl: './category-item.component.html',
  styleUrls: ['./category-item.component.scss'],
})
export class CategoryItemComponent implements OnChanges {
  @Input()
  public item!: Property;

  @Input()
  public selectedValue!: any;

  @Output()
  public selectedValueChange = new EventEmitter<any>();

  public readonly conditions: Decorator<DecoratorValueCondition>[] = [];

  ngOnChanges(): void {
    this.conditions.splice(0);

    this.conditions.push(
      ...this.item.decorators.filter(
        (d) => d.name === DECORATOR_CUSTOM_CHANGEABLE_CONDITION
      )
    );
  }

  getOptionDecorator(): Decorator<DecoratorValueOption> | undefined {
    return this.item.decorators.find(
      (d) => d.name === DECORATOR_CUSTOM_CHANGEABLE_OPTION
    );
  }

  getNumberRangeDecorator(): Decorator<DecoratorValueNumberRange> | undefined {
    return this.item.decorators.find(
      (d) => d.name === DECORATOR_CUSTOM_CHANGEABLE_NUMBER
    );
  }

  getDisplayNameDecorator(): string | undefined {
    return this.item.decorators.find(
      (d) => d.name === DECORATOR_CUSTOM_DISPLAY_NAME
    )?.value;
  }
}
