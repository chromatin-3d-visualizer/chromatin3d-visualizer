/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  Component,
  ElementRef,
  Input,
  NgZone,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Group, PerspectiveCamera } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import Stats from 'three/examples/jsm/libs/stats.module';
import AbstractThreeJsSceneHelper from '../../../dockable-workspace/viewer/helpers/abstract-three-js-scene.helper';
import { ViewService } from '../../../../core/services/view/view.service';
import InstancedObjectBuilder from '../../../dockable-workspace/viewer/beads/instanced-object.builder';
import { OneTimeBuilder } from '../../../../shared/models/one-time-builder.model';
import BuilderGetter from '../../../../shared/builders/builder-getter';
import { ColorsService } from '../../../../core/services/colors/colors.service';
import QualityLevel from '../../../../core/services/session-mananger/models/quality-level.model';
import { AssetsService } from '../../../../core/services/assets/assets.service';
import { Configuration } from '../../../../shared/models/configuration.model';
import { CohesinService } from '../../../../core/services/cohesin.service';
import { EXAMPLE_TRAJECTORY } from './example-trajectory';
import CohesinHeadBuilder from '../../../dockable-workspace/viewer/beads/cohesin-head.builder';
import { EMaterialType } from '../../../../shared/models/material-type.enum';
import { IChangeableMaterial } from '../../../dockable-workspace/viewer/beads/changeable-material.interface';
import { BlockTrajectoryParser } from '../../../../shared/parsers/block-trajectory/block-trajectory.parser';
import {
  getBoxDimensionCenterAndSizes,
  getCenterPosition,
} from '../../../../shared/utilities/three.js.utility';
import Vector from '../../../../shared/models/vector.model';
import NucleosomeBead from '../../../../shared/models/nucleosome-bead.model';
import { getBuilderInstance } from '../../../../shared/utilities/bead-builder.utility';
import BlockTrajectoryConfigurationParser from '../../../../shared/parsers/block-trajectory/block-trajectory-configuration.parser';
import { ELinkerDNABuilderType } from '../../../../core/services/session-mananger/models/linker-dna-builder-type.enum';
import DoubleHelixBasePairBuilder from '../../../dockable-workspace/viewer/beads/double-helix-base-pair.builder';
import { MultipleBuilderResult } from '../../../../shared/models/multiple-builder-result.model';
import { getBasePairs } from '../../../../shared/builders/base-pair';
import FiberLightsManager from '../../../dockable-workspace/viewer/objects/threejs/managers/fiber-lights/fiber-lights.manager';
import { BehaviorSubject, Subject } from 'rxjs';
import { THREE_JS_TEXTURE_NAME_BEAD } from '../../../../core/services/assets/constants';

const ROTATION_SPEED = 0.005;

@Component({
  selector:
    'trj-settings-performance-preview[qualityLevel][autoRotate][wireframe]',
  templateUrl: './settings-preview.component.html',
  styleUrls: ['./settings-preview.component.scss'],
})
export class SettingsPreviewComponent
  extends AbstractThreeJsSceneHelper
  implements OnInit, OnChanges, OnDestroy
{
  private _stats: Stats | undefined;
  private _multipleUseBuilders: InstancedObjectBuilder<any, any>[] = [];
  private _oneTimeUseBuilders: OneTimeBuilder[] = [];
  private readonly _group = new Group();
  private readonly _cohesinService = {
    getCohDistance: () => 4,
    getCohesinThickness: () => 1,
    getCohesinHeadRadius: () => 2,
  } as CohesinService;

  private readonly _rerender$$ = new Subject<null>();
  private readonly _viewServiceMock = {
    rerender$: this._rerender$$.asObservable(),
    glossyMaterial$$: new BehaviorSubject<boolean>(true),
    getContainerSizeOfPBCOrFiberWithCenterPoint: () =>
      getBoxDimensionCenterAndSizes(
        this._currentConfig?.beads.map((b) => b.drawPosition) ?? []
      ),
  } as ViewService;

  @Input()
  public qualityLevel!: QualityLevel;

  @Input()
  public autoRotate!: boolean;

  @Input()
  public wireframe!: boolean;

  private _currentConfig: Configuration | undefined;

  private _controls: OrbitControls | undefined;

  @ViewChild('rendererCanvas', { static: true })
  private readonly _rendererCanvas!: ElementRef<HTMLCanvasElement>;

  constructor(
    private readonly _ngZone: NgZone,
    private readonly _viewService: ViewService,
    private readonly _colorService: ColorsService,
    private readonly _assetsService: AssetsService
  ) {
    super(_ngZone);
  }

  private static async getExampleConfiguration(): Promise<Configuration> {
    const file = new File([EXAMPLE_TRAJECTORY], 'trj.trj');
    const configOffset = (await new BlockTrajectoryParser().parse(file))
      .configurations[0];
    return new BlockTrajectoryConfigurationParser().parse(file, configOffset);
  }

  async ngOnInit(): Promise<void> {
    this._currentConfig =
      await SettingsPreviewComponent.getExampleConfiguration();
    this.moveChainToCenter();

    this._viewService.setBeadsFromConfiguration(this._currentConfig, -1);
    this.createScene(this._rendererCanvas.nativeElement);
    this.animate();
  }

  ngOnChanges(): void {
    if (this._controls) this._controls.enabled = !this.autoRotate;

    this.rerenderScene();
  }

  ngOnDestroy(): void {
    this.cancelAnimation();
    this.disposeCurrentScene();
    this.clearManagers();
    this.clearScene();
  }

  createScene(canvas: HTMLCanvasElement): void {
    this.initManagers();
    this.createStats();

    this.scene = super.createDefaultScene(
      canvas,
      window.innerWidth,
      window.innerHeight
    );

    this.camera = new PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      100
    );
    this.camera.position.z = 30;

    this._controls = new OrbitControls(this.camera, this.renderer?.domElement);
    this._controls.enabled = !this.autoRotate;

    this._group.add(...this.managers.map((m) => m.getGroup()));
    this.scene.add(this.camera, this._group);

    this.rerenderScene();
  }

  resize(width: number, height: number): void {
    if (this.camera instanceof PerspectiveCamera)
      this.camera.aspect = width / height;

    this.renderer?.setSize(width, height);
  }

  protected render(): void {
    this.frameId = requestAnimationFrame(() => {
      this._controls?.update();
      this.render();

      this._stats?.update();
    });

    if (this.autoRotate) {
      this._group.rotation.x += ROTATION_SPEED;
      this._group.rotation.y += ROTATION_SPEED;
    }

    if (!this.renderer || !this.scene || !this.camera) return;

    this.renderer.render(this.scene, this.camera);
  }

  protected subscribeListeners(): void {
    // ...
  }

  private initManagers(): void {
    this.managers.push(new FiberLightsManager(this._viewServiceMock));
  }

  private createStats(): void {
    this._stats = Stats();
    this._stats.showPanel(0);
    const doc = document.getElementById('stats-preview');
    doc?.appendChild(this._stats.dom);
  }

  private rerenderScene(): void {
    this.disposeCurrentScene();
    this._rerender$$.next(null);

    const builders = BuilderGetter.getMultipleUseBuilders(
      this._currentConfig,
      this._colorService,
      this.qualityLevel
    );

    for (const builder of builders.filter((b) => b.isActive)) {
      this.createMultipleBuilder(builder);
    }

    this.createOneTimeUseBuilders();
    this.createBasePairs();
    this.changeMaterial(this._viewService.materialType);
  }

  private createOneTimeUseBuilders(): void {
    if (!this._currentConfig) return;

    this._oneTimeUseBuilders.push(
      ...BuilderGetter.getOneTimeBuilders(
        this._currentConfig,
        this._colorService,
        { linkerDNASplitter: [] } as unknown as ViewService,
        this._cohesinService,
        this.qualityLevel,
        this._assetsService.getTexture(THREE_JS_TEXTURE_NAME_BEAD)
      )
    );
    for (const builder of this._oneTimeUseBuilders) {
      if (builder instanceof InstancedObjectBuilder) {
        builder.material.wireframe = this.wireframe;
        builder.setUpFirstStyle(
          this._colorService.cohesinRingAndHingeColors.color
        );
        this._group.add(builder.mesh);
      } else {
        builder.materials.forEach((m) => (m.wireframe = this.wireframe));
        this._group.add(...builder.meshes);
      }
    }
  }

  private createBasePairs(): void {
    if (
      this.qualityLevel.linkerDNABuilder !== ELinkerDNABuilderType.DOUBLE_HELIX
    )
      return;

    const baseBindingBeads = getBasePairs(
      this._multipleUseBuilders,
      this._oneTimeUseBuilders
    );
    this.createMultipleBuilder({
      builder: DoubleHelixBasePairBuilder,
      colorClass: this._colorService.basePairColors,
      param: baseBindingBeads,
      renderSize: baseBindingBeads.length,
      isActive: true,
    });
  }

  private createMultipleBuilder(builder: MultipleBuilderResult): void {
    const newBuilder = getBuilderInstance(
      builder,
      this._assetsService.getTexture(THREE_JS_TEXTURE_NAME_BEAD),
      this.qualityLevel,
      []
    );
    if (newBuilder instanceof CohesinHeadBuilder)
      newBuilder.setHeadSize(this._cohesinService.getCohesinHeadRadius());
    newBuilder.setUpFirstStyle(builder.colorClass.color);

    newBuilder.material.wireframe = this.wireframe;
    this._group.add(newBuilder.mesh);
    this._multipleUseBuilders.push(newBuilder);
  }

  private disposeCurrentScene(): void {
    while (this._multipleUseBuilders.length > 0) {
      const builder = this._multipleUseBuilders.splice(0, 1)[0];
      this.destroySingleMultipleUseBuilder(builder);
    }

    this.disposeOneTimeUseBuilders();
  }

  private destroySingleMultipleUseBuilder(
    builder: InstancedObjectBuilder<any, any>
  ): void {
    this._group.remove(builder.mesh);
    builder.destroy();
  }

  /**
   * There are special builders that have to be created over and over again. For example, the linker DNS cannot be represented as an
   * InstancedBuffer element because it is too different. For the Cohesin Head, the radius or thickness may change. For this reason, they
   * have to be recreated every time the configuration changes or PBC mode is (de)activated.
   * @private
   */
  private disposeOneTimeUseBuilders(): void {
    while (this._oneTimeUseBuilders.length > 0) {
      const builder = this._oneTimeUseBuilders.splice(0, 1)[0];
      if (builder instanceof InstancedObjectBuilder) {
        this.destroySingleMultipleUseBuilder(builder);
      } else {
        this._group.remove(...builder.meshes);
      }
      builder.destroy();
    }
  }

  private changeMaterial(material: EMaterialType): void {
    for (const changeableMaterial of <IChangeableMaterial[]>(
      [...this._multipleUseBuilders, ...this._oneTimeUseBuilders].filter(
        (b) => (<IChangeableMaterial>b).changeMaterialTo
      )
    )) {
      changeableMaterial.changeMaterialTo(material);
    }
  }

  private moveChainToCenter(): void {
    if (!this._currentConfig) return;

    const dim = getCenterPosition(
      this._currentConfig.beads.map((b) => b.position)
    );
    const dimV = new Vector(dim.x, dim.y, dim.z);
    for (const bead of this._currentConfig.beads) {
      bead.position.sub(dimV);
      if (bead instanceof NucleosomeBead) bead.center.sub(dimV);
    }
    for (const ring of this._currentConfig.cohesinRings) {
      ring.centerPos.sub(dimV);
    }
  }
}
