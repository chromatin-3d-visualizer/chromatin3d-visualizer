/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Subscription } from 'rxjs';

export class SubscribeEvents {
  protected readonly subscribes: Subscription[] = [];

  protected unsubscribeListeners(): void {
    for (const sub of this.subscribes) {
      sub.unsubscribe();
    }

    this.subscribes.splice(0);
  }
}
