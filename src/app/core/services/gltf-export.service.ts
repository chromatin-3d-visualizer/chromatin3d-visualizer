/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import {
  GLTFExporter,
  GLTFExporterOptions,
} from 'three/examples/jsm/exporters/GLTFExporter';
import {
  AmbientLight,
  BufferGeometry,
  Color,
  Group,
  InstancedMesh,
  Mesh,
  MeshStandardMaterial,
  Object3D,
  Scene,
  ShaderMaterial,
  Vector3,
  WebGLInfo,
} from 'three';
import { downloadBlob } from '../../shared/utilities/window.utility';
import { WorkspaceService } from './workspace.service';
import {
  MATERIAL_ATTRIBUTE_NAME_COLOR,
  MATERIAL_ATTRIBUTE_NAME_VIS,
  MATERIAL_COLOR_LENGTH,
} from '../../modules/dockable-workspace/viewer/beads/models.variables';
import { ViewService } from './view/view.service';
import { ContainerDimensionExtended } from './view/models/container-dimension-extended.model';
import { ProgressStateHandlerService } from './progress-state-handler.service';
import { EPS } from '../../shared/parsers/block-trajectory/constants.static';

/**
 * Type, describing a unique material.
 */
type UniqueMaterial = {
  name: string;
  material: MeshStandardMaterial;
};

/**
 * Type describing a group of converted instanced meshes.
 */
type ConverterGroup = {
  group: Group;
  convertedMeshes: ConvertedInstancedMesh[];
};

/**
 * An instance of a converted instanced mesh.
 */
type ConvertedInstancedMesh = {
  mesh: Mesh;
  color?: Color;
  visibility?: boolean;
};

const REGEX_MESH_NAME_SEARCH = /\(.*?\)\s*$/g;
/** Amount of triangles in the scene from which the resulting export is too large for the browser. */
const EXPORT_SPLIT_TRIGGER_CUTOFF = 60000000;
const EXPORT_FILE_EXTENSION = 'gltf';
const EXPORT_MODEL_ERROR_MESSAGE =
  'Something went wrong while trying to export the model';

/**
 * Source: node_modules/three/examples/jsm/exporters/GLTFExporter.js (Line 1005)
 */
const SUPPORTED_BUFFER_ATTRIBUTE_TYPES = [
  Float32Array,
  Uint32Array,
  Uint16Array,
  Uint8Array,
];

@Injectable({
  providedIn: 'root',
})
export class GLTFExportService {
  /** Array to store the unique materials encountered while exporting a scene. */
  private readonly _usedMaterials: UniqueMaterial[] = [];
  /** Array to store the unique colors encountered while exporting a scene. */
  private readonly _usedColors: Color[] = [];
  /** Amount of files to split the export into. */
  private _splitFileAmount = -1;

  public constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _viewService: ViewService,
    private readonly _progStateService: ProgressStateHandlerService
  ) {}

  /**
   * Handles a custom visibility attribute used on some elements of the scene
   * and aims to replace that incompatible attribute with a more compatible
   * one.
   *
   * @param mesh The mesh with the custom visibility attribute.
   * @private
   */
  private static handleCustomVisAttribute(mesh: Mesh): void {
    if (!mesh.geometry.hasAttribute(MATERIAL_ATTRIBUTE_NAME_VIS)) return;

    const visibility = mesh.geometry.getAttribute(
      MATERIAL_ATTRIBUTE_NAME_VIS
    ).array;
    mesh.visible = visibility.length < 1 || visibility[0] < 1;
    mesh.geometry.deleteAttribute(MATERIAL_ATTRIBUTE_NAME_VIS);
  }

  /**
   * Spreads the content of an array evenly over the given amount of result
   * arrays.
   *
   * @param childElements Elements to spread over the given amount of resulting arrays.
   * @param finalArrayAmount The amount of arrays to spread the given objects over.
   * @private
   */
  private static spreadArrayContentEvenly(
    childElements: Object3D[],
    finalArrayAmount: number
  ): Object3D[][] {
    const resultingArrays: Object3D[][] = [];

    for (let i = 0; i < finalArrayAmount; ++i) {
      resultingArrays.push([]);
    }

    for (let i = 0; i < childElements.length; ++i) {
      const childElement = childElements[i];
      resultingArrays[i % finalArrayAmount].push(childElement);
    }

    return resultingArrays;
  }

  private static isExportable(o: Object3D): boolean {
    return (
      !o.skipExportWhenInvisible || (o.skipExportWhenInvisible && o.visible)
    );
  }

  /**
   * Exports a given scene.
   *
   * @param scene The scene to export.
   * @param fileName The suggested file name for the completed export.
   * @param exporterOptions Options for the export.
   * @param rendererInfo Describes information about the rendering
   */
  public exportScene(
    scene: Scene,
    fileName: string,
    exporterOptions: GLTFExporterOptions = { onlyVisible: true }, // When setting it to false, everything (even if invisible) is exported, but they lose their transparency
    rendererInfo?: WebGLInfo
  ): void {
    this._progStateService.setProgressStatus({
      title: 'Exporting...',
      textLeft: 'Processing Model...',
      textRight: '',
      value: -1,
    });

    const exporter: GLTFExporter = new GLTFExporter();

    // Make sure the original scene is not changed by the export
    const sceneToExport = new Scene();
    for (const obj of this.getGrouplessObjects(
      this.getExportableObjects(scene.children)
    )) {
      sceneToExport.add(obj.clone(true));
    }

    // Preprocess the scene
    this.preProcessScene(sceneToExport);

    if (
      rendererInfo &&
      rendererInfo.render.triangles > EXPORT_SPLIT_TRIGGER_CUTOFF
    ) {
      this._splitFileAmount = Math.ceil(
        rendererInfo.render.triangles / EXPORT_SPLIT_TRIGGER_CUTOFF
      );
      console.debug('Export cutoff exceeded!');
      this.splitExport(sceneToExport, fileName, exporterOptions, exporter);
      return;
    }

    this._progStateService.setProgressStatus({
      textLeft: 'Parsing Model...',
      textRight: '',
      value: -1,
    });

    exporter.parse(
      sceneToExport,
      (result) => {
        console.debug('Parsing successful');
        this._progStateService.setProgressStatus({
          textLeft: 'Writing Data to File (This might take a while) ...',
          textRight: '',
          value: -1,
        });
        this.writeExportToFile(result, fileName);
        this._progStateService.removeDialog();
      },
      this.handleExportExceptions.bind(this),
      exporterOptions
    );
  }

  /**
   * Preprocesses the scene coming from ThreeJS by removing every potential element
   * that is not compatible with the exporter or by converting some objects that
   * don't comply with the exporter to elements that do.
   *
   * @param sceneToProcess The unedited scene that will be processed to fit the export.
   * @private
   */
  private preProcessScene(sceneToProcess: Scene): void {
    this.cleanUpPreProcessHelperArrays();

    // Filter the scene for elements that require further processing
    const filterResults = this.filterSceneAndRepositionChildren(
      sceneToProcess.children
    );

    const ambientLights: AmbientLight[] = filterResults.ambientLights;
    const instancedMeshes: InstancedMesh[] = filterResults.instancedMeshes;
    const meshesWithCustomAttributes: Mesh[] =
      filterResults.meshesWithCustomAttributes;
    const convertedIMeshesWithCustomAttributes: ConvertedInstancedMesh[] = [];

    // Since ambient lights are not supported by glTF, they are filtered out
    for (const ambientLight of ambientLights) {
      sceneToProcess.remove(ambientLight);
    }

    // Convert and then replace the instanced meshes with compatible, regular meshes
    for (const instancedMesh of instancedMeshes) {
      // Introduced to prevent exceeding call stack
      for (const convertedMesh of this.convertInstancedMeshToRegularMeshes(
        instancedMesh
      ).convertedMeshes) {
        convertedIMeshesWithCustomAttributes.push(convertedMesh);
      }

      sceneToProcess.remove(instancedMesh);
    }

    // Introduced to prevent exceeding call stack
    for (const mesh of this.handleInstancedMeshMaterials(
      convertedIMeshesWithCustomAttributes
    )) {
      sceneToProcess.add(mesh);
    }

    this.handleCustomAttributes(meshesWithCustomAttributes);
    this.cleanUpPreProcessHelperArrays();
  }

  private getExportableObjects(objects: Object3D[]): Object3D[] {
    return objects.filter((o) => GLTFExportService.isExportable(o));
  }

  /**
   * Recursively removes groups from the array given and adds their children
   * into the list of objects.
   * @param objects The array to remove groups from.
   * @returns Object3D[] - Array containing every child of the array except for the groups.
   * @private
   */
  private getGrouplessObjects(objects: Object3D[]): Object3D[] {
    const finalChildren: Object3D[] = [];
    const currentChildren: Object3D[] = [...objects];

    let groupIndex = -1;

    while (
      (groupIndex = currentChildren.findIndex(
        (child) => child instanceof Group
      )) > -1
    ) {
      const group = currentChildren.splice(groupIndex, 1)[0];
      if (!GLTFExportService.isExportable(group)) continue;

      finalChildren.push(
        ...this.getGrouplessObjects(this.getExportableObjects(group.children))
      );
    }

    finalChildren.splice(0, 0, ...currentChildren);

    return finalChildren;
  }

  /**
   * Goes through the children of a scene and adds those that require further processing to the appropriate,
   * given arrays. Also replaces the material of meshes where possible to ensure compatibility with the exporter
   * and additionally, moves the elements of the scene towards the coordinate center.
   *
   * @param sceneChildren Array of child objects of the scene. May contain cameras, meshes etc.
   * @return Returns an object containing arrays which will be filled with the found ambient lights, instanced meshes and meshes with custom attributes.
   * @private
   */
  private filterSceneAndRepositionChildren(sceneChildren: Object3D[]): {
    ambientLights: AmbientLight[];
    instancedMeshes: InstancedMesh[];
    meshesWithCustomAttributes: Mesh[];
  } {
    const ambientLights: AmbientLight[] = [];
    const instancedMeshes: InstancedMesh[] = [];
    const meshesWithCustomAttributes: Mesh[] = [];
    const fibreBoxWithCenter =
      this._viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();

    for (const sceneObject of sceneChildren) {
      // Move the scene to the coordinate center (since the models often land hundreds of units away from
      // the center with nothing in between)
      sceneObject.position.sub(
        new Vector3(
          fibreBoxWithCenter.centerX,
          fibreBoxWithCenter.centerY,
          fibreBoxWithCenter.centerZ
        )
      );

      if (!GLTFExportService.isExportable(sceneObject)) continue;

      if (sceneObject instanceof AmbientLight) {
        ambientLights.push(sceneObject);
      } else if (sceneObject instanceof Mesh) {
        this.handleMaterialProperties(sceneObject);

        // Filter for instanced Meshes, since these are not natively supported by glTF
        if (sceneObject instanceof InstancedMesh) {
          instancedMeshes.push(sceneObject);
          continue;
        }

        // Since the custom visibility attribute features an unsupported datatype, it will be removed later on
        if (
          sceneObject.geometry.hasAttribute(MATERIAL_ATTRIBUTE_NAME_VIS) ||
          sceneObject.geometry.hasAttribute(MATERIAL_ATTRIBUTE_NAME_COLOR)
        ) {
          meshesWithCustomAttributes.push(sceneObject);
        }
      }
    }

    return { ambientLights, instancedMeshes, meshesWithCustomAttributes };
  }

  /**
   * Adapted from https://github.com/mrdoob/three.js/blob/master/examples/jsm/utils/SceneUtils.js
   *
   * This function was originally taken from the ThreeJS-SceneUtil and later adapted.
   * This was done since the method could not actually be imported in TypeScript.
   *
   * It has been edited so that it is capable of handling the custom properties of the model when
   * converting the meshes.
   * @param instancedMesh An instanced Mesh to be transformed to a collection of regular meshes.
   * @private
   */
  private convertInstancedMeshToRegularMeshes(
    instancedMesh: InstancedMesh
  ): ConverterGroup {
    const converterGroup: ConverterGroup = {
      group: new Group(),
      convertedMeshes: [],
    };
    const count = instancedMesh.count;
    const geometry = instancedMesh.geometry.clone();
    const material = instancedMesh.material;
    const fibreBoxWithCenter: ContainerDimensionExtended =
      this._viewService.getContainerSizeOfPBCOrFiberWithCenterPoint();

    // the custom color of the nodes needs to be maintained
    const customColorAttribute: Float32Array = geometry.getAttribute(
      MATERIAL_ATTRIBUTE_NAME_COLOR
    ).array as Float32Array;
    geometry.deleteAttribute(MATERIAL_ATTRIBUTE_NAME_COLOR);

    // the custom visibility of the nodes needs to be maintained
    const customVisibilityAttribute: Int32Array = geometry.getAttribute(
      MATERIAL_ATTRIBUTE_NAME_VIS
    ).array as Int32Array;
    geometry.deleteAttribute(MATERIAL_ATTRIBUTE_NAME_VIS);

    this.checkAndClearUnsupportedBufferAttributes(geometry);

    // This value states whether there are enough color values in the array (since this does not change)
    const enoughColorValues: boolean =
      customColorAttribute.length >= count * MATERIAL_COLOR_LENGTH;

    // This value states whether there are enough visibility values in the array (since this does not change)
    const enoughVisibilityValues: boolean =
      customVisibilityAttribute.length >= count;

    for (let i = 0; i < count; ++i) {
      const mesh = new Mesh(geometry, material); // clone() so that the geometry could be changed for each individual mesh
      mesh.name = `${instancedMesh.name} (${i + 1})`; // For more clarity regarding the node's purpose, add a name
      instancedMesh.getMatrixAt(i, mesh.matrix);
      mesh.matrix.decompose(mesh.position, mesh.quaternion, mesh.scale);

      // Some instanced meshes contained matrices purely containing zeros!, which led to NaN-values after decomposing
      if (
        mesh.quaternion.toArray().includes(NaN) ||
        mesh.position.toArray().includes(NaN) ||
        mesh.scale.toArray().includes(NaN)
      )
        continue;

      const convertedMesh: ConvertedInstancedMesh = { mesh: mesh };

      // Since meshes stemming from instanced meshes are not affected by the position correction via
      // (see filterSceneAndRepositionChildren()), it will have to be done to these meshes again
      mesh.position.sub(
        new Vector3(
          fibreBoxWithCenter.centerX,
          fibreBoxWithCenter.centerY,
          fibreBoxWithCenter.centerZ
        )
      );

      // If the instanced mesh does have a custom color attribute their children get their dedicated colour
      if (enoughColorValues) {
        const startIndex = i * MATERIAL_COLOR_LENGTH;

        convertedMesh.color = this.ensureUniqueColor(
          new Color(
            customColorAttribute[startIndex],
            customColorAttribute[startIndex + 1],
            customColorAttribute[startIndex + 2]
          )
        );

        // Check the visibility based on the alpha value in the color attribute (checked against EPS, since it's a float)
        convertedMesh.visibility = customColorAttribute[startIndex + 3] > EPS;
      }

      // If the instanced mesh does have a custom visibility attribute their children get their dedicated visibility
      if (enoughVisibilityValues) {
        // If the alpha was 0 and thus the object is not visible to the user, respect that fact and
        // ignore any differing visibility value (since the user can't see the object, it should also
        // be treated as invisible by the export in order to have a consistent user experience)
        // In any other case, just use the visibility attribute
        convertedMesh.visibility =
          convertedMesh.visibility === false
            ? convertedMesh.visibility
            : customVisibilityAttribute[i] < 1;
      }

      converterGroup.group.add(mesh);
      converterGroup.convertedMeshes.push(convertedMesh);
    }

    converterGroup.group.copy(instancedMesh as any);
    converterGroup.group.updateMatrixWorld(); // ensure correct world matrices of meshes

    return converterGroup;
  }

  /**
   * Cleans up all helper arrays that were used during the
   * preprocessing.
   * @private
   */
  private cleanUpPreProcessHelperArrays(): void {
    this._usedMaterials.splice(0);
    this._usedColors.splice(0);
  }

  /**
   * Writes the parsed model object to file.
   *
   * @param exportResult The json object containing the parsed model.
   * @param fileName The suggested file name.
   * @private
   */
  private writeExportToFile(exportResult: object, fileName: string): void {
    try {
      const glTFBlob: Blob = new Blob([JSON.stringify(exportResult, null, 2)], {
        type: 'application/text',
      });
      downloadBlob(glTFBlob, `${fileName}.${EXPORT_FILE_EXTENSION}`);
    } catch (exception) {
      console.warn(exception);
      this.handleExportExceptions(exception);
    }
  }

  /**
   * Conducts an export that was split due to exceeding a certain export
   * file size limit. Splits the export of the given scene into multiple
   * files below said limit.
   *
   * @param scene The scene to be exported.
   * @param fileName The name of the resulting file(s).
   * @param exporterOptions The options to be used while exporting.
   * @param exporter The exporter to parse the scene into the desired format.
   * @private
   */
  private splitExport(
    scene: Scene,
    fileName: string,
    exporterOptions: GLTFExporterOptions,
    exporter: GLTFExporter
  ): void {
    this._progStateService.setProgressStatus({
      textLeft: 'Exported File too large, splitting export...',
      textRight: '',
      value: -1,
    });

    const splitScenes: Scene[] = [];
    const splitChildren: Object3D[][] =
      GLTFExportService.spreadArrayContentEvenly(
        scene.children.splice(0),
        this._splitFileAmount
      );

    for (const childArray of splitChildren) {
      const splitScene = new Scene();
      // Get all set options and parameters of the original scene (so no data is lost)
      splitScene.copy(scene);
      splitScene.children = childArray;
      splitScenes.push(splitScene);
    }

    this.processSplitExport(
      splitScenes,
      exporterOptions,
      exporter,
      fileName
    ).then(() => {
      this._splitFileAmount = -1;
    });
  }

  /**
   * Processes multiple scenes into multiple distinct export files of the desired
   * format.
   *
   * @param scenes The scenes to be exported.
   * @param exporterOptions The options to be used during the export.
   * @param exporter The exporter to convert the scenes with.
   * @param fileName The name of the resulting export file.
   * @private
   */
  private async processSplitExport(
    scenes: Scene[],
    exporterOptions: GLTFExporterOptions,
    exporter: GLTFExporter,
    fileName: string
  ): Promise<void> {
    try {
      for (let i = 0; i < scenes.length; ++i) {
        const humanReadableCount = i + 1;

        this._progStateService.setProgressStatus({
          textLeft: 'Parsing Model...',
          textRight: `${i + 1} of ${this._splitFileAmount}`,
          value: (i / this._splitFileAmount) * 100,
        });

        const scene = scenes[i];
        const result = await new Promise<object>((resolve, reject) =>
          exporter.parse(
            scene,
            (gltfResult) => resolve(gltfResult),
            () => reject(),
            exporterOptions
          )
        );

        this._progStateService.setProgressStatus({
          textLeft: 'Writing Data to File (This might take a while) ...',
          textRight: `${i + 1} of ${this._splitFileAmount}`,
          value: (i / this._splitFileAmount) * 100,
        });

        this.writeExportToFile(
          result,
          `${fileName} Part ${humanReadableCount}`
        );
      }

      this._progStateService.removeDialog();
    } catch (exception) {
      console.warn(exception);
      this.handleExportExceptions(exception);
    }
  }

  /**
   * Takes a mesh and changes its material to a more supported material for
   * the export. Also aims to transport all properties of the old material to
   * the new one.
   *
   * @param mesh The mesh whose material is changed.
   * @private
   */
  private handleMaterialProperties(mesh: Mesh): void {
    const meshMaterial = mesh.material as any;

    // Some objects have a shader material and are using vertex coloring, which is not stored as the "color"
    // This means, that meshMaterial does not gain the correct color in this case
    if (mesh.material instanceof ShaderMaterial) {
      meshMaterial.color = mesh.material.uniforms.vertexColor
        ? mesh.material.uniforms.vertexColor.value
        : undefined;
    }

    if (!meshMaterial.color) return;

    // Visibility only seems to be output correctly when used upon the entire mesh
    mesh.visible = meshMaterial.visible;

    mesh.material = this.createStandardMaterial(
      mesh.name,
      this.ensureUniqueColor(meshMaterial.color),
      mesh.visible
    );
  }

  /**
   * Handles custom attributes on meshes from the scene by either transforming
   * the attributes to gltf compatible alternatives or removing the attribute
   * from the object if no alternative can be found. This is then also applied
   * to the material of the mesh which is replaced by a gltf friendly alternative,
   * keeping the custom attributes
   *
   * @param meshesWithCustomAttributes Meshes with custom attributes.
   * @private
   */
  private handleCustomAttributes(meshesWithCustomAttributes: Mesh[]): void {
    // Handle the custom attributes by either replacing them with valid attributes or removing them
    for (const meshWithCustomAttribute of meshesWithCustomAttributes) {
      meshWithCustomAttribute.geometry =
        meshWithCustomAttribute.geometry.clone();

      GLTFExportService.handleCustomVisAttribute(meshWithCustomAttribute);

      meshWithCustomAttribute.material = this.createStandardMaterial(
        meshWithCustomAttribute.name,
        this.handleCustomColorAttribute(meshWithCustomAttribute)
      );
    }
  }

  /**
   * Does similar things to {@link handleMaterialProperties} in that the method
   * aims to convert the materials of these converted instanced meshes
   * to a material that is more supported by gltf.
   *
   * @param convertedMeshes The converted instanced meshes with the material to be changed.
   * @private
   */
  private handleInstancedMeshMaterials(
    convertedMeshes: ConvertedInstancedMesh[]
  ): Mesh[] {
    const resultMeshes: Mesh[] = [];

    for (const convertedMesh of convertedMeshes) {
      const mesh = convertedMesh.mesh;
      const meshColor = convertedMesh.color ?? (mesh.material as any).color;

      mesh.visible = convertedMesh.visibility ?? true;
      mesh.material = this.createStandardMaterial(
        mesh.name,
        meshColor,
        mesh.visible
      );

      resultMeshes.push(mesh);
    }

    return resultMeshes;
  }

  /**
   * Designed to handle the custom color attribute used on some objects of the
   * scene in the viewer. This is meant to transform the attribute to a more
   * compatible alternative while also removing the incompatible attribute.
   *
   * @param mesh A mesh with the custom color attribute.
   * @returns A color object.
   * @private
   */
  private handleCustomColorAttribute(mesh: Mesh): Color {
    if (!mesh.geometry.hasAttribute(MATERIAL_ATTRIBUTE_NAME_COLOR))
      return new Color();

    const colorArray = mesh.geometry.getAttribute(
      MATERIAL_ATTRIBUTE_NAME_COLOR
    ).array;
    mesh.geometry.deleteAttribute(MATERIAL_ATTRIBUTE_NAME_COLOR);

    return this.ensureUniqueColor(
      new Color(colorArray[0] ?? 0, colorArray[1] ?? 0, colorArray[2] ?? 0)
    );
  }

  /**
   * Creates a material that is supported by the gltf exporter / converter out
   * of the given information while making sure that no duplications for the
   * materials occur in order to improve performance.
   *
   * @param fullMeshName Name of the mesh.
   * @param color Color of the mesh.
   * @param visible Visibility of the mesh.
   * @returns Returns a MeshStandardMaterial instance.
   * @private
   */
  private createStandardMaterial(
    fullMeshName: string,
    color: Color,
    visible: boolean = true
  ): MeshStandardMaterial {
    const meshName = fullMeshName.replace(REGEX_MESH_NAME_SEARCH, '').trim();

    const alternativeMaterial = this._usedMaterials.find(
      (material) =>
        color.equals(material.material.color) && meshName === material.name
    );

    if (alternativeMaterial) return alternativeMaterial.material;

    const newMaterial = new MeshStandardMaterial({
      color: color,
      opacity: visible ? 1 : 0,
    });
    this._usedMaterials.push({
      name: meshName,
      material: newMaterial,
    });
    return newMaterial;
  }

  /**
   * Handles exceptions occurring during the export.
   *
   * @param exception The exception which occurred.
   * @private
   */
  private handleExportExceptions(exception: unknown): void {
    const errorMessage: string =
      exception instanceof Error ? exception.message : 'Unknown Error';

    this._progStateService.setErrorMessage(
      `${EXPORT_MODEL_ERROR_MESSAGE}: ${errorMessage}`
    );
  }

  /**
   * Checks if a given color already appeared during the conversion process
   * and makes sure that no duplicate colors are created.
   *
   * @param givenColor The color to be checked.
   * @returns Returns either the same color if it was unique or the original color the given one was a duplicate of.
   * @private
   */
  private ensureUniqueColor(givenColor: Color): Color {
    const alternativeColor = this._usedColors.find((color) =>
      color.equals(givenColor)
    );

    if (alternativeColor) return alternativeColor;

    this._usedColors.push(givenColor);
    return givenColor;
  }

  private checkAndClearUnsupportedBufferAttributes(
    geometry: BufferGeometry
  ): void {
    const toDelete: string[] = [];

    for (const [attrName, val] of Object.entries(geometry.attributes)) {
      if (
        SUPPORTED_BUFFER_ATTRIBUTE_TYPES.find(
          (c) => val.array.constructor === c
        )
      )
        continue;

      console.warn(
        `Buffer attribute "${attrName}" has an unsupported array type!`
      );
      toDelete.push(attrName);
    }

    toDelete.forEach((n) => geometry.deleteAttribute(n));
  }
}
