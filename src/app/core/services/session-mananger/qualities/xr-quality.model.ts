/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import QualityLevel from '../models/quality-level.model';
import { ELinkerDNABuilderType } from '../models/linker-dna-builder-type.enum';

export const XR_QUALITY = new QualityLevel({
  linkerDNABuilder: ELinkerDNABuilderType.INSTANCED_MESHED,
  cohesinHeadWidthSegments: 8,
  cohesinHeadHeightSegments: 8,
  cohesinRingRadialSegments: 6,
  cohesinRingTubularSegments: 8,
  cohesinHingeRadialSegments: 6,
  cohesinHingeTubularSegments: 6,
  nucleosomeDNATubularSegments: 8,
  nucleosomeDNARadiusSegments: 6,
  linkerDNASmoothing: 8,
  histoneOctamerRadialSegments: 6,
  doubleHelixBasePairRadiusSegments: 0,
  doubleHelixBasePairTubularSegments: 0,
});
