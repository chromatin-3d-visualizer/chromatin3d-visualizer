/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import QualityLevel from '../models/quality-level.model';
import { ELinkerDNABuilderType } from '../models/linker-dna-builder-type.enum';
import { QualityProfile } from '../models/quality-profile.model';

export const CUSTOM_QUALITY_PROFILE_NAME = 'custom';

export const CUSTOM_QUALITY: QualityProfile = {
  displayName: 'Custom (changeable)',
  name: CUSTOM_QUALITY_PROFILE_NAME,
  level: new QualityLevel({
    linkerDNABuilder: ELinkerDNABuilderType.NORMAL,
    cohesinHeadWidthSegments: 16,
    cohesinHeadHeightSegments: 16,
    cohesinRingRadialSegments: 16,
    cohesinRingTubularSegments: 32,
    cohesinHingeRadialSegments: 16,
    cohesinHingeTubularSegments: 16,
    nucleosomeDNATubularSegments: 30,
    nucleosomeDNARadiusSegments: 12,
    linkerDNASmoothing: 8,
    histoneOctamerRadialSegments: 16,
    doubleHelixBasePairRadiusSegments: 12,
    doubleHelixBasePairTubularSegments: 16,
  }),
};
