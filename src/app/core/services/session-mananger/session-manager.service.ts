/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Session } from './models/session.model';
import QualityLevel from './models/quality-level.model';
import { HIGH_QUALITY } from './qualities/high-quality.model';
import {
  CUSTOM_QUALITY,
  CUSTOM_QUALITY_PROFILE_NAME,
} from './qualities/custom-quality.model';
import { NORMAL_QUALITY } from './qualities/normal-quality.model';
import { LOW_QUALITY } from './qualities/low-quality.model';
import { QualityProfile } from './models/quality-profile.model';

@Injectable({
  providedIn: 'root',
})
export class SessionManagerService {
  private readonly _loadSession$$ = new Subject<Partial<Session>>();
  private readonly _qualityProfile$$ = new BehaviorSubject<QualityProfile>(
    CUSTOM_QUALITY
  );
  private readonly _customQuality$$ = new Subject<QualityLevel>();

  public readonly getCurrentSession: (() => Partial<Session>)[] = [];
  public readonly qualityProfiles: QualityProfile[] = [
    LOW_QUALITY,
    NORMAL_QUALITY,
    HIGH_QUALITY,
    CUSTOM_QUALITY,
  ];

  public readonly loadSession$ = this._loadSession$$.asObservable();
  public readonly qualityProfile$ = this._qualityProfile$$.asObservable();
  public readonly customQuality$ = this._customQuality$$.asObservable();

  public getSession(): Session {
    let result = {} as Session;

    for (const getSessionFunction of this.getCurrentSession) {
      result = { ...result, ...getSessionFunction() };
    }

    return result;
  }

  public getCustomQualityLevel(): QualityLevel {
    const customProfile = this.qualityProfiles.find(
      (q) => q.name === CUSTOM_QUALITY_PROFILE_NAME
    );
    if (!customProfile) throw new Error("Custom quality level couldn't found!");

    return customProfile.level;
  }

  public loadSession(session: Session): void {
    this._loadSession$$.next(session);
  }

  public get qualityProfile(): QualityProfile {
    return this._qualityProfile$$.value;
  }

  public get qualityLevel(): QualityLevel {
    return this._qualityProfile$$.value.level;
  }

  public get qualityProfileName(): string {
    return this._qualityProfile$$.value.name;
  }

  public setQualityLevel(name: string): void {
    const quality = this.qualityProfiles.find((q) => q.name === name);
    if (quality) this._qualityProfile$$.next(quality);
  }

  public setCustomLevel(profile: QualityLevel): void {
    const customProfile = this.qualityProfiles.find(
      (q) => q.name === CUSTOM_QUALITY_PROFILE_NAME
    );
    if (customProfile) {
      customProfile.level = profile;

      if (this._qualityProfile$$.value.name === CUSTOM_QUALITY_PROFILE_NAME)
        this._qualityProfile$$.next(customProfile);

      this._customQuality$$.next(profile);
    }
  }
}
