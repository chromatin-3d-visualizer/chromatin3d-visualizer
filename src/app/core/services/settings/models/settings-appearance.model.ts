/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ETheme } from '../../../../shared/models/theme.enum';

export type SettingsAppearance = {
  theme: ETheme;
  /**
   * **Note:** The content is not actually read from the local storage. This
   * attribute is actually only used at runtime to edit the settings and then
   * save them. This value must be stored separately, because the reading of
   * this value must happen before the initialization of any service.
   */
  animationEnabled: boolean;
};
