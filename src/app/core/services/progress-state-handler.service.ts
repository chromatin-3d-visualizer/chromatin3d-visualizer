/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProgressState } from './models/progress-state.model';
import { LoadingDialogComponent } from '../../shared/components/loading-dialog/loading-dialog.component';
import { DEFAULT_LOADING_DIALOG_WIDTH } from '../../constants';
import { SubscribeEvents } from '../classes/subscribe-events';

@Injectable({
  providedIn: 'root',
})
export class ProgressStateHandlerService
  extends SubscribeEvents
  implements OnDestroy
{
  private _dialogRef: MatDialogRef<LoadingDialogComponent>[] = [];

  public constructor(
    private readonly _dialog: MatDialog,
    private readonly _snackBar: MatSnackBar
  ) {
    super();
  }

  public ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  public setProgressStatus(loadingState: ProgressState): void {
    let dialogIndex = this._dialogRef.length - 1;
    if (dialogIndex < 0) {
      dialogIndex = 0;
      const newDialog = this._dialog.open(LoadingDialogComponent, {
        width: DEFAULT_LOADING_DIALOG_WIDTH,
        disableClose: true,
      });
      this.subscribes.push(
        newDialog
          .afterClosed()
          .subscribe(
            () =>
              (this._dialogRef = this._dialogRef.filter((d) => d !== newDialog))
          )
      );
      newDialog.componentInstance.title = loadingState.title ?? '';

      this._dialogRef.push(newDialog);
    }

    const dialog = this._dialogRef[dialogIndex];
    if (loadingState.title) dialog.componentInstance.title = loadingState.title;
    dialog.componentInstance.progressType =
      loadingState.value < 0 ? 'indeterminate' : 'determinate';
    dialog.componentInstance.progressValue = loadingState.value;
    dialog.componentInstance.textLeft = loadingState.textLeft;
    dialog.componentInstance.textRight = loadingState.textRight;
  }

  public setErrorMessage(
    errorMessage: string,
    clearLoadingDialog = true
  ): void {
    if (clearLoadingDialog) this.closeLoadingDialog();

    if (!errorMessage) return;

    this._snackBar.open(errorMessage, 'Okay', { duration: 7000 });
  }

  public removeDialog(): void {
    this.closeLoadingDialog();
  }

  private closeLoadingDialog(): void {
    if (!this._dialogRef) return;

    while (this._dialogRef.length > 0) {
      const dialog = this._dialogRef.splice(0, 1)[0];
      dialog.close();
    }
  }
}
