/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { TestBed } from '@angular/core/testing';
import { PbcSettingsSerializerDeserializerService } from './pbc-settings-serializer-deserializer.service';
import { MatDialog } from '@angular/material/dialog';
import { MATERIAL_IMPORTS } from '../__mocks__/imports';
import { WorkspaceService } from '../workspace.service';
import { WorkspaceService as MockWorkspaceService } from '../__mocks__/workspace.service';

describe('PbcSettingsSerializerDeserializerService', () => {
  let service: PbcSettingsSerializerDeserializerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [...MATERIAL_IMPORTS],
      providers: [
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
        { provide: MatDialog, useValue: {} },
      ],
    });
    service = TestBed.inject(PbcSettingsSerializerDeserializerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
