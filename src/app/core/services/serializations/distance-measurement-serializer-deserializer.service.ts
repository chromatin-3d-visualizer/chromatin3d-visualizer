/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { pairwise } from 'rxjs/operators';
import { DistanceMeasurementService } from '../distance-measurement.service';
import { LOCAL_STORAGE_SETTINGS_TOOL_DISTANCE_MEASUREMENT } from '../../../shared/local-storage-keys.constants';

type DeserializedDistanceMeasurementSettings = {
  lineColor: string;
  lineWidth: number;
  textColor: string;
  textSize: number;
};

@Injectable({
  providedIn: 'root',
})
export class DistanceMeasurementSerializerDeserializerService {
  public constructor(
    private readonly _distanceMeasurementService: DistanceMeasurementService
  ) {
    this.restoreColors();

    this._distanceMeasurementService.lineColor$
      .pipe(pairwise())
      .subscribe(this.saveColors.bind(this));
    this._distanceMeasurementService.lineWidth$
      .pipe(pairwise())
      .subscribe(this.saveColors.bind(this));
    this._distanceMeasurementService.textColor$
      .pipe(pairwise())
      .subscribe(this.saveColors.bind(this));
    this._distanceMeasurementService.textSize$
      .pipe(pairwise())
      .subscribe(this.saveColors.bind(this));
  }

  private restoreColors(): void {
    const settingsStr = localStorage.getItem(
      LOCAL_STORAGE_SETTINGS_TOOL_DISTANCE_MEASUREMENT
    );
    if (!settingsStr) {
      return;
    }

    try {
      const settings = JSON.parse(
        settingsStr
      ) as DeserializedDistanceMeasurementSettings;

      this.setColorsFromParsedColors(settings);
    } catch (e) {
      console.warn('Distance measurement tool settings could not be restored.');
      return;
    }
  }

  private setColorsFromParsedColors(
    settings: DeserializedDistanceMeasurementSettings
  ): void {
    this._distanceMeasurementService.lineColor = settings.lineColor;
    this._distanceMeasurementService.lineWidth = settings.lineWidth;
    this._distanceMeasurementService.textColor = settings.textColor;
    this._distanceMeasurementService.textSize = settings.textSize;
  }

  private saveColors(): void {
    localStorage.setItem(
      LOCAL_STORAGE_SETTINGS_TOOL_DISTANCE_MEASUREMENT,
      JSON.stringify({
        lineColor: this._distanceMeasurementService.lineColor,
        lineWidth: this._distanceMeasurementService.lineWidth,
        textColor: this._distanceMeasurementService.textColor,
        textSize: this._distanceMeasurementService.textSize,
      } as DeserializedDistanceMeasurementSettings)
    );
  }
}
