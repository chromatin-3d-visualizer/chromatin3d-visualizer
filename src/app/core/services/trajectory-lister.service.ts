/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { DownloadService } from './download.service';
import { ProgressStateHandlerService } from './progress-state-handler.service';
import PaperHelper from '../../shared/models/paper-helper.model';

type TableOfContent = {
  files: { name: string; path: string }[];
};

@Injectable({
  providedIn: 'root',
})
export class TrajectoryListerService {
  private readonly _trajectories$$ = new BehaviorSubject<PaperHelper | null>(
    null
  );

  public readonly trajectories$ = this._trajectories$$.asObservable();

  public get trajectories(): PaperHelper | null {
    return this._trajectories$$.value;
  }

  public constructor(
    private readonly _downloader: DownloadService,
    private readonly _progressStateHandlerService: ProgressStateHandlerService
  ) {
    this._downloader.file$
      .pipe(filter((p) => p.isTableOfContentFile))
      .subscribe(async (p) => {
        const files = await this.parseTableOfContentFile(p.file);
        if (!files) return;
        this._trajectories$$.next(files);
      });
  }

  private async parseTableOfContentFile(
    file: File
  ): Promise<PaperHelper | null> {
    try {
      return new PaperHelper(
        JSON.parse(await file.text()) as Partial<TableOfContent>
      );
    } catch (e) {
      this._progressStateHandlerService.setErrorMessage(
        `Error occurred while parsing. The file may be corrupted. Error message: ${e}`
      );
      console.warn(e);
    }

    return null;
  }
}
