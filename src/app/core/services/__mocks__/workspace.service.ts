/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import PartialTrajectory from '../../../shared/parsers/models/partial-trajectory.model';
import { ParsedConfiguration } from '../../../shared/parsers/models/parsed-configuration.model';
import { Session } from '../session-mananger/models/session.model';
import { ReadProgress } from '../../../shared/web-workers/models/read-progress.model';
import { SessionManagerService } from '../session-mananger/session-manager.service';
import {
  EXCHANGE_CONFIGURATION,
  EXCHANGE_LOAD_TRAJECTORY,
} from '../../../shared/web-workers/constants';
import { ExchangeFormat } from '../../../shared/web-workers/models/exchange-format.model';

@Injectable({
  providedIn: 'root',
})
export class WorkspaceService implements OnDestroy {
  private readonly _trajectory$$ =
    new BehaviorSubject<PartialTrajectory | null>(null);
  private readonly _config$$ = new BehaviorSubject<ParsedConfiguration | null>(
    null
  ); // null => NO CONFIGURATION
  private readonly _beforeSetTrajectory$$ = new Subject<Session | undefined>();
  private readonly _beforeSetConfig$$ =
    new Subject<ParsedConfiguration | null>();
  private readonly _selectionTexts$$ = new BehaviorSubject<string[]>([]);
  private readonly _triggeredFiles$$ = new BehaviorSubject<string[]>([]);

  public readonly trajectory$ = this._trajectory$$.asObservable();
  public readonly config$ = this._config$$.asObservable();
  public readonly beforeSetTrajectory$ =
    this._beforeSetTrajectory$$.asObservable();
  public readonly beforeSetConfig$ = this._beforeSetConfig$$.asObservable();
  public readonly selectionTexts$ = this._selectionTexts$$.asObservable();
  public readonly triggeredFiles$ = this._triggeredFiles$$.asObservable();

  public currentFile: File | undefined;

  private _renderCount = -1;
  private _worker: Worker | undefined;
  private _currentFileName: string | undefined;
  private _session: Session | undefined;
  private _isViewerActive = false;

  public set isViewerActive(val: boolean) {
    this._isViewerActive = val;
  }

  public get trajectory(): PartialTrajectory | null {
    return this._trajectory$$.getValue();
  }

  public get renderCount(): number {
    return this._trajectory$$.value?.maxBeadLength ?? -1;
  }

  public get config(): ParsedConfiguration | null {
    return this._config$$.value;
  }

  public get selectionTexts(): string[] {
    return this._selectionTexts$$.value;
  }

  public set selectionTexts(val: string[]) {
    this._selectionTexts$$.next(val);
  }

  public get triggeredFiles(): string[] {
    return this._triggeredFiles$$.value;
  }

  public constructor(private readonly _sessionManager: SessionManagerService) {
    this.init();

    this._sessionManager.getCurrentSession.push(() => ({
      configIndex:
        this._config$$.value && this._config$$.value.index >= 0
          ? this._config$$.value.index
          : 0,
    }));
  }

  public isConfigurationLoaded(): boolean {
    return !!this._trajectory$$.value && !!this._config$$.value;
  }

  /**
   *
   * @param trj
   * @param showConfig The configuration to be displayed. By default 0. A negative number means that no configuration is displayed.
   */
  public setTrajectory(trj: PartialTrajectory | null, showConfig = 0): void {
    if (!trj) this.unloadTrajectory();
    else {
      // TODO: renderCount
      this._trajectory$$.next(trj);
      this.setConfig(showConfig);
    }
  }

  public unloadTrajectory(): void {
    this.currentFile = undefined;
    this._session = undefined;
    this._config$$.next(null);
    this._trajectory$$.next(null);
  }

  public ngOnDestroy(): void {
    if (this._worker) this._worker.terminate();
  }

  public loadFile(file: File, session: Session | undefined = undefined): void {
    if (!this._worker) return;

    this._session = session;
    this.currentFile = file;

    this._currentFileName = file.name;
    this._worker.postMessage({
      message: EXCHANGE_LOAD_TRAJECTORY,
      data: file,
    } as ExchangeFormat<File>);
  }

  public setConfig(configIndex: number): void {
    // this._loadingMessage.next(`Parsing configuration ${configIndex}`);

    this._worker?.postMessage({
      message: EXCHANGE_CONFIGURATION,
      data: configIndex,
    } as ExchangeFormat<number>);
  }

  private init(): void {}
}
