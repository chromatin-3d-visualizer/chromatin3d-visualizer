/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { WorkspaceService } from './workspace.service';
import CohesinHeadBead from '../../shared/models/cohesin-head-bead.model';
import { ColorsService } from './colors/colors.service';
import { ViewService } from './view/view.service';
import Bead from '../../shared/models/bead.model';
import { PbcService } from './pbc.service';
import DNABead from '../../shared/models/dna-bead.model';
import { DEFAULT_COLOR_COHESIN_COLORING_DNA } from '../../shared/color.constants';
import { getLinkerDNASegments } from '../../shared/utilities/bead.utility';
import { Configuration } from '../../shared/models/configuration.model';

@Injectable({
  providedIn: 'root',
})
export class CohesinService {
  private readonly _hasCohesin$$ = new BehaviorSubject<boolean>(false);
  private _lastColoringLinkerDNAIndices: number[] = []; // contains the indices of linker DNA segment, NOT bead index!

  public readonly hasCohesin$ = this._hasCohesin$$.asObservable();

  public get hasCohesinMode(): boolean {
    return this._hasCohesin$$.value;
  }

  public constructor(
    private readonly _workspaceService: WorkspaceService,
    private readonly _colorsService: ColorsService,
    private readonly _viewService: ViewService,
    private readonly _pbcService: PbcService
  ) {
    // workaround circular dependency
    this.hasCohesin$.subscribe(
      (has) => (this._colorsService.trajectoryHasCohesin = has)
    );
    this._workspaceService.config$.subscribe((conf) => {
      const trajectory = this._workspaceService.trajectory;
      if (!conf || !trajectory) {
        this.clearLastLinkerDNACohesinColoring();
        this._hasCohesin$$.next(false);
        return;
      }

      // last time was cohesin => linker dna was coloring => clear colors
      if (this._hasCohesin$$.value) this.clearLastLinkerDNACohesinColoring();

      const hasCohesin =
        this.getCohesinThicknessFromConfig(conf.config) > 0 &&
        this.getCohesinRadiusFromConfig(conf.config) > 0 &&
        this.getCohesinHeadRadiusFromConfig(conf.config) > 0 &&
        this.getCohDistanceFromConfig(conf.config) > 0;
      this._hasCohesin$$.next(hasCohesin);

      if (!hasCohesin) return;

      this.coloringLinkerDNA();
    });
  }

  private static getLinkerDNAIDsToColoring(beads: Bead[]): number[] {
    const linkerDNAIDsToColoring: number[] = [];
    for (let i = 0; i < beads.length; ++i) {
      if (!(beads[i] instanceof CohesinHeadBead)) continue;

      // coloring always the previous dna bead
      let linkerDNAIndex = i;
      while (--linkerDNAIndex >= 0) {
        if (!(beads[linkerDNAIndex] instanceof DNABead)) continue;

        linkerDNAIDsToColoring.push(beads[linkerDNAIndex].id);
        break;
      }
    }

    return linkerDNAIDsToColoring;
  }

  private static getSegmentIndicesToColoring(
    linkerDNASegments: Bead[][],
    linkerDNAIDsToColoring: number[]
  ): number[] {
    const segmentIndicesToColoring: number[] = [];
    for (let i = 0; i < linkerDNASegments.length; ++i) {
      const coloringIndex = !!linkerDNASegments[i].find((b) =>
        linkerDNAIDsToColoring.includes(b.id)
      );
      if (coloringIndex) segmentIndicesToColoring.push(i);
    }

    return segmentIndicesToColoring;
  }

  public getCohesinThickness(): number {
    return this.getCohesinThicknessFromConfig(
      this._workspaceService.config?.config
    );
  }

  public getCohesinThicknessFromConfig(
    config: Configuration | undefined
  ): number {
    let cohesinThickness =
      this._workspaceService.trajectory?.globalParameters?.ringThickness ?? -1;
    const configThickness = config?.parameters?.ringThickness ?? 0;
    if (configThickness > 0) cohesinThickness = configThickness;

    return cohesinThickness;
  }

  public getCohesinRadius(): number {
    return this.getCohesinRadiusFromConfig(
      this._workspaceService.config?.config
    );
  }

  public getCohesinRadiusFromConfig(config: Configuration | undefined): number {
    let cohesinRadius =
      this._workspaceService.trajectory?.globalParameters?.ringRadius ?? -1;
    const configRadius = config?.parameters?.ringRadius ?? 0;
    if (configRadius > 0) cohesinRadius = configRadius;

    return cohesinRadius;
  }

  public getCohesinHeadRadius(): number {
    return this.getCohesinHeadRadiusFromConfig(
      this._workspaceService.config?.config
    );
  }

  public getCohesinHeadRadiusFromConfig(
    config: Configuration | undefined
  ): number {
    let cohesinHeadRadius =
      this._workspaceService.trajectory?.globalParameters?.cohesinHeadRadius ??
      -1;
    const configHeadRadius = config?.parameters?.cohesinHeadRadius ?? 0;
    if (configHeadRadius > 0) cohesinHeadRadius = configHeadRadius;

    return cohesinHeadRadius;
  }

  public getCohDistance(): number {
    return this.getCohDistanceFromConfig(this._workspaceService.config?.config);
  }

  public getCohDistanceFromConfig(config: Configuration | undefined): number {
    let cohesinDistance =
      this._workspaceService.trajectory?.globalParameters?.cohDistance ?? -1;
    const configDistance = config?.parameters?.cohDistance ?? 0;
    if (configDistance > 0) cohesinDistance = configDistance;

    return cohesinDistance;
  }

  private clearLastLinkerDNACohesinColoring(): void {
    this._colorsService.linkerDNAColors.setConfigSpecificColors(
      this._colorsService.linkerDNAColors.color, // default color = reset color
      this._lastColoringLinkerDNAIndices
    );
    this._lastColoringLinkerDNAIndices = [];
  }

  private coloringLinkerDNA(): void {
    const beads = this._workspaceService.config?.config.beads ?? [];
    const linkerDNAIDsToColoring: number[] =
      CohesinService.getLinkerDNAIDsToColoring(beads);
    const linkerDNASegments = this.getLinkerDNASegments(beads);
    const segmentIndicesToColoring = CohesinService.getSegmentIndicesToColoring(
      linkerDNASegments,
      linkerDNAIDsToColoring
    );

    this._colorsService.linkerDNAColors.setConfigSpecificColors(
      DEFAULT_COLOR_COHESIN_COLORING_DNA,
      segmentIndicesToColoring
    );
    this._lastColoringLinkerDNAIndices = segmentIndicesToColoring;
  }

  private getLinkerDNASegments(beads: Bead[]): Bead[][] {
    return getLinkerDNASegments(
      beads,
      this._pbcService.pbcMode ? this._viewService.linkerDNASplitter : []
    );
  }
}
