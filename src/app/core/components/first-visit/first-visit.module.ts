/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntroDialogComponent } from './intro-dialog/intro-dialog.component';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { LOCAL_STORAGE_FIRST_VISIT } from '../../../shared/local-storage-keys.constants';
import { parseBoolean } from '../../../shared/utilities/parser.utility';

@NgModule({
  declarations: [IntroDialogComponent],
  imports: [CommonModule, MatDialogModule, MatButtonModule],
})
export class FirstVisitModule {
  public constructor(private readonly _matDialog: MatDialog) {
    // Make sure to show the help dialog when the user first opens the app,
    // then, never show it again
    const isFirstVisit = localStorage.getItem(LOCAL_STORAGE_FIRST_VISIT);

    if (!isFirstVisit || parseBoolean(isFirstVisit)) {
      const dialogRef = this._matDialog.open(IntroDialogComponent);
      dialogRef.afterClosed().subscribe(() => {
        localStorage.setItem(LOCAL_STORAGE_FIRST_VISIT, false.toString());
      });
    }
  }
}
