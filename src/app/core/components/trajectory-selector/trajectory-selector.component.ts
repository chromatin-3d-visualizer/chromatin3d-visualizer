/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PaperTrajectoryFile } from '../../../shared/models/paper-trajectory-file.model';
import PaperHelper from '../../../shared/models/paper-helper.model';

const MAXIMAL_AMOUNT_OF_AUTHORS = 3;

@Component({
  selector: 'trj-trajectory-selector',
  templateUrl: './trajectory-selector.component.html',
  styleUrls: ['./trajectory-selector.component.scss'],
})
export class TrajectorySelectorComponent {
  public selectedFile: PaperTrajectoryFile | undefined;

  public readonly paper: PaperHelper;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: PaperHelper,
    private readonly _matDialogRef: MatDialogRef<TrajectorySelectorComponent>,
    private readonly router: Router
  ) {
    this.paper = data;
  }

  setSelectedFile(file: PaperTrajectoryFile | undefined): void {
    this.selectedFile = file;
  }

  submitFile(file: PaperTrajectoryFile): void {
    this.selectedFile = file;

    this.selectFile();
  }

  async selectFile(): Promise<void> {
    if (!this.selectedFile) return;

    await this.router.navigate([], {
      queryParams: { file: this.selectedFile.path },
      queryParamsHandling: 'merge',
    });

    this._matDialogRef.close();
  }

  getAuthors(): string[] {
    return this.paper.paper.authors?.slice(0, MAXIMAL_AMOUNT_OF_AUTHORS) ?? [];
  }

  isAuthorListShorted(): boolean {
    return (this.paper.paper.authors?.length ?? 0) > MAXIMAL_AMOUNT_OF_AUTHORS;
  }
}
