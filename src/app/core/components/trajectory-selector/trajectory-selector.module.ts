/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { filter } from 'rxjs/operators';
import { TrajectorySelectorComponent } from './trajectory-selector.component';
import { TrajectoryListerService } from '../../services/trajectory-lister.service';
import { FileListComponent } from './file-list/file-list.component';

@NgModule({
  declarations: [TrajectorySelectorComponent, FileListComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
  ],
})
export class TrajectorySelectorModule {
  public constructor(
    private readonly _trajectoryListerService: TrajectoryListerService,
    private readonly _matDialog: MatDialog
  ) {
    this._trajectoryListerService.trajectories$
      .pipe(filter((paper) => !!paper && paper.files.length > 0))
      .subscribe((p) => {
        this._matDialog.open(TrajectorySelectorComponent, {
          disableClose: true,
          data: p,
        });
      });
  }
}
