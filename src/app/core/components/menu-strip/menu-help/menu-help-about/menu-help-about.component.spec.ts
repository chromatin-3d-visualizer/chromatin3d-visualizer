/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuHelpAboutComponent } from './menu-help-about.component';
import {
  APPLICATION_IMPORTS,
  MATERIAL_IMPORTS,
} from '../../../../services/__mocks__/imports';

describe('MenuHelpAboutComponent', () => {
  let component: MenuHelpAboutComponent;
  let fixture: ComponentFixture<MenuHelpAboutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MenuHelpAboutComponent],
      imports: [...MATERIAL_IMPORTS, ...APPLICATION_IMPORTS],
    }).compileComponents();
  });

  beforeEach(() => {
    // @ts-ignore
    window['PACKAGE_VERSION'] = '1.0.0';
    // @ts-ignore
    window['BUILD_DATE'] = '2022/05/06';
    // @ts-ignore
    window['LICENSE'] = '';
    // @ts-ignore
    window['COMMIT'] = '000000';
    fixture = TestBed.createComponent(MenuHelpAboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    // @ts-ignore
    expect(component).toBeTruthy();
  });
});
