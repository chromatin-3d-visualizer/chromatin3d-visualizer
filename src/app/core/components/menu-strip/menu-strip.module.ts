/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuDebugComponent } from './menu-debug/menu-debug.component';
import { MenuFileComponent } from './menu-file/menu-file.component';
import { MenuHelpComponent } from './menu-help/menu-help.component';
import { MenuHelpAboutComponent } from './menu-help/menu-help-about/menu-help-about.component';
import { MenuViewComponent } from './menu-view/menu-view.component';
import { MenuWindowComponent } from './menu-window/menu-window.component';
import { MenuStripComponent } from './menu-strip.component';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import { throwIfAlreadyLoaded } from '../../guards/module-import.guard';
import { SettingsModule } from '../../../modules/settings/settings.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
    MenuDebugComponent,
    MenuFileComponent,
    MenuHelpComponent,
    MenuHelpAboutComponent,
    MenuViewComponent,
    MenuWindowComponent,
    MenuStripComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatMenuModule,
    MatCheckboxModule,
    MatDividerModule,
    MatIconModule,
    MatDialogModule,
    SettingsModule,
    RouterModule,
    MatTooltipModule,
    SharedModule,
  ],
  exports: [MenuStripComponent],
})
export class MenuStripModule {
  public constructor(@Optional() @SkipSelf() parentModule: MenuStripModule) {
    throwIfAlreadyLoaded(parentModule, 'MenuStripModule');
  }
}
