/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component } from '@angular/core';
import { BlobReader, BlobWriter, TextReader, ZipWriter } from '@zip.js/zip.js';
import { MenuStripService } from '../../../services/menu-strip.service';
import { WorkspaceService } from '../../../services/workspace.service';
import { SessionManagerService } from '../../../services/session-mananger/session-manager.service';
import { downloadBlob } from '../../../../shared/utilities/window.utility';
import { CONFIGURATION_FILE_NAME } from '../../../../shared/utilities/file.utility';
import { TrajectoryListerService } from '../../../services/trajectory-lister.service';
import { GITLAB_DOMAIN } from '../../../../constants';
import { ActivatedRoute, Router } from '@angular/router';

const ISO_TIME_SPLICE_LENGTH = 19;

@Component({
  selector: 'trj-menu-file',
  templateUrl: './menu-file.component.html',
  styleUrls: ['./menu-file.component.scss'],
})
export class MenuFileComponent {
  constructor(
    public readonly workspaceService: WorkspaceService,
    public readonly trajectoryListerService: TrajectoryListerService,
    private readonly _msService: MenuStripService,
    private readonly _sessionManagerService: SessionManagerService,
    private readonly _menuStripService: MenuStripService,
    private readonly _route: ActivatedRoute,
    private readonly _router: Router
  ) {}

  openFileSelector(): void {
    this._msService.openFileDialog$.next(null);
  }

  unloadTrajectory(): void {
    this.workspaceService.setTrajectory(null);
    this._router.navigate([], { relativeTo: this._route, queryParams: {} });
  }

  /**
   * Exports the model displayed in the viewer.
   */
  exportModel(): void {
    this._menuStripService.startGLTFExport$.next(null);
  }

  async exportView(): Promise<void> {
    const blobWriter = new BlobWriter('application/zip');
    const writer = new ZipWriter(blobWriter);

    if (this.workspaceService.currentFile)
      await writer.add(
        this.workspaceService.currentFile.name ?? 'trj.trj',
        new BlobReader(this.workspaceService.currentFile)
      );

    await writer.add(
      CONFIGURATION_FILE_NAME,
      new TextReader(JSON.stringify(this._sessionManagerService.getSession()))
    );

    const blob = await writer.close(
      new TextEncoder().encode(
        `Created by Chromatin 3D Visualizer (${GITLAB_DOMAIN})`
      )
    );
    downloadBlob(
      blob,
      `export_${new Date()
        .toISOString()
        .substring(0, ISO_TIME_SPLICE_LENGTH)
        .replace(/:/g, '.')}.zip`
    );

    console.log(this._sessionManagerService.getSession());
  }

  isLoadedFile(file: string): boolean {
    // determines if a TOC file is currently loaded
    // note that it is possible that the file comes from a zip (recursive).
    // For this reason it must be checked if the query "file path" matches the
    // first triggeredFiles
    return (
      this.workspaceService.currentFile?.name === file ||
      (this.workspaceService.triggeredFiles.length > 0 &&
        this.workspaceService.triggeredFiles[0] === file)
    );
  }
}
