/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DebugService } from '../../services/debug.service';
import { SettingsComponent } from '../../../modules/settings/settings.component';

@Component({
  selector: 'trj-menu-strip',
  templateUrl: './menu-strip.component.html',
  styleUrls: ['./menu-strip.component.scss'],
})
export class MenuStripComponent {
  constructor(
    public readonly debugService: DebugService,
    private readonly _dialog: MatDialog
  ) {}

  openSettingsDialog(): void {
    this._dialog.open(SettingsComponent, {
      width: '70%',
    });
  }
}
