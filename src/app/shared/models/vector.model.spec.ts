/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Vector from './vector.model';

describe('Vector Model', () => {
  it('should compute the length of a vector', () => {
    expect(new Vector(12, 0, 5).length).toEqual(13);
  });

  it('should clone a vector', () => {
    const vector = new Vector(-5, 0, 12);
    const clone = vector.clone();
    const adder = -1;
    clone.add(adder);
    expect(vector.x).toEqual(-5);
    expect(vector.y).toEqual(0);
    expect(vector.z).toEqual(12);

    expect(clone.x).toEqual(-5 + adder);
    expect(clone.y).toEqual(adder);
    expect(clone.z).toEqual(12 + adder);
  });
});
