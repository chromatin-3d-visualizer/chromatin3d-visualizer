/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { PaperTrajectoryFile } from './paper-trajectory-file.model';
import { PaperInfo } from './paper-info.model';

export default class PaperHelper {
  public readonly paper: PaperInfo;
  public readonly files: PaperTrajectoryFile[];

  public constructor(model: Partial<PaperHelper>) {
    if (!model.paper) throw new Error('Paper info are missing!');
    if (!model.files) throw new Error('Files are missing in the paper!');

    if (!model.paper.title) throw new Error('Paper title is missing!');

    this.paper = model.paper;
    this.files = model.files;
  }
}
