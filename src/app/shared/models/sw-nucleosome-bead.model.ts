/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Quaternion } from 'three';
import NucleosomeBead from './nucleosome-bead.model';
import { parseQuaternionOrReturnDefaultQuaternion } from '../utilities/three.js.utility';

/**
 * Represents a nucleosome bead in this software.
 */
export default class SoftwareNucleosomeBead extends NucleosomeBead {
  public rotation: Quaternion;

  public constructor(nucleosomeBead: Partial<SoftwareNucleosomeBead>) {
    super(nucleosomeBead);

    this.rotation = parseQuaternionOrReturnDefaultQuaternion(
      nucleosomeBead.rotation
    );
  }
}
