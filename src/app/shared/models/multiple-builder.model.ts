/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Texture } from 'three';
import InstancedObjectBuilder from '../../modules/dockable-workspace/viewer/beads/instanced-object.builder';
import QualityLevel from '../../core/services/session-mananger/models/quality-level.model';

export type MultipleBuilder = new (
  beads: any,
  texture: Texture,
  qualityLevel: QualityLevel,
  renderSize: number
) => InstancedObjectBuilder<any, any, any, any>;

export type MultipleLinkerBuilder = new (
  beads: any,
  texture: Texture,
  qualityLevel: QualityLevel,
  renderSize: number,
  splice: number[]
) => InstancedObjectBuilder<any, any, any, any>;
