/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import IMetaInfo from './meta-info.interface';

export default class MetaInfo implements IMetaInfo {
  public readonly date: number;
  public readonly version: string;
  public readonly program: string;
  public readonly schemaVersion: string | number | undefined;

  public constructor(model: Partial<IMetaInfo>) {
    this.date = model.date ?? Date.now();
    this.version = model.version ?? '3.0.1';
    this.program = model.program ?? 'webViewer';
    this.schemaVersion = model.schemaVersion;
  }
}
