/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Position from './position.model';

const LENGTH_TOLERANCE = 1e-4;

type Coordinates = {
  x: number;
  y: number;
  z: number;
};

/**
 * Represents a 3-dimensional vector of a (bead) object.
 */
export default class Vector extends Position {
  // region Statics Functions
  public static normalize(vector: Vector): Vector {
    const len = vector.length;

    // check if already normalized
    if (Math.abs(len - 1.0) < LENGTH_TOLERANCE) return vector;

    if (len === 0) throw new Error('Invalid vector length.');

    // build a new normalized vector
    return new Vector(vector.x / len, vector.y / len, vector.z / len);
  }

  public static crossProduct(vec1: Vector, vec2: Vector): Vector {
    return new Vector(
      vec1.y * vec2.z - vec1.z * vec2.y,
      vec1.z * vec2.x - vec1.x * vec2.z,
      vec1.x * vec2.y - vec1.y * vec2.x
    );
  }

  public static dotProduct(vec1: Vector, vec2: Vector): number {
    return vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z;
  }

  public static add(vec1: Vector, vec2: Vector | number): Vector {
    return new Vector(vec1.x, vec1.y, vec1.z).add(vec2);
  }

  public static mult(vec1: Vector, vec2: number): Vector {
    return new Vector(vec1.x, vec1.y, vec1.z).mult(vec2);
  }

  public static div(vec1: Vector, vec2: Vector | number): Vector {
    return new Vector(vec1.x, vec1.y, vec1.z).div(vec2);
  }
  // endregion

  public clone(): Vector {
    return new Vector(this._x, this._y, this._z);
  }

  public get length(): number {
    return Math.sqrt(
      Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2)
    );
  }

  public mult(multer: number | Coordinates): Vector {
    if ((multer as Coordinates).x !== undefined) {
      this._x *= (multer as Coordinates).x;
      this._y *= (multer as Coordinates).y;
      this._z *= (multer as Coordinates).z;
    } else {
      this._x *= multer as number;
      this._y *= multer as number;
      this._z *= multer as number;
    }

    return this;
  }

  public add(adder: Vector | number): Vector {
    super.add(adder);
    return this;
  }

  public sub(adder: Vector | number): Vector {
    if (adder instanceof Vector)
      super.add(new Vector(-adder.x, -adder.y, -adder.z));
    else super.add(-adder);

    return this;
  }

  public div(diver: Vector | number): Vector {
    if (diver instanceof Vector) {
      this._x /= diver.x;
      this._y /= diver.y;
      this._z /= diver.z;
    } else {
      this._x /= diver;
      this._y /= diver;
      this._z /= diver;
    }

    return this;
  }
}
