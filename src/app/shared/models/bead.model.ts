/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import IBead from './bead.interface';
import Position from './position.model';
import Vector from './vector.model';
import {
  parsePositionOrReturnDefaultPosition,
  parseVectorOrReturnDefaultVector,
} from '../utilities/parser.utility';

/**
 * Represents a bead.
 */
export default abstract class Bead implements IBead {
  /** Specifies the id of the bead. */
  public readonly id: number;
  public readonly equilibriumSegmentLength: number;
  public readonly intrinsicTwist: number;
  /** Specifies the position of the bead. */
  public readonly position: Position;

  public drawPosition: Position;
  public readonly fVector: Vector;
  public readonly bendVector: Vector;
  public readonly segmentVector: Vector;

  public readonly marker?: string;

  protected constructor(bead: Partial<IBead>) {
    if (!bead.id && bead.id !== 0) throw new Error('The bead needs an id.');
    if (!bead.position) throw new Error('The bead needs a position.');

    this.id = bead.id;
    this.equilibriumSegmentLength = bead.equilibriumSegmentLength ?? 0;
    this.intrinsicTwist = bead.intrinsicTwist ?? 0;
    this.position = parsePositionOrReturnDefaultPosition(bead.position);
    this.drawPosition = parsePositionOrReturnDefaultPosition(bead.drawPosition);
    this.fVector = parseVectorOrReturnDefaultVector(bead.fVector);
    this.bendVector = parseVectorOrReturnDefaultVector(bead.bendVector);
    this.segmentVector = parseVectorOrReturnDefaultVector(bead.segmentVector);

    this.marker = bead.marker;
  }
}
