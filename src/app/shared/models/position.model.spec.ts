/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Position from './position.model';

describe('Position Model', () => {
  it('should get the values (x,y,z)', () => {
    const position = new Position(12, -5, 0);
    expect(position.x).toEqual(12);
    expect(position.y).toEqual(-5);
    expect(position.z).toEqual(0);
  });

  it('should add a value to each coordinate', () => {
    const position = new Position(12, -5, 0);
    const adder = 5;
    position.add(adder);
    expect(position.x).toEqual(12 + adder);
    expect(position.y).toEqual(-5 + adder);
    expect(position.z).toEqual(adder);
  });

  it('should add a position to the current position', () => {
    const position = new Position(12, -5, 0);
    const adder = new Position(-2, 0, 44);
    position.add(adder);
    expect(position.x).toEqual(12 + adder.x);
    expect(position.y).toEqual(-5 + adder.y);
    expect(position.z).toEqual(adder.z);
  });

  it('should clone a position', () => {
    const position = new Position(12, -5, 0);
    const clone = position.clone();
    const adder = 5;
    clone.add(adder);
    expect(position.x).toEqual(12);
    expect(position.y).toEqual(-5);
    expect(position.z).toEqual(0);

    expect(clone.x).toEqual(12 + adder);
    expect(clone.y).toEqual(-5 + adder);
    expect(clone.z).toEqual(adder);
  });

  it('should compute the difference of two positions', () => {
    const position1 = new Position(12, -5, 0);
    const position2 = new Position(2, -2, 12);
    expect(Position.difference(position1, position2)).toEqual(
      new Position(
        position2.x - position1.x,
        position2.y - position1.y,
        position2.z - position1.z
      )
    );
  });
});
