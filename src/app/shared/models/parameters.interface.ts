/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

/**
 * Represents a partial of parameters that members are not "read only" and optional.
 */
export default interface IParameters {
  [key: string]: number | boolean;

  numberOfBeads: number;
  radiusDNA: number;
  radiusNucleosome: number;
  heightNucleosome: number;
  chainIsClosed: boolean;
  pbcContainerStartDimension: number;

  cubeSizeLJ: number;
  cubeSizeDNANucIntersection: number;
  cubeSizeEstat: number;

  cohesinHeadRadius: number;
  ringThickness: number;
  ringRadius: number;
  cohDistance: number;

  temperature: number;
  eStatTemperature: number; // electro static temperature
}
