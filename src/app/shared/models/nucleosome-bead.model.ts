/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import Bead from './bead.model';
import INucleosomeBead from './nucleosome-bead.interface';
import Position from './position.model';
import Vector from './vector.model';
import { BEAD_MARKER_NUCLEOSOME } from '../parsers/block-trajectory/constants.static';
import {
  parsePositionOrReturnDefaultPosition,
  parseVectorOrReturnDefaultVector,
} from '../utilities/parser.utility';

/**
 * Represents a nucleosome bead.
 */
export default class NucleosomeBead extends Bead implements INucleosomeBead {
  public readonly centerOrientationVector: Vector;
  public readonly center: Position;

  public constructor(nucleosomeBead: Partial<INucleosomeBead>) {
    super({ ...nucleosomeBead, marker: BEAD_MARKER_NUCLEOSOME });
    this.centerOrientationVector = parseVectorOrReturnDefaultVector(
      nucleosomeBead.centerOrientationVector
    );
    this.center = parsePositionOrReturnDefaultPosition(nucleosomeBead.center);
  }
}
