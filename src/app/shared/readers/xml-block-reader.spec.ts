/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { XmlBlockReader } from './xml-block-reader';

describe('Xml Block Reader', () => {
  it('should read completely text in a chunk', async () => {
    const blob = new Blob([
      '<xml><test><test5>Test6</test5></test><abc></abc></xml>',
    ]);
    const file = new XmlBlockReader(new File([blob], 'test.xml'));
    const blocks: string[] = [];

    for await (const line of file.blocks()) blocks.push(line);

    const block = blocks.join('');
    expect(block).toEqual(
      '<xml><test><test5>Test6</test5></test><abc></abc></xml>'
    );
  });

  it('should read text in multiple chunks and returns completely blocks', async () => {
    const blob = new Blob([
      '<xml><test><test5>Test6</test5></test><abc></abc></xml>',
    ]);
    const file = new XmlBlockReader(new File([blob], 'test.xml'), {
      chunkSize: 5,
    });
    const blocks: string[] = [];

    for await (const line of file.blocks()) blocks.push(line);

    const block = blocks.join('');
    expect(block).toEqual(
      '<xml><test><test5>Test6</test5></test><abc></abc></xml>'
    );
  });

  it('should read an incompletely xml', async () => {
    const blob = new Blob([
      '<xml><test><test5>Test6</test5></test><abc></abc></xml',
    ]);
    const file = new XmlBlockReader(new File([blob], 'test.xml'), {
      chunkSize: 5,
    });
    const blocks: string[] = [];

    for await (const line of file.blocks()) blocks.push(line);

    const block = blocks.join('');
    expect(block).toEqual(
      '<xml><test><test5>Test6</test5></test><abc></abc></xml'
    );
  });

  it('should return the xml element names and values', async () => {
    const blob = new Blob([
      '<xml><test><test5>Test6</test5></test><abc></abc></xml>',
    ]);
    const file = new XmlBlockReader(new File([blob], 'test.xml'));

    const blocks: { matchValue: string; isValue: boolean }[] = [];

    for await (const line of file.blocks())
      blocks.push(
        ...XmlBlockReader.getBlocksAndValues(line).map((b) => ({
          matchValue: b.match[0],
          isValue: b.isValue,
        }))
      );

    expect(blocks).toEqual([
      {
        matchValue: '<xml>',
        isValue: false,
      },
      {
        matchValue: '<test>',
        isValue: false,
      },
      {
        matchValue: '<test5>',
        isValue: false,
      },
      {
        matchValue: 'Test6',
        isValue: true,
      },
      {
        matchValue: '</test5>',
        isValue: false,
      },
      {
        matchValue: '</test>',
        isValue: false,
      },
      {
        matchValue: '<abc>',
        isValue: false,
      },
      {
        matchValue: '</abc>',
        isValue: false,
      },
      {
        matchValue: '</xml>',
        isValue: false,
      },
    ]);
  });
});
