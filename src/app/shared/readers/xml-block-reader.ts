/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ChunkReader } from './chunk-reader';
import { ReadLineOptions } from './models/read-line-options.model';
import { RegExpExecArrayXml } from './models/reg-exp-exec-array-xml.model';

const XML_BLOCK_END_CHAR = 62; // char code of ">"

const REGEX_ELEMENT = /<[^<>]+>/gi;
const REGEX_ELEMENT_CONTENT = /(?<=>)[^<>]+(?=<)/gi;

export class XmlBlockReader extends ChunkReader {
  public constructor(file: File, options?: Partial<ReadLineOptions>) {
    super(file, options);
  }

  public static getBlocksAndValues(input: string): RegExpExecArrayXml[] {
    const matches: RegExpExecArrayXml[] = [];
    let match: RegExpExecArray | null;
    while ((match = REGEX_ELEMENT.exec(input))) {
      if (match[0].trim().length !== 0) matches.push({ match, isValue: false });
    }
    while ((match = REGEX_ELEMENT_CONTENT.exec(input))) {
      if (match[0].trim().length !== 0) matches.push({ match, isValue: true });
    }

    matches.sort((m1, m2) => m1.match.index - m2.match.index);

    return matches;
  }

  public async *blocks(): AsyncGenerator<string> {
    this._lastBytes.splice(0);
    for (
      let offset = this._seek;
      offset < this._readLength;
      offset += this._chunkSize
    ) {
      const chunk = await this.readChunk(offset);
      if (!chunk) continue;

      let bytes = new Uint8Array(chunk);
      let point = bytes.length - 1;

      while (point >= 0 && bytes[point] !== XML_BLOCK_END_CHAR) {
        --point;
      }

      if (point >= 0) {
        bytes = this.getMerged(bytes, point, true);

        yield this._decoder.decode(bytes);
      } else {
        this._lastBytes.push(...bytes);
      }
    }

    if (this._lastBytes.length === 0) return;

    yield this._decoder.decode(new Uint8Array(this._lastBytes));
  }
}
