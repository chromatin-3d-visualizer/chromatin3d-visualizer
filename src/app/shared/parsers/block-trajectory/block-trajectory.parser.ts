/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import BlockParser from '../block-parser.abstract';
import { ETrjBlockState } from './models/trj-block-state.enum';
import * as Constants from './constants.static';
import { getFileExtension } from '../../utilities/file.utility';
import { ReadLine } from '../../readers/read-line';
import { CachedTrajectory } from '../models/cached-trajectory.model';
import PartialTrajectory from '../models/partial-trajectory.model';
import CachedConfigurationReaderParser from './cached-configuration-reader.parser';
import { ConfigurationOffset } from '../models/configuration-offset.model';
import { Configuration } from '../../models/configuration.model';
import { BLOCK_SPECIES } from './constants.static';

const BEGIN_PATTERN = new RegExp(`^${Constants.BLOCK_BEGIN}\\s*`, 'gi');
const END_PATTERN = new RegExp(`^${Constants.BLOCK_END}\\s*`, 'gi');

/**
 * Represents a block trajectory parser (for ".trj" files), which first reads
 * the most necessary information from the whole file, which will be used at
 * runtime.
 */
export class BlockTrajectoryParser extends BlockParser {
  private static readonly PARSING_BLOCKS: string[] = [
    ETrjBlockState.Date,
    ETrjBlockState.Parameters,
    ETrjBlockState.ProgramName,
    ETrjBlockState.SimulationSteps,
    ETrjBlockState.Version,
  ].map((c) => c.toString());

  private readonly _configurationOffsets: ConfigurationOffset[] = [];
  private readonly _configurations: (Partial<Configuration> | undefined)[] = [];
  private readonly _beadsPerConfiguration: number[] = [];
  private _configurationParser: CachedConfigurationReaderParser | null = null;

  public static useThisParser(fileName: string): boolean {
    const ext = getFileExtension(fileName).toLowerCase();
    return ext === 'trj' || ext === '';
  }

  protected async parseTrajectoryFile(file: File): Promise<CachedTrajectory> {
    this.resetTempVariables();

    let lineOffset = -1;
    let lastUpdate = 0;
    for await (const line of new ReadLine(file, {
      chunkSize: BlockTrajectoryParser.CHUNK_SIZE,
    }).lines()) {
      ++lineOffset; // +1 coz new line char

      if (line.match(BEGIN_PATTERN)) {
        this.parsingBlockBegin(line, lineOffset);
      } else if (line.match(END_PATTERN)) {
        this.parsingBlockEnd(line, lineOffset);
      } else if (
        this.validationStack[this.validationStack.length - 1] === BLOCK_SPECIES
      ) {
        this.addSpecies();
      } else if (
        this.validationStack.length > 0 &&
        this.validationStack.find((s) =>
          BlockTrajectoryParser.PARSING_BLOCKS.includes(s)
        ) &&
        this._configurationParser
      ) {
        this._configurationParser.parseLine(line);
      }

      lineOffset += line.length; // +1 coz newline char

      const currentUpdate = Date.now();
      if (
        Math.abs(currentUpdate - lastUpdate) >
        BlockTrajectoryParser.UPDATE_NOTIFICATION_DURATION
      ) {
        this._readBytes.next(lineOffset);
        lastUpdate = currentUpdate;
      }
    }

    this.checkValidationStack();

    return {
      trajectory: new PartialTrajectory({
        configurationLength: this._configurationOffsets.length,
        maxBeadLength: this.getMaxBeadLength(),
      }),
      file,
      configurations: this._configurationOffsets,
    };
  }

  private resetTempVariables(): void {
    this.validationStack.splice(0);
    this._configurationOffsets.splice(0);
    this._configurations.splice(0);
    this._beadsPerConfiguration.splice(0);
    this._configurationParser = null;
  }

  private parsingBlockBegin(line: string, lineOffset: number): void {
    const blockName = line.replace(BEGIN_PATTERN, '').trim().toLowerCase();
    this.addBlockToStack(blockName);

    if (blockName === Constants.BLOCK_CONFIGURATION) {
      this._configurationOffsets.push({ offset: lineOffset, endOffset: -1 });
      this._configurationParser = new CachedConfigurationReaderParser();
    }

    this._configurationParser?.parseLine(line);
  }

  private parsingBlockEnd(line: string, lineOffset: number): void {
    const blockName = line.replace(END_PATTERN, '').trim().toLowerCase();
    this.popBlockFromStackAndCheckValidation(blockName, lineOffset);

    this._configurationParser?.parseLine(line);

    if (this.validationStack.length !== 0 || !this._configurationParser) return;

    if (
      this._configurationOffsets.length > 0 &&
      this._configurationOffsets[this._configurationOffsets.length - 1]
        .endOffset < 0
    )
      this._configurationOffsets[
        this._configurationOffsets.length - 1
      ].endOffset = lineOffset + line.length;
    else console.warn('Could not set endOffset of configuration!');

    this._configurations.push(this._configurationParser.getResult());
    this._configurationParser = null;
  }

  private addSpecies(): void {
    // +1 coz configuration doesn't exist while parsing
    while (this._configurations.length + 1 > this._beadsPerConfiguration.length)
      this._beadsPerConfiguration.push(0);

    this._beadsPerConfiguration[this._beadsPerConfiguration.length - 1] += 1;
  }

  private getMaxBeadLength(): number {
    let max = 0;
    for (const count of this._beadsPerConfiguration) max = Math.max(max, count);

    return max;
  }
}
