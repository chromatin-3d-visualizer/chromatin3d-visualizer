/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import IFakeConfiguration from './models/fake-configuration.interface';
import { ETrjBlockState } from './models/trj-block-state.enum';
import { ETrjChainState } from './models/trj-chain-state.enum';
import Vector from '../../models/vector.model';
import {
  parseNumber,
  getLintFriendlyVariableName,
} from '../../utilities/parser.utility';
import * as Constants from './constants.static';
import * as ParserConstants from '../constants.static';
import IBlockTrajectoryBead from './models/block-trajectory-bead.interface';
import { getValueOfEnumKey } from '../../utilities/enum.utility';
import { Configuration } from '../../models/configuration.model';
import IMetaInfo from '../../models/meta-info.interface';
import IParameters from '../../models/parameters.interface';
import Position from '../../models/position.model';
import IBlockTrajectoryNucleosomeBead from './models/block-trajectory-nucleosome-bead.interface';
import { CohesinRing } from '../../models/cohesin-ring.model';
import { convertFakeConfigurationToOriginal } from './converter.helper';

const BEGIN_PATTERN = new RegExp(`^${Constants.BLOCK_BEGIN}\\s*`, 'gi');
const END_PATTERN = new RegExp(`^${Constants.BLOCK_END}\\s*`, 'gi');
const SIM_STEPS_MC_PATTERN = new RegExp(
  `^${Constants.BLOCK_CONTENT_SIM_STEPS_MC}\\s*`,
  'gi'
);

export default class ConfigurationReaderParser {
  protected result: Configuration | undefined;
  protected currentConfiguration: IFakeConfiguration | undefined;
  private _configurationState: ETrjBlockState = ETrjBlockState.None;
  private _chainState: ETrjChainState = ETrjChainState.None;

  private _speciesList: string[] = [];
  private _beadCounter = 0;
  private _subBlockLineCounter = 0;

  private _firstFVector: Vector | undefined;
  private _validationStack: string[] = [];

  private readonly _chainStateHandlers: {
    key: ETrjChainState;
    caller: (lines: string[]) => void;
  }[] = [
    {
      key: ETrjChainState.Coordinates,
      caller: this.chainCoordinates.bind(this),
    },
    {
      key: ETrjChainState.EquilibriumSegLength,
      caller: this.chainEquilibriumSegLength.bind(this),
    },
    {
      key: ETrjChainState.fvec,
      caller: this.chainFVector.bind(this),
    },
    {
      key: ETrjChainState.TwistAng,
      caller: this.chainTwistAng.bind(this),
    },
    {
      key: ETrjChainState.BendAng,
      caller: this.chainBendAngles.bind(this),
    },
    {
      key: ETrjChainState.ITwistAng,
      caller: this.chainITwistAng.bind(this),
    },
    {
      key: ETrjChainState.NucCenterDistance,
      caller: this.chainNucleosomeCenterDistance.bind(this),
    },
    {
      key: ETrjChainState.NucRot,
      caller: this.chainNucleosomeRotation.bind(this),
    },
    {
      key: ETrjChainState.CohesinRings,
      caller: this.chainCohesinRings.bind(this),
    },
  ];

  private readonly _configurationStateHandlers: {
    key: ETrjBlockState;
    caller: (lines: string) => void;
  }[] = [
    {
      key: ETrjBlockState.Date,
      caller: this.setDate.bind(this),
    },
    {
      key: ETrjBlockState.SimulationSteps,
      caller: this.setSimulationStep.bind(this),
    },
    {
      key: ETrjBlockState.Parameters,
      caller: this.setParameter.bind(this),
    },
    {
      key: ETrjBlockState.Chain,
      caller: this.setChain.bind(this),
    },
  ];

  private static getVectorFromLine(lineInformation: string[]): Vector {
    if (lineInformation.length !== 3)
      throw new Error('Invalid vector information!');

    const coordsX = parseNumber(lineInformation[0]);
    const coordsY = parseNumber(lineInformation[1]);
    const coordsZ = parseNumber(lineInformation[2]);

    return new Vector(coordsX, coordsY, coordsZ);
  }

  public getResult(): Configuration {
    if (!this.result) throw new Error('Invalid configuration! Parsing failed!');

    this.checkValidationStack();
    return this.result;
  }

  public parseLine(line: string): boolean {
    line = line.trim();

    if (line.match(BEGIN_PATTERN)) {
      this.parsingBlockBegin(line);
    } else if (line.match(END_PATTERN)) {
      return this.parsingBlockEnd(line, 0);
    } else {
      if (!this._configurationState) return false;

      this.parsingConfigurationState(line);
    }

    return false;
  }

  protected endConfiguration(): boolean {
    if (!this.currentConfiguration || !this._firstFVector) throw new Error('');

    this.result = convertFakeConfigurationToOriginal(
      this.currentConfiguration,
      this._speciesList,
      this._firstFVector
    ) as Configuration;

    return true;
  }

  private parsingBlockBegin(line: string): void {
    const blockName = line.replace(BEGIN_PATTERN, '').toLowerCase();
    this._validationStack.push(blockName);
    this.setBlockState(blockName);
  }

  private parsingBlockEnd(line: string, lineCounter: number): boolean {
    if (this._validationStack.length === 0)
      throw new Error(`The file content is invalid at ${lineCounter}.`);

    const lastItem = this._validationStack[this._validationStack.length - 1];
    if (lastItem === line.replace(END_PATTERN, '').toLowerCase()) {
      this._validationStack.pop();
      return this.removeBlockState(lastItem);
    } else {
      throw new Error(`The file content is invalid at ${lineCounter}.`);
    }
  }

  private parsingConfigurationState(line: string): void {
    const handler = this._configurationStateHandlers.find(
      (h) => h.key === this._configurationState
    );
    handler?.caller(line);
  }

  private checkValidationStack(): void {
    if (this._validationStack.length > 0)
      throw new Error(`The file content is invalid, block not closed.`);
  }

  // region BlockState Stuff
  private setBlockState(blockName: string): void {
    const oldChainState = this._chainState;
    const oldConfigState = this._configurationState;

    if (blockName === Constants.BLOCK_CONFIGURATION) {
      this.currentConfiguration = {
        beads: [] as IBlockTrajectoryBead[],
      } as IFakeConfiguration;
    } else {
      const enumVal = getValueOfEnumKey<ETrjBlockState>(
        ETrjBlockState,
        blockName
      );

      const enumValChainState = getValueOfEnumKey<ETrjChainState>(
        ETrjChainState,
        blockName
      );

      if (enumVal) this._configurationState = enumVal;
      if (enumValChainState) this._chainState = enumValChainState;
    }

    if (
      this._configurationState !== oldConfigState ||
      this._chainState !== oldChainState
    ) {
      this._beadCounter = 0;
      this._subBlockLineCounter = 0;
    }
  }

  private removeBlockState(blockName: string): boolean {
    if (blockName === Constants.BLOCK_CONFIGURATION) {
      if (this.currentConfiguration) {
        return this.endConfiguration();
      }

      this._speciesList.splice(0);
      return false;
    }

    const enumVal = getValueOfEnumKey<ETrjBlockState>(
      ETrjBlockState,
      blockName
    );
    if (enumVal) {
      this._configurationState = ETrjBlockState.None;
      if (enumVal === ETrjBlockState.Chain) {
        this._chainState = ETrjChainState.None;
      }

      return false;
    }

    const enumValChainState = getValueOfEnumKey<ETrjChainState>(
      ETrjChainState,
      blockName
    );
    if (enumValChainState) this._chainState = ETrjChainState.None;

    return false;
  }
  // endregion

  // region Parsing
  private setDate(date: string): void {
    if (this.currentConfiguration) {
      if (!this.currentConfiguration.metaInfo)
        this.currentConfiguration.metaInfo = {} as IMetaInfo;

      this.currentConfiguration.metaInfo.date = Date.parse(date);
    }
  }

  private setSimulationStep(steps: string): void {
    if (!this.currentConfiguration) throw new Error();

    if (steps.match(SIM_STEPS_MC_PATTERN)) {
      this.currentConfiguration.mcSteps = parseNumber(
        steps.replace(SIM_STEPS_MC_PATTERN, '').trim()
      );
    }
  }

  private setParameter(line: string): void {
    if (!this.currentConfiguration) throw new Error();
    if (!this.currentConfiguration.parameters)
      this.currentConfiguration.parameters = {} as IParameters;

    const lineInfo = line.split(/\s+/);
    if (lineInfo.length === 2) {
      const attrNameInClass = getLintFriendlyVariableName(
        ParserConstants.ATTRIBUTE_RENAMER[lineInfo[0]] ?? lineInfo[0]
      );
      this.currentConfiguration.parameters[attrNameInClass] = parseNumber(
        lineInfo[1]
      );
    }
  }

  private setChain(line: string): void {
    if (
      this._subBlockLineCounter++ === 0 &&
      this._chainState !== ETrjChainState.fvec &&
      this._chainState !== ETrjChainState.CohesinRings
    )
      return; // ignore the line "N #NUMER"

    if (this._chainState === ETrjChainState.Species) {
      this.chainSpecies(line);
    } else {
      // read the information in the line
      const lineInformation = line.split(/\s+/g);
      const handler = this._chainStateHandlers.find(
        (h) => h.key === this._chainState
      );
      handler?.caller(lineInformation);
    }

    ++this._beadCounter;
  }

  private chainSpecies(line: string): void {
    this._speciesList.push(line);
  }

  private chainCoordinates(lineInformation: string[]): void {
    if (!this.currentConfiguration) throw new Error();

    if (!this.currentConfiguration.beads) this.currentConfiguration.beads = [];

    this.currentConfiguration.beads.push({
      position: ConfigurationReaderParser.getVectorFromLine(
        lineInformation
      ) as Position,
    } as IBlockTrajectoryBead);

    // calculate segment vector
    if (this._beadCounter > 0) {
      const diff = Position.difference(
        this.currentConfiguration.beads[this._beadCounter - 1].position,
        this.currentConfiguration.beads[this._beadCounter].position
      );
      this.currentConfiguration.beads[this._beadCounter - 1].segmentVector =
        new Vector(diff.x, diff.y, diff.z);

      // copy segmentVector to last bead
      if (this._beadCounter === this.currentConfiguration.beads.length - 1) {
        this.currentConfiguration.beads[this._beadCounter].segmentVector =
          this.currentConfiguration.beads[this._beadCounter - 1].segmentVector;
      }
    }
  }

  private chainEquilibriumSegLength(lineInformation: string[]): void {
    if (!this.currentConfiguration) throw new Error();

    this.currentConfiguration.beads[
      this._beadCounter
    ].equilibriumSegmentLength = parseNumber(lineInformation[0]);
  }

  private chainFVector(lineInformation: string[]): void {
    this._firstFVector =
      ConfigurationReaderParser.getVectorFromLine(lineInformation);
  }

  private chainTwistAng(lineInformation: string[]): void {
    if (!this.currentConfiguration) throw new Error();

    if (this._beadCounter === 0)
      this.currentConfiguration.beads[0].twistAngle = 0;

    if (this._beadCounter + 1 >= this.currentConfiguration.beads.length)
      throw new Error('More twist angles as beads!');

    this.currentConfiguration.beads[this._beadCounter + 1].twistAngle =
      parseNumber(lineInformation[0]);
  }

  private chainBendAngles(lineInformation: string[]): void {
    if (!this.currentConfiguration) throw new Error();

    this.currentConfiguration.beads[this._beadCounter].bendAngles = [
      parseNumber(lineInformation[0]),
      parseNumber(lineInformation[1]),
    ];
  }

  private chainITwistAng(lineInformation: string[]): void {
    if (!this.currentConfiguration) throw new Error();

    this.currentConfiguration.beads[this._beadCounter].intrinsicTwist =
      parseNumber(lineInformation[0]);
  }

  private chainNucleosomeCenterDistance(lineInformation: string[]): void {
    if (!this.currentConfiguration) throw new Error();

    const lastNuc = this.getLastNucleosomeIndexFromCurrentBeadCounter();
    (
      this.currentConfiguration.beads[lastNuc] as IBlockTrajectoryNucleosomeBead
    ).nucCenterDistance = parseNumber(lineInformation[0]);
  }

  private chainNucleosomeRotation(lineInformation: string[]): void {
    if (!this.currentConfiguration) throw new Error();

    const lastNuc = this.getLastNucleosomeIndexFromCurrentBeadCounter();
    const lastNucBead = this.currentConfiguration.beads[
      lastNuc
    ] as IBlockTrajectoryNucleosomeBead;

    lastNucBead.epsilon = parseNumber(lineInformation[0]);
    lastNucBead.phi = parseNumber(lineInformation[1]);
    lastNucBead.delta = parseNumber(lineInformation[2]);
    lastNucBead.zeta = parseNumber(lineInformation[3]);
  }

  private chainCohesinRings(lineInformation: string[]): void {
    if (lineInformation.length !== 6) return;

    if (!this.currentConfiguration) throw new Error();

    const index = this._subBlockLineCounter - 1; // this._subBlockLineCounter is already increased
    const centerPos = new Position(
      parseNumber(lineInformation[0]),
      parseNumber(lineInformation[1]),
      parseNumber(lineInformation[2])
    );
    const orientationVector = new Vector(
      parseNumber(lineInformation[3]),
      parseNumber(lineInformation[4]),
      parseNumber(lineInformation[5])
    );

    if (!this.currentConfiguration.cohesinRings)
      this.currentConfiguration.cohesinRings = [];

    this.currentConfiguration.cohesinRings.push(
      new CohesinRing({
        index,
        centerPos,
        orientationVector,
      })
    );
  }

  private getLastNucleosomeIndexFromCurrentBeadCounter(): number {
    let nucleosomeCounter = 0;
    for (let i = 0; i < this._speciesList.length; ++i) {
      if (
        this._speciesList[i] === Constants.BEAD_MARKER_NUCLEOSOME &&
        nucleosomeCounter++ === this._beadCounter
      ) {
        return i;
      }
    }

    throw new Error('There are no nucleosome');
  }
  // endregion
}
