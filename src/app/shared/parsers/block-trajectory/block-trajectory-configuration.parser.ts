/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ReadLine } from '../../readers/read-line';
import { Configuration } from '../../models/configuration.model';
import ConfigurationReaderParser from './configuration-reader.parser';
import ConfigurationParser from '../configuration-parser.abstract';
import { getFileExtension } from '../../utilities/file.utility';
import { ConfigurationOffset } from '../models/configuration-offset.model';

export default class BlockTrajectoryConfigurationParser extends ConfigurationParser {
  public static useThisParser(fileName: string): boolean {
    const ext = getFileExtension(fileName).toLowerCase();
    return ext === 'trj' || ext === '';
  }

  public async parse(
    file: File,
    configOffset: ConfigurationOffset
  ): Promise<Configuration> {
    const configurationParser = new ConfigurationReaderParser();
    for await (const line of new ReadLine(file, {
      seek: configOffset.offset,
      readLength: configOffset.endOffset,
    }).lines()) {
      if (configurationParser.parseLine(line))
        return configurationParser.getResult();
    }

    throw new Error('Invalid offset! Configuration not found!');
  }
}
