/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Configuration } from '../models/configuration.model';
import { ConfigurationOffset } from './models/configuration-offset.model';

export default abstract class ConfigurationParser {
  public static useThisParser: (fileName: string) => boolean;

  public abstract parse(
    file: File,
    configOffset: ConfigurationOffset
  ): Promise<Configuration>;
}
