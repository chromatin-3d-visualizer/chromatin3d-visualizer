/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Subject } from 'rxjs';
import { CachedTrajectory } from './models/cached-trajectory.model';

export default abstract class Parser {
  protected readonly _readBytes = new Subject<number>();
  public readBytesChanged = this._readBytes.asObservable();

  public static useThisParser: (fileName: string) => boolean;

  protected static readonly UPDATE_NOTIFICATION_DURATION = 750; // in ms
  protected static readonly CHUNK_SIZE = 1024 * 1024 * 10; // in bytes

  public async parse(file: File): Promise<CachedTrajectory> {
    return this.parseTrajectoryFile(file);
  }

  protected abstract parseTrajectoryFile(file: File): Promise<CachedTrajectory>;
}
