/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { CachedTrajectory } from '../models/cached-trajectory.model';
import { ReadLine } from '../../readers/read-line';
import PartialTrajectory from '../models/partial-trajectory.model';
import BlockParser from '../block-parser.abstract';
import { getFileExtension } from '../../utilities/file.utility';
import { ConfigurationOffset } from '../models/configuration-offset.model';

export default class AiPredictionParser extends BlockParser {
  public static useThisParser(fileName: string): boolean {
    return getFileExtension(fileName).toLowerCase() === 'csv';
  }

  protected async parseTrajectoryFile(file: File): Promise<CachedTrajectory> {
    let lineOffset = -1;
    let lastUpdate = 0;
    const configurationOffsets: ConfigurationOffset[] = [];
    for await (const line of new ReadLine(file, {
      chunkSize: AiPredictionParser.CHUNK_SIZE,
    }).lines()) {
      ++lineOffset; // +1 coz new line char

      if (line.length > 35) {
        configurationOffsets.push({
          offset: lineOffset,
          endOffset: lineOffset + line.length,
        });
      }

      lineOffset += line.length;
      const currentUpdate = Date.now();
      if (
        Math.abs(currentUpdate - lastUpdate) >
        AiPredictionParser.UPDATE_NOTIFICATION_DURATION
      ) {
        this._readBytes.next(lineOffset);
        lastUpdate = currentUpdate;
      }
    }

    return {
      trajectory: new PartialTrajectory({
        configurationLength: configurationOffsets.length,
        maxBeadLength: 200,
      }),
      file,
      configurations: configurationOffsets,
    };
  }
}
