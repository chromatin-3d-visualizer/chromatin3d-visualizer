/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { XMLParser } from 'fast-xml-parser';
import ConfigurationParser from '../configuration-parser.abstract';
import { getFileExtension } from '../../utilities/file.utility';
import { ConfigurationOffset } from '../models/configuration-offset.model';
import { Configuration } from '../../models/configuration.model';
import { ChunkReader } from '../../readers/chunk-reader';
import { convertXMLConfigurationToConfiguration } from './converter.helper';
import { XMLConfiguration } from './models/xml-configuration.model';
import { XML_ELEMENT_CONFIGURATION } from './constants.static';

export default class XmlConfigurationParser extends ConfigurationParser {
  public static useThisParser(fileName: string): boolean {
    return getFileExtension(fileName.toLowerCase()) === 'xml';
  }

  public async parse(
    file: File,
    configOffset: ConfigurationOffset
  ): Promise<Configuration> {
    const reader = new ChunkReader(file, {
      seek: configOffset.offset,
      chunkSize: configOffset.endOffset - configOffset.offset, // workaround, see note a few lines below
      readLength: configOffset.endOffset,
    });

    const xmlReader = new XMLParser();

    // this is a workaround.
    // in fact, most parsers only allow whole XML documents. Splitting into
    // multiple blocks and parsing one after the other works with the so called
    // ChunkReader in this application, but everything (the parsing) would have
    // to be implemented by itself.
    // for this reason it is assumed that a single configuration as string does
    // not cause an out-of-memory error and an entire configuration is read
    // as/in one chunk.
    return convertXMLConfigurationToConfiguration(
      xmlReader.parse(await reader.readChunkAsString(configOffset.offset))[
        XML_ELEMENT_CONFIGURATION
      ] as XMLConfiguration
    );
  }
}
