/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { XMLParameters } from './models/xml-parameters.model';
import Parameters from '../../models/parameters.model';
import * as Constants from './constants.static';
import { XMLConfiguration } from './models/xml-configuration.model';
import { Configuration } from '../../models/configuration.model';
import { XMLBeads } from './models/xml-beads.model';
import Bead from '../../models/bead.model';
import DNABead from '../../models/dna-bead.model';
import Vector from '../../models/vector.model';
import SoftwareNucleosomeBead from '../../models/sw-nucleosome-bead.model';
import { XMLBead } from './models/xml-bead.model';
import { parseBooleanNumber } from 'src/app/shared/utilities/parser.utility';

export function convertXMLConfigurationToConfiguration(
  config: XMLConfiguration
): Configuration {
  const mcSteps = config.mc_steps ?? 0;
  const overwriteParametersVal = config.overwirte_paramters;
  let overwriteParameters = undefined;
  if (overwriteParametersVal)
    overwriteParameters = convertXMLParametersToConfigurationParameters(
      overwriteParametersVal
    );

  const beads = convertXMLBeadsToBeads(config.beads);

  return new Configuration({
    mcSteps,
    parameters: overwriteParameters,
    beads,
  });
}

export function convertXMLBeadsToBeads(beads: XMLBeads): Bead[] {
  const result: Bead[] = [];
  result.push(
    ...beads.dnabead.map(
      (b) =>
        new DNABead({
          ...getPartialBeadFromXMLBead(b),
          charged: b.charged,
        })
    )
  );

  result.push(
    ...beads.nucleosomebead.map(
      (b) =>
        new SoftwareNucleosomeBead({
          ...getPartialBeadFromXMLBead(b),
          center: new Vector(b.center.x, b.center.y, b.center.z),
          centerOrientationVector: new Vector(
            b.center_orientation_vector.x,
            b.center_orientation_vector.y,
            b.center_orientation_vector.z
          ),
        })
    )
  );

  return result.sort((b1, b2) => b1.id - b2.id);
}

export function convertXMLParametersToConfigurationParameters(
  parameters: XMLParameters
): Partial<Parameters> {
  const paraList = parameters.parameter;
  const chainIsClosedVal = paraList.find(
    (p) => p.name === Constants.PARAMETER_CHAIN_IS_CLOSED
  )?.value;
  let chainIsClosed: boolean | undefined = undefined;
  if (chainIsClosedVal) chainIsClosed = parseBooleanNumber(chainIsClosedVal);

  return {
    numberOfBeads: paraList.find(
      (p) => p.name === Constants.PARAMETER_NUMBER_OF_BEADS
    )?.value,
    radiusDNA: paraList.find((p) => p.name === Constants.PARAMETER_RADIUS_DNA)
      ?.value,
    radiusNucleosome: paraList.find(
      (p) => p.name === Constants.PARAMETER_RADIUS_NUCLEOSOME
    )?.value,
    heightNucleosome: paraList.find(
      (p) => p.name === Constants.PARAMETER_HEIGHT_NUCLEOSOME
    )?.value,
    chainIsClosed: chainIsClosed,
    pbcContainerStartDimension: paraList.find(
      (p) => p.name === Constants.PARAMETER_PBC_START_DIMENSION
    )?.value,
    cubeSizeLJ: paraList.find(
      (p) => p.name === Constants.PARAMETER_PBC_CUBE_SIZE_LJ
    )?.value,
    cubeSizeDNANucIntersection: paraList.find(
      (p) => p.name === Constants.PARAMETER_PBC_CUBE_SIZE_DNA_NUC_ITR
    )?.value,
    cubeSizeEstat: paraList.find(
      (p) => p.name === Constants.PARAMETER_PBC_CUBE_SIZE_ESTAT
    )?.value,
  } as Partial<Parameters>;
}

function getPartialBeadFromXMLBead(b: XMLBead): Partial<Bead> {
  return {
    id: b.id,
    equilibriumSegmentLength: b.equilibrium_segment_length,
    intrinsicTwist: b.intrinsic_twist,
    position: new Vector(b.position.x, b.position.y, b.position.z),
    fVector: new Vector(b.f_vector.x, b.f_vector.y, b.f_vector.z),
    bendVector: new Vector(b.bend_vector.x, b.bend_vector.y, b.bend_vector.z),
    segmentVector: new Vector(
      b.segment_vector.x,
      b.segment_vector.y,
      b.segment_vector.z
    ),
  };
}
