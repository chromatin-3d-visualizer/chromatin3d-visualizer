/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import MetaInfo from '../../models/meta-info.model';
import IParameters from '../../models/parameters.interface';
import Parameters from '../../models/parameters.model';
import IPartialTrajectory from './partial-trajectory.interface';

/**
 * Represents a partial trajectory, without configurations.
 * Larger files cause an out-of-memory error if too many configurations or too
 * large configurations are in the trajectory. For this reason, since the
 * implementation of #187 configurations are no longer stored in the trajectory.
 * Instead, they are parsed at runtime if necessary.
 */
export default class PartialTrajectory implements IPartialTrajectory {
  public readonly globalParameters: Parameters;
  public readonly metaInfo: MetaInfo | undefined;
  public readonly configurationLength: number;
  public readonly maxBeadLength: number;

  public constructor(trajectory: Partial<IPartialTrajectory>) {
    this.globalParameters = new Parameters(
      trajectory.globalParameters ?? ({} as IParameters)
    );

    if (trajectory.metaInfo) this.metaInfo = new MetaInfo(trajectory.metaInfo);

    this.configurationLength =
      trajectory.configurationLength !== undefined
        ? trajectory.configurationLength
        : 0;

    this.maxBeadLength =
      trajectory.maxBeadLength !== undefined ? trajectory.maxBeadLength : 0;
  }
}
