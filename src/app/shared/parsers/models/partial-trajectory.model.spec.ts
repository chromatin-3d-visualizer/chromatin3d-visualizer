/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import PartialTrajectory from './partial-trajectory.model';
import IPartialTrajectory from './partial-trajectory.interface';

describe('Partial Trajectory Model', () => {
  it('should create model with default values', () => {
    const model = new PartialTrajectory({});
    expect(model.globalParameters).toBeDefined();
    expect(model.metaInfo).toBeUndefined();
    expect(model.configurationLength).toEqual(0);
    expect(model.maxBeadLength).toEqual(0);
  });

  it('should create model with values', () => {
    const partial = {
      globalParameters: { numberOfBeads: 5, radiusNucleosome: 10 },
      metaInfo: { date: '2022-08-24', program: 'Lui Tests' },
      configurationLength: 5,
      maxBeadLength: 120,
    } as unknown as Partial<IPartialTrajectory>;

    const model = new PartialTrajectory(partial);
    expect(model.globalParameters.numberOfBeads).toEqual(
      partial.globalParameters!.numberOfBeads
    );
    expect(model.globalParameters.radiusNucleosome).toEqual(
      partial.globalParameters!.radiusNucleosome
    );
    expect(model.metaInfo).toBeDefined();
    expect(model.metaInfo!.date).toEqual(partial.metaInfo!.date);
    expect(model.metaInfo!.program).toEqual(partial.metaInfo!.program);
    expect(model.configurationLength).toEqual(5);
    expect(model.maxBeadLength).toEqual(120);
  });
});
