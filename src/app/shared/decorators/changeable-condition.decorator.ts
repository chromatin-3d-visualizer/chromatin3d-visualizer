/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import 'reflect-metadata';
import { DECORATOR_CUSTOM_CHANGEABLE_CONDITION } from './constants';
import { DecoratorValueCondition } from './models/decorator-value-condition.model';

export function ChangeableCondition<T>(
  must: T,
  targetProp: string
): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => void {
  return function (target: any, propertyKey: string) {
    Reflect.defineMetadata(
      DECORATOR_CUSTOM_CHANGEABLE_CONDITION,
      ((input: any): boolean =>
        input[targetProp] == must) as DecoratorValueCondition,
      target,
      propertyKey
    );
  };
}
