/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'trj-player-controls',
  templateUrl: './player-controls.component.html',
  styleUrls: ['./player-controls.component.scss'],
})
export class PlayerControlsComponent {
  @Input()
  public isFirstEnabled = true;

  @Input()
  public isPreviousEnabled = true;

  @Input()
  public isNextEnabled = true;

  @Input()
  public isLastEnabled = true;

  @Input()
  public isPause = true;

  @Input()
  public isPauseStartVisible = true;

  @Input()
  public disableControlsOnPause = true;

  @Output()
  public first = new EventEmitter<never>();

  @Output()
  public previous = new EventEmitter<never>();

  @Output()
  public pauseStart = new EventEmitter<boolean>();

  @Output()
  public next = new EventEmitter<boolean>();

  @Output()
  public last = new EventEmitter<boolean>();
}
