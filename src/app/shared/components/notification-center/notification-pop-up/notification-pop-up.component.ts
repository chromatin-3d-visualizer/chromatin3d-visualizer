/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'trj-notification-pop-up[title][description]',
  templateUrl: './notification-pop-up.component.html',
  styleUrls: ['./notification-pop-up.component.scss'],
})
export class NotificationPopUpComponent {
  @Input()
  public icon: string;

  @Input()
  public iconColor: string;

  @Input()
  public title!: string;

  @Input()
  public description!: string;

  @Output()
  public closeClicked: EventEmitter<NotificationPopUpComponent> =
    new EventEmitter<NotificationPopUpComponent>();

  constructor() {
    this.icon = 'info';
    this.iconColor = '#389fd6';
  }
}
