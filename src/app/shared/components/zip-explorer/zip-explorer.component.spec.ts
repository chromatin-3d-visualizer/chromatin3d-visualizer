/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import {
  BlobReader,
  BlobWriter,
  Entry,
  ZipWriter,
  configure,
} from '@zip.js/zip.js';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { By } from '@angular/platform-browser';
import { MATERIAL_IMPORTS } from '../../../core/services/__mocks__/imports';
import { ZipExplorerComponent } from './zip-explorer.component';
import createSpy = jasmine.createSpy;
import Spy = jasmine.Spy;
import { ArchiveDirectoryExplorerComponent } from './archive-directory-explorer/archive-directory-explorer.component';
import { PasswordRequestDialogComponent } from './password-request-dialog/password-request-dialog.component';
import { SingleFileLoaderComponent } from './single-file-loader/single-file-loader.component';

async function getAsset(file: string): Promise<Blob> {
  const response = await fetch(file, { method: 'GET' });
  return response.blob();
}

async function generateZipFile(
  zipFileContent: Blob,
  fileCount: number,
  password: string | undefined = undefined
): Promise<Blob> {
  configure({ useWebWorkers: false });
  const blobWriter = new BlobWriter('application/zip');
  const writer = new ZipWriter(blobWriter, { password });

  for (let i = 0; i < fileCount; ++i) {
    await writer.add(`my-file-one_${i}.trj`, new BlobReader(zipFileContent));
  }

  return writer.close(undefined);
}

describe('ZipExplorerComponent', () => {
  let sampleTrj!: Blob;
  let component: ZipExplorerComponent;
  let fixture: ComponentFixture<ZipExplorerComponent>;
  let dialogRef: { close: Spy<() => void> };

  beforeAll(async () => {
    sampleTrj = await getAsset('/mock_data/shortTestTrajectory.trj');
  });

  beforeEach(() => {
    dialogRef = { close: createSpy('close') };

    TestBed.configureTestingModule({
      declarations: [
        SingleFileLoaderComponent,
        ArchiveDirectoryExplorerComponent,
        PasswordRequestDialogComponent,
        ZipExplorerComponent,
      ],
      imports: [...MATERIAL_IMPORTS],
      providers: [
        {
          provide: MatDialogRef,
          useValue: dialogRef,
        },
      ],
    });
  });

  it('should create', async () => {
    TestBed.overrideProvider(MAT_DIALOG_DATA, {
      useValue: {
        file: await generateZipFile(sampleTrj, 2),
        useWebWorkers: false,
      },
    });
    await TestBed.compileComponents();
    fixture = TestBed.createComponent(ZipExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  it('should display the archive explorer', async () => {
    TestBed.overrideProvider(MAT_DIALOG_DATA, {
      useValue: {
        file: await generateZipFile(sampleTrj, 2),
        useWebWorkers: false,
      },
    });
    await TestBed.compileComponents();
    fixture = TestBed.createComponent(ZipExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(
      fixture.debugElement.query(
        By.directive(ArchiveDirectoryExplorerComponent)
      )
    ).toBeDefined();
  });

  it('should display the single file loader', async () => {
    TestBed.overrideProvider(MAT_DIALOG_DATA, {
      useValue: {
        file: await generateZipFile(sampleTrj, 1),
        useWebWorkers: false,
      },
    });
    await TestBed.compileComponents();
    fixture = TestBed.createComponent(ZipExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(
      fixture.debugElement.query(By.directive(SingleFileLoaderComponent))
    ).toBeDefined();
  });

  it('should display the single file loader with password request', async () => {
    TestBed.overrideProvider(MAT_DIALOG_DATA, {
      useValue: {
        file: await generateZipFile(sampleTrj, 1, 'password'),
        useWebWorkers: false,
      },
    });
    await TestBed.compileComponents();
    fixture = TestBed.createComponent(ZipExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(
      fixture.debugElement.query(By.directive(PasswordRequestDialogComponent))
    ).toBeDefined();
  });
});
