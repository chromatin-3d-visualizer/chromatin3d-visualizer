/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchiveDirectoryExplorerComponent } from './archive-directory-explorer.component';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { WorkspaceService as MockWorkspaceService } from '../../../../core/services/__mocks__/workspace.service';
import {
  APPLICATION_IMPORTS,
  MATERIAL_IMPORTS,
} from '../../../../core/services/__mocks__/imports';

describe('ArchiveDirectoryExplorerComponent', () => {
  let component: ArchiveDirectoryExplorerComponent;
  let fixture: ComponentFixture<ArchiveDirectoryExplorerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ArchiveDirectoryExplorerComponent],
      imports: [...MATERIAL_IMPORTS, ...APPLICATION_IMPORTS],
      providers: [
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchiveDirectoryExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
