/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  Component,
  OnChanges,
  Output,
  SimpleChanges,
  EventEmitter,
  Input,
} from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { ExplorerNode } from '../models/explorere-node.model';
import { getFileSizeString } from '../../../utilities/file.utility';

@Component({
  selector: 'trj-archive-directory-explorer',
  templateUrl: './archive-directory-explorer.component.html',
  styleUrls: ['./archive-directory-explorer.component.scss'],
})
export class ArchiveDirectoryExplorerComponent implements OnChanges {
  public treeControl = new NestedTreeControl<ExplorerNode>(
    (node) => node.children
  );
  public dataSource = new MatTreeNestedDataSource<ExplorerNode>();

  @Input()
  public rootNode: ExplorerNode | undefined;

  @Output()
  public selectedNode = new EventEmitter<ExplorerNode | undefined>();

  constructor() {
    this.dataSource.data = [];
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);

    if (changes.rootNode && changes.rootNode.currentValue) {
      this.dataSource.data = [changes.rootNode.currentValue];
      this.treeControl.expand(this.dataSource.data[0]); // expand "root" directory
    }
  }

  hasChild(_: number, node: ExplorerNode): boolean {
    return !!node.children && node.children.length > 0;
  }

  setSelection(entry: ExplorerNode): void {
    const nodes = [...this.dataSource.data];
    while (nodes.length > 0) {
      const node = nodes.splice(0, 1)[0];

      node.isSelected = entry.entry === node.entry;

      if (node.children) nodes.push(...node.children);
    }

    // this.dataSource.data = this.dataSource.data;
    this.selectedNode.emit(entry);
  }

  getFileSize(byteSize: number): string {
    return getFileSizeString(byteSize);
  }
}
