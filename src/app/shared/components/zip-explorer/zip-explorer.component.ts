/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject, OnDestroy } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { BlobReader, Entry, ZipReader } from '@zip.js/zip.js';
import {
  getFileNameFromPath,
  isConfigFile,
  isDraggableFile,
  isReadableFile,
} from '../../utilities/file.utility';
import { PasswordRequestDialogComponent } from './password-request-dialog/password-request-dialog.component';
import { getContent, getContentString } from './utility';
import { ExplorerNode } from './models/explorere-node.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ZipExplorerResult } from './models/zip-explorer-result.model';
import { SubscribeEvents } from '../../../core/classes/subscribe-events';

const ZIP_PATH_SPLITTER = '/';

@Component({
  selector: 'trj-zip-explorer',
  templateUrl: './zip-explorer.component.html',
  styleUrls: ['./zip-explorer.component.scss'],
})
export class ZipExplorerComponent extends SubscribeEvents implements OnDestroy {
  public selectedNode: ExplorerNode | undefined;
  public rootNode: ExplorerNode | undefined;

  private _reader: ZipReader<BlobReader> | undefined;
  private _configFileContent: string | undefined;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    private readonly _data: { file: File; useWebWorkers?: boolean },
    private readonly _dialogRef: MatDialogRef<ZipExplorerComponent>,
    private readonly _snackBar: MatSnackBar,
    private readonly _dialog: MatDialog
  ) {
    super();

    this.init(
      this._data.file,
      this._data.useWebWorkers !== undefined
        ? this._data.useWebWorkers
        : typeof Worker !== 'undefined'
    );
  }

  private static getAllFilesFromEntry(startEntry: ExplorerNode): Entry[] {
    const result: Entry[] = [];
    const entries = [startEntry];
    while (entries.length > 0) {
      const entry = entries.splice(0, 1)[0];
      if (entry.entry.directory && entry.children)
        entries.push(...entry.children);

      if (!entry.entry.directory && entry.entry) result.push(entry.entry);
    }

    return result;
  }

  private static sortEntry(entry: ExplorerNode): ExplorerNode {
    const checkNodes = [entry];
    while (checkNodes.length > 0) {
      const node = checkNodes[0];

      node.children = node.children?.sort((n1, n2) => {
        if (n1.children && !n2.children) return -1;
        else if (!n1.children && n2.children) return 1;

        return n1.name.localeCompare(n2.name);
      });

      checkNodes.splice(0, 1);
      if (node.children) checkNodes.push(...node.children);
    }

    return entry;
  }

  private static getPathNamesFromNode(file: Entry): string[] {
    const paths = file.filename.split(ZIP_PATH_SPLITTER);
    if (paths.length > 0 && paths[paths.length - 1] === '')
      paths.splice(paths.length - 1, 1);

    return paths;
  }

  setSelectedNode(node: ExplorerNode | undefined): void {
    this.selectedNode = node;
  }

  async selectFile(): Promise<void> {
    if (!this.handleEntry) {
      console.error('Handle entry function is empty!');
      return;
    }

    if (!this.selectedNode || !this.selectedNode.entry) return;

    if (!isDraggableFile(this.selectedNode.name)) {
      this._snackBar.open('File extension is not supported!', 'Okay', {
        duration: 7000,
      });
      return;
    }

    await this.handleEntry(this.selectedNode.entry);
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }

  private async init(file: File, useWebWorkers: boolean): Promise<void> {
    this._reader = new ZipReader(new BlobReader(file), { useWebWorkers });

    const rootEntry = await this.getFiles();
    await this.setConfigFile(rootEntry);

    const readableFiles = ZipExplorerComponent.getAllFilesFromEntry(
      rootEntry
    ).filter((e) => isReadableFile(e.filename));
    if (readableFiles.length === 1) {
      await this.handleEntry(readableFiles[0]);
      this.rootNode = undefined;
    } else {
      this.rootNode = rootEntry;
    }
  }

  private async setConfigFile(startEntry: ExplorerNode): Promise<void> {
    const configFiles = ZipExplorerComponent.getAllFilesFromEntry(
      startEntry
    ).filter((e) => isConfigFile(e.filename));

    if (configFiles.length === 1) {
      this._configFileContent = await getContentString(configFiles[0]);
    }
  }

  private async getFiles(): Promise<ExplorerNode> {
    const filesAndDirectories: ExplorerNode = {
      name: 'root',
      entry: { directory: true, encrypted: false } as Entry,
    };

    if (!this._reader) return filesAndDirectories;

    for (const file of await this._reader.getEntries()) {
      const paths = ZipExplorerComponent.getPathNamesFromNode(file);

      let lastNode = filesAndDirectories;
      for (let i = 0; i < paths.length; ++i) {
        const currentPath = [...paths].splice(0, i + 1);
        const fullPath = currentPath.join(ZIP_PATH_SPLITTER);
        const parentNode = lastNode.children?.find((n) => n.name === fullPath);

        if (!parentNode) {
          const newNode = {
            name: fullPath,
            entry: file,
          } as ExplorerNode;
          if (!lastNode.children) lastNode.children = [newNode];
          else lastNode.children.push(newNode);
        } else {
          lastNode = parentNode;
        }
      }
    }

    return ZipExplorerComponent.sortEntry(filesAndDirectories);
  }

  private async handleEntry(entry: Entry): Promise<void> {
    if (!entry.encrypted) {
      await this.closeDialog({
        trjFile: new File(
          [await getContent(entry)],
          getFileNameFromPath(entry.filename)
        ),
        configContent: this._configFileContent,
      } as ZipExplorerResult);
      return;
    }

    const dialogRef = this._dialog.open(PasswordRequestDialogComponent, {
      width: '60%',
      disableClose: true,
      data: {
        entry: entry,
        name: getFileNameFromPath(entry.filename),
      },
    });

    this.subscribes.push(
      dialogRef.afterClosed().subscribe((data) => {
        if (data === undefined) return;

        this.closeDialog({
          trjFile: data,
          configContent: this._configFileContent,
        } as ZipExplorerResult);
      })
    );
  }

  private async closeDialog(closeResult?: any): Promise<void> {
    if (this._reader) await this._reader.close();

    this._dialogRef.close(closeResult);
  }
}
