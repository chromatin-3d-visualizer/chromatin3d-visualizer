/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatTreeModule } from '@angular/material/tree';
import { ZipExplorerComponent } from './zip-explorer.component';
import { ArchiveDirectoryExplorerComponent } from './archive-directory-explorer/archive-directory-explorer.component';
import { PasswordRequestDialogComponent } from './password-request-dialog/password-request-dialog.component';
import { SingleFileLoaderComponent } from './single-file-loader/single-file-loader.component';

@NgModule({
  declarations: [
    ZipExplorerComponent,
    ArchiveDirectoryExplorerComponent,
    PasswordRequestDialogComponent,
    SingleFileLoaderComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatProgressBarModule,
    MatTreeModule,
    ReactiveFormsModule,
  ],
  exports: [ZipExplorerComponent],
})
export class ZipExplorerModule {}
