/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Inject } from '@angular/core';
import { FormControl, ValidationErrors, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Entry } from '@zip.js/zip.js';
import { getContent } from '../utility';

const ZIP_JS_INVALID_PASSWORD_MESSAGE = 'Invalid password';

@Component({
  selector: 'trj-password-request-dialog',
  templateUrl: './password-request-dialog.component.html',
  styleUrls: ['./password-request-dialog.component.scss'],
})
export class PasswordRequestDialogComponent {
  public showPassword = false;
  public reading = false;

  public passwordFormControl = new FormControl<string>('', [
    Validators.required,
  ]);

  constructor(
    @Inject(MAT_DIALOG_DATA)
    private readonly _data: { entry: Entry; name: string },
    private readonly _dialogRef: MatDialogRef<PasswordRequestDialogComponent>
  ) {}

  async checkPassword(password: string): Promise<void> {
    this.reading = true;
    try {
      const content = await getContent(this._data.entry, password);
      this._dialogRef.close(new File([content], this._data.name));
    } catch (e: any) {
      if (e.message === ZIP_JS_INVALID_PASSWORD_MESSAGE) {
        this.passwordFormControl.setErrors({
          incorrect: true,
        } as ValidationErrors);
        return;
      }
      console.warn(e);
    } finally {
      this.reading = false;
    }
  }
}
