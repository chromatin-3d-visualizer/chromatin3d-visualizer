/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MATERIAL_IMPORTS } from '../../../core/services/__mocks__/imports';
import { SliderComponent } from './slider.component';
import { By } from '@angular/platform-browser';
import { MatSliderThumb } from '@angular/material/slider';
import createSpy = jasmine.createSpy;

describe('SliderComponent', () => {
  let component: SliderComponent;
  let fixture: ComponentFixture<SliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SliderComponent],
      imports: [...MATERIAL_IMPORTS],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should trigger selection emitter', () => {
    const spyFunc = createSpy(
      'Value Changer',
      component.changeValue
    ).and.callThrough();
    const spyEmitter = createSpy('valueChange', component.valueChange.emit);
    component.value = 5;
    component.maximum = 10;
    // @ts-ignore
    component.changeValue = spyFunc;
    // @ts-ignore
    component.valueChange.emit = spyEmitter;
    fixture.detectChanges();

    const slider = fixture.debugElement.query(By.directive(MatSliderThumb));
    expect(slider).toBeTruthy();

    slider.triggerEventHandler('valueChange', null);
    expect(spyFunc.calls.count()).toEqual(1);
    expect(spyEmitter.calls.count()).toEqual(0);

    slider.triggerEventHandler('valueChange', 5);
    expect(spyFunc.calls.count()).toEqual(2);
    expect(spyEmitter.calls.argsFor(0)).toEqual([5]);
  });
});
