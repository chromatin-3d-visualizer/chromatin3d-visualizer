/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'trj-slider[value][maximum]',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
})
export class SliderComponent {
  @Input()
  public value!: number;
  @Input()
  public maximum!: number;
  @Input()
  public minimum = 0;
  @Input()
  public step = 1;

  @Input()
  public minWidthInputField = 75; // in pixel

  @Output()
  public valueChange = new EventEmitter<number>();

  @Input()
  public isSmall = false;

  changeValue(val: number | null): void {
    if (val === null) return;

    this.valueChange.emit(val);
  }
}
