/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DockableAreaHostComponent } from './dockable-area-host.component';
import { WorkspaceService } from '../../../../core/services/workspace.service';
import { WorkspaceService as MockWorkspaceService } from '../../../../core/services/__mocks__/workspace.service';
import {
  APPLICATION_IMPORTS,
  MATERIAL_IMPORTS,
  WORKSPACE_IMPORTS,
} from '../../../../core/services/__mocks__/imports';
import { RouterTestingModule } from '@angular/router/testing';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader';
import { layout } from '../../../../modules/dockable-workspace/viewer/__mocks__/layout';

describe('DockableAreaHostComponent', () => {
  let component: DockableAreaHostComponent;
  let fixture: ComponentFixture<DockableAreaHostComponent>;

  beforeEach(async () => {
    spyOn(FontLoader.prototype, 'load');
    await TestBed.configureTestingModule({
      declarations: [DockableAreaHostComponent],
      imports: [
        ...MATERIAL_IMPORTS,
        ...WORKSPACE_IMPORTS,
        ...APPLICATION_IMPORTS,
        RouterTestingModule,
      ],
      providers: [
        {
          provide: WorkspaceService,
          useClass: MockWorkspaceService,
        },
      ],
    }).compileComponents();
    localStorage.clear();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DockableAreaHostComponent);
    component = fixture.componentInstance;
    component.fallbackLayout = layout;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
