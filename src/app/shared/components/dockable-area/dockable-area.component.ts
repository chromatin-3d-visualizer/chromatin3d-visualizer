/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Input, ViewChild } from '@angular/core';
import { LayoutConfig, ResolvedLayoutConfig } from 'golden-layout';
import { DockableAreaHostComponent } from './dockable-area-host/dockable-area-host.component';

@Component({
  selector: 'trj-dockable-area[fallbackLayout]',
  templateUrl: './dockable-area.component.html',
  styleUrls: ['./dockable-area.component.scss'],
})
export class DockableAreaComponent {
  @Input()
  public layout: LayoutConfig | undefined;

  @Input()
  public fallbackLayout!: LayoutConfig;

  @ViewChild(DockableAreaHostComponent)
  private _host: DockableAreaHostComponent | undefined;

  setSize(newWidth: number, newHeight: number): void {
    this._host?.setSize(newWidth, newHeight);
  }

  setLayout(layout: LayoutConfig): void {
    this._host?.setLayout(layout);
  }

  getLayout(): ResolvedLayoutConfig {
    return this._host?.getConfig() ?? ({} as ResolvedLayoutConfig);
  }
}
