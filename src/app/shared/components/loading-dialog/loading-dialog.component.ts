/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { Component, Input, OnDestroy } from '@angular/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { SubscribeEvents } from '../../../core/classes/subscribe-events';

@Component({
  selector: 'trj-loading-dialog',
  templateUrl: './loading-dialog.component.html',
  styleUrls: ['./loading-dialog.component.scss'],
})
export class LoadingDialogComponent
  extends SubscribeEvents
  implements OnDestroy
{
  @Input()
  public title = '';
  @Input()
  public textLeft = '';
  @Input()
  public textRight = '';

  @Input()
  public progressType: ProgressBarMode = 'indeterminate';

  @Input()
  public progressValue = 0;

  constructor() {
    super();
  }

  ngOnDestroy(): void {
    this.unsubscribeListeners();
  }
}
