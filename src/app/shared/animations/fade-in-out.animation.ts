/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  animate,
  AnimationMetadata,
  style,
  transition,
} from '@angular/animations';

const FADE_IN_OUT_ANIMATION_DURATION = 250; // in ms
const FADE_ANIMATION_STRING = `${FADE_IN_OUT_ANIMATION_DURATION}ms ease-in-out`;

export const FADE_ANIMATION: AnimationMetadata[] = [
  transition('void => to-fade-in', [
    style({ opacity: 0, filter: 'blur(10px)' }),
    animate(FADE_ANIMATION_STRING, style({ opacity: 1, filter: 'blur(0px)' })),
  ]),

  transition(':leave', [
    style({ opacity: 1, filter: 'blur(0px)' }),
    animate(FADE_ANIMATION_STRING, style({ opacity: 0, filter: 'blur(10px)' })),
  ]),
];
