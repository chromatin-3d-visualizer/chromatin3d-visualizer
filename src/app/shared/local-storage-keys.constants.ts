/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

export const LOCAL_STORAGE_LAYOUT = 'layout';
export const LOCAL_STORAGE_CAMERA = 'camera';
export const LOCAL_STORAGE_DEBUG = 'debug';
export const LOCAL_STORAGE_DEFAULT_COLORS = 'defaultColors';
export const LOCAL_STORAGE_SETTINGS = 'settings';
export const LOCAL_STORAGE_SETTINGS_PBC = 'settingsPBC';
export const LOCAL_STORAGE_SETTINGS_TOOL_DISTANCE_MEASUREMENT =
  'settingsToolDistanceMeasurement';
export const LOCAL_STORAGE_LAST_VISIT = 'lastVisit';
export const LOCAL_STORAGE_FIRST_VISIT = 'firstVisit';
export const LOCAL_STORAGE_DEFINED_CAMERA_POSITIONS = 'definedCameraPositions';
export const LOCAL_STORAGE_CAMERA_ANIMATIONS = 'cameraAnimations';
export const LOCAL_STORAGE_SETTINGS_ANIMATION = 'isAnimationEnabled';

export const LOCAL_STORAGE_DEBUG_CACHED_FILE_XML = 'debugCachedFileXML';
export const LOCAL_STORAGE_DEBUG_CACHED_FILE_TRJ = 'debugCachedFileTRJ';
export const LOCAL_STORAGE_DEBUG_CACHED_SESSION = 'debugCachedSession';
