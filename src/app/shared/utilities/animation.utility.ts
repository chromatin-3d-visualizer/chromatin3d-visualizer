/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { DefinedCamera } from '../../modules/dockable-workspace/tools/tool-defined-camera-positions/models/defined-camera.model';
import { Animation } from '../../core/services/animation/models/animation.model';

export function getUsedDefinedCameraPoints(
  definedCameraPoints: DefinedCamera[],
  animations: Animation[],
  selectedAnimation: Animation | null = null
): DefinedCamera[] {
  const filteredCameraPoints = [...definedCameraPoints];
  let cameraIndex = 0;
  while (cameraIndex < filteredCameraPoints.length) {
    const camera = filteredCameraPoints[cameraIndex];
    const usedKeyframe = getAnimationsFromDefinedCamera(camera.id, animations);
    if (usedKeyframe.length === 1 && usedKeyframe[0] === selectedAnimation) {
      filteredCameraPoints.splice(cameraIndex, 1);
    } else {
      ++cameraIndex;
    }
  }

  return filteredCameraPoints;
}

export function getAnimationsFromDefinedCamera(
  cameraID: string,
  animations: Animation[]
): Animation[] {
  return animations.filter((a) =>
    a.keyframes.value.find((k) => k.cameraID === cameraID)
  );
}
