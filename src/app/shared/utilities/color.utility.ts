/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import { RGBAColor } from '../models/rgba-color.model';
import { RGBColor } from '../models/rgb-color.model';
import {
  REGEX_PATTERN_IS_MODERN_RGB_COLOR,
  REGEX_PATTERN_IS_RGB_COLOR,
  REGEX_PATTERN_IS_RGBA_COLOR,
  REGEX_PATTERN_RGBA_SPLITTER,
} from './color.constants.no-sonar';

export function colorToArgb(color: string): RGBAColor {
  if (
    REGEX_PATTERN_IS_RGBA_COLOR.test(color) ||
    REGEX_PATTERN_IS_MODERN_RGB_COLOR.test(color) ||
    REGEX_PATTERN_IS_RGB_COLOR.test(color)
  )
    return rgbaStringToArgb(color);
  else return hexToArgb(color);
}

export function rgbaStringToArgb(rgba: string): RGBAColor {
  const numberRegexResult = rgba.match(REGEX_PATTERN_RGBA_SPLITTER);
  if (numberRegexResult) {
    const numbers = [...numberRegexResult];
    if (numbers.length === 4)
      return {
        r: parseInt(numbers[0]),
        g: parseInt(numbers[1]),
        b: parseInt(numbers[2]),
        a: parseFloat(numbers[3]),
      };
    else if (numbers.length === 3)
      return {
        r: parseInt(numbers[0]),
        g: parseInt(numbers[1]),
        b: parseInt(numbers[2]),
        a: 1,
      };
  }

  return { r: 0, g: 0, b: 0, a: 1 };
}

export function hexToArgb(hex: string): RGBAColor {
  if (hex.startsWith('#')) hex = hex.substring(1);

  if (hex.length === 6) {
    return {
      a: 1,
      r: parseInt(hex.substring(0, 2), 16),
      g: parseInt(hex.substring(2, 4), 16),
      b: parseInt(hex.substring(4, 6), 16),
    };
  } else if (hex.length === 8) {
    return {
      r: parseInt(hex.substring(0, 2), 16),
      g: parseInt(hex.substring(2, 4), 16),
      b: parseInt(hex.substring(4, 6), 16),
      a: parseInt(hex.substring(6, 8), 16) / 255,
    };
  } else if (hex.length === 4) {
    return {
      r: parseInt(`${hex[0]}${hex[0]}`, 16),
      g: parseInt(`${hex[1]}${hex[1]}`, 16),
      b: parseInt(`${hex[2]}${hex[2]}`, 16),
      a: parseInt(`${hex[3]}${hex[3]}`, 16) / 255,
    };
  } else {
    return {
      a: 1,
      r: parseInt(`${hex[0]}${hex[0]}`, 16),
      g: parseInt(`${hex[1]}${hex[1]}`, 16),
      b: parseInt(`${hex[2]}${hex[2]}`, 16),
    };
  }
}

export function componentToHex(c: number): string {
  return c.toString(16).padStart(2, '0');
}

export function rgbToHex({
  r,
  g,
  b,
}: {
  r: number;
  g: number;
  b: number;
}): string {
  return `#${componentToHex(r)}${componentToHex(g)}${componentToHex(b)}`;
}

export function argbToHex({ a, r, g, b }: RGBColor): string {
  return `#${componentToHex(
    Math.ceil((a !== undefined ? a : 1) * 255)
  )}${componentToHex(r)}${componentToHex(g)}${componentToHex(b)}`;
}

export function aOptionalRGBToHex({ a, r, g, b }: RGBColor): string {
  const firstComponent = a !== undefined ? componentToHex(a * 255) : '';
  return `#${firstComponent}${componentToHex(r)}${componentToHex(
    g
  )}${componentToHex(b)}`;
}

export function aOptionalRGBToHexCSSFriendly({ a, r, g, b }: RGBColor): string {
  const firstComponent = a !== undefined ? a : 1;
  return `rgba(${r}, ${g}, ${b}, ${firstComponent})`;
}
