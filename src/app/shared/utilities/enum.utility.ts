/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

// eslint-disable-next-line
export function getValueOfEnumKey<T>(_enum: any, value: any): T | undefined {
  // don't use if(!value) because 0/false will return a true but it is legit
  if (value === undefined || value === null || !_enum) return undefined;

  const foundObj = Object.values(_enum).find(
    (c: any) => c.toString() === value.toString() // eslint-disable-line
  );
  if (foundObj !== undefined) return foundObj as T;

  return undefined;
}
