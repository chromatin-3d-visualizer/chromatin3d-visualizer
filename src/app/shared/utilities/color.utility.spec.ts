/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  aOptionalRGBToHex,
  aOptionalRGBToHexCSSFriendly,
  argbToHex,
  colorToArgb,
  hexToArgb,
  rgbaStringToArgb,
} from './color.utility';

describe('Color Utility', () => {
  it('should convert color string (hex) to color', () => {
    expect(colorToArgb('#ff00ff')).toEqual({ r: 255, g: 0, b: 255, a: 1 });
  });

  it('should convert color string (rgba) to color', () => {
    expect(colorToArgb('rgba(255, 0, 255, 1.0)')).toEqual({
      r: 255,
      g: 0,
      b: 255,
      a: 1,
    });
  });

  it('should convert rgba string to color', () => {
    expect(rgbaStringToArgb('rgba(255, 0, 255, 0.5)')).toEqual({
      r: 255,
      g: 0,
      b: 255,
      a: 0.5,
    });
  });

  it('should convert rgb string to color', () => {
    expect(rgbaStringToArgb('rgba(255, 0, 255)')).toEqual({
      r: 255,
      g: 0,
      b: 255,
      a: 1,
    });
  });

  it('should return a default color on invalid input', () => {
    expect(rgbaStringToArgb('INVALID')).toEqual({
      r: 0,
      g: 0,
      b: 0,
      a: 1,
    });
  });

  it('should return a default color on invalid input #2', () => {
    expect(rgbaStringToArgb('255, 255')).toEqual({
      r: 0,
      g: 0,
      b: 0,
      a: 1,
    });
  });

  it('should convert hex string (without #) to color', () => {
    expect(hexToArgb('00ee00')).toEqual({ r: 0, g: 0xee, b: 0, a: 1.0 });
  });

  it('should convert hex string (with #) to color', () => {
    expect(hexToArgb('#00ee00')).toEqual({ r: 0, g: 0xee, b: 0, a: 1.0 });
  });

  it('should convert hex string (6 chars) to color', () => {
    expect(hexToArgb('#00ee00')).toEqual({ r: 0, g: 0xee, b: 0, a: 1.0 });
  });

  it('should convert hex string (8 chars) (with alpha) to color', () => {
    expect(hexToArgb('#00ee0005')).toEqual({ r: 0, g: 0xee, b: 0, a: 5 / 255 });
  });

  it('should convert hex string (3 chars) (without alpha) to color', () => {
    expect(hexToArgb('#0e0')).toEqual({ r: 0, g: 0xee, b: 0, a: 1.0 });
  });

  it('should convert hex string (4 chars) (with alpha) to color', () => {
    expect(hexToArgb('#0e03')).toEqual({ r: 0, g: 0xee, b: 0, a: 0x33 / 255 });
  });

  it('should convert color to hex with alpha', () => {
    expect(
      argbToHex({ r: 255, g: 0xee, b: 0x20, a: 0.5 }).toLowerCase()
    ).toEqual(`#80FFEE20`.toLowerCase());
  });

  it('should convert color to hex with alpha without that alpha has a value', () => {
    expect(argbToHex({ r: 255, g: 0xee, b: 0x20 }).toLowerCase()).toEqual(
      `#FFFFEE20`.toLowerCase()
    );
  });

  it('should convert color to hex without alpha', () => {
    expect(
      aOptionalRGBToHex({
        r: 255,
        g: 0xee,
        b: 0x20,
      }).toLowerCase()
    ).toEqual(`#FFEE20`.toLowerCase());
  });

  it('should convert color to hex with alpha #2', () => {
    expect(
      aOptionalRGBToHex({ r: 255, g: 0xee, b: 0x20, a: 1.0 }).toLowerCase()
    ).toEqual(`#FFFFEE20`.toLowerCase());
  });

  it('should convert color to a css friendly string (without alpha input)', () => {
    expect(aOptionalRGBToHexCSSFriendly({ r: 255, g: 20, b: 50 })).toEqual(
      'rgba(255, 20, 50, 1)'
    );
  });

  it('should convert color to a css friendly string (with alpha input)', () => {
    expect(
      aOptionalRGBToHexCSSFriendly({ r: 255, g: 20, b: 50, a: 0.36 })
    ).toEqual('rgba(255, 20, 50, 0.36)');
  });
});
