/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import {
  ARCHIVE_EXTENSIONS,
  DRAGGABLE_EXTENSIONS,
  READABLE_EXTENSIONS,
} from '../supported-files.constants';

const BYTE_SIZE_LIST = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
const BYTE_LOG = Math.log(1024);
const BYTE_FRACTION_DIGITS = 2;

export const CONFIGURATION_FILE_NAME = 'config.json';

export function isReadableFile(fileName: string): boolean {
  const fileExt = getFileExtension(fileName).toLowerCase();
  return READABLE_EXTENSIONS.find((ext) => ext === fileExt) !== undefined;
}

export function isDraggableFile(fileName: string): boolean {
  const fileExt = getFileExtension(fileName).toLowerCase();
  return DRAGGABLE_EXTENSIONS.find((ext) => ext === fileExt) !== undefined;
}

export function isArchive(fileName: string): boolean {
  const fileExt = getFileExtension(fileName).toLowerCase();
  return ARCHIVE_EXTENSIONS.find((ext) => ext === fileExt) !== undefined;
}

export function isConfigFile(fileName: string): boolean {
  const realFileName = getFileNameFromPath(fileName);
  return realFileName === CONFIGURATION_FILE_NAME;
}

export function getFileNameWithoutExtension(fileName: string): string {
  fileName = getFileNameFromPath(fileName);
  const lastDot = fileName.lastIndexOf('.');
  if (lastDot < 0) return fileName;

  return fileName.substring(0, lastDot);
}

export function getFileNameFromPath(fileNameWithPath: string): string {
  const lastSlash = fileNameWithPath.lastIndexOf('/');
  const lastSlash2 = fileNameWithPath.lastIndexOf('\\');
  if (
    (lastSlash < 0 && lastSlash2 < 0) ||
    lastSlash === fileNameWithPath.length - 1 ||
    lastSlash2 === fileNameWithPath.length - 1
  )
    return fileNameWithPath;

  if (lastSlash2 > lastSlash) return fileNameWithPath.substring(lastSlash2 + 1);

  return fileNameWithPath.substring(lastSlash + 1);
}

export function getFileExtension(fileName: string): string {
  const possibleExts = fileName.split('.');
  if (possibleExts.length === 1) return '';
  return possibleExts.pop() ?? '';
}

export function getFileSizeString(byteSize: number): string {
  let index = 0;
  if (byteSize > 0) index = Math.trunc(Math.log(byteSize) / BYTE_LOG);

  if (index >= BYTE_SIZE_LIST.length) index = BYTE_SIZE_LIST.length - 1;

  const formattedSize = byteSize / Math.pow(1024, index);
  const formattedSizeStr =
    index === 0
      ? formattedSize.toFixed(0)
      : formattedSize.toFixed(BYTE_FRACTION_DIGITS);
  return `${formattedSizeStr} ${BYTE_SIZE_LIST[index]}`;
}
