/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

export function findIndexAtPos<T>(
  models: T[],
  predicate: (value: T, index: number, obj: T[]) => boolean,
  startPosition: number
): number {
  const index = models.slice(startPosition).findIndex(predicate);
  return index === -1 ? -1 : index + startPosition;
}

export function convert2DArrayTo1D<T>(array: T[][]): T[] {
  return ([] as T[]).concat(...array);
}

export function equalsArray<T>(array: T[], array2: T[]): boolean {
  if (array.length !== array2.length) return false;

  for (let i = 0; i < array.length; ++i) {
    if (array[i] !== array2[i]) return false;
  }

  return true;
}
