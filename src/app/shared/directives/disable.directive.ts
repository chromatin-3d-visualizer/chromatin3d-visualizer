/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

/**
 * Important!
 * The class has been copied from an article on the subject: https://imdurgeshpal.medium.com/enable-disable-all-dom-elements-inside-div-in-angular-using-directive-56ca4602ab7b
 * @source https://imdurgeshpal.medium.com/enable-disable-all-dom-elements-inside-div-in-angular-using-directive-56ca4602ab7b
 */

import {
  AfterViewInit,
  Directive,
  ElementRef,
  Input,
  OnChanges,
  Renderer2,
} from '@angular/core';

const DISABLED = 'disabled';
const APP_DISABLED = 'app-disabled';
const APP_DISABLE = 'app-disable';
const TAB_INDEX = 'tabindex';
const TAG_ANCHOR = 'a';

@Directive({
  selector: '[appDisable]',
})
export class DisableDirective implements OnChanges, AfterViewInit {
  @Input()
  public appDisable = true;

  public constructor(
    private eleRef: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {}

  private static removeDisableAttribute(ele: HTMLElement): void {
    if (ele.getAttribute(DISABLED) !== '') ele.removeAttribute(DISABLED);

    ele.removeAttribute(APP_DISABLED);
    if (ele.tagName.toLowerCase() === TAG_ANCHOR)
      ele.removeAttribute(TAB_INDEX);
  }

  public ngOnChanges(): void {
    this.disableElement(this.eleRef.nativeElement);
  }

  public ngAfterViewInit(): void {
    this.disableElement(this.eleRef.nativeElement);
  }

  private disableElement(element: HTMLElement): void {
    const elements = [element];

    while (elements.length > 0) {
      const ele = elements.splice(0, 1)[0];
      if (ele.tagName.toLowerCase() === 'mat-slider') {
        // in angular > 14 there is strange bug with "input" child, so special case...
        this.setDisableAttribute(ele);
        continue;
      }

      if (ele != element && ele.hasAttribute(`ng-reflect-${APP_DISABLE}`))
        continue;

      if (this.appDisable && !ele.hasAttribute(DISABLED)) {
        this.setDisableAttribute(ele);
      } else if (!this.appDisable && ele.hasAttribute(APP_DISABLED)) {
        DisableDirective.removeDisableAttribute(ele);
      }

      if (ele.children)
        elements.push(...Array.prototype.slice.call(ele.children));
    }
  }

  private setDisableAttribute(ele: HTMLElement): void {
    this.renderer.setAttribute(ele, APP_DISABLED, '');
    this.renderer.setAttribute(ele, DISABLED, 'true');

    // disabling anchor tab keyboard event
    if (ele.tagName.toLowerCase() === TAG_ANCHOR)
      this.renderer.setAttribute(ele, TAB_INDEX, '-1');

    this.setTopDisableClassName(ele);
  }

  private setTopDisableClassName(ele: HTMLElement): void {
    let parent: HTMLElement | null = ele.parentElement;
    let parentHasDisabled = false;
    while (parent != null) {
      if (parent.hasAttribute(APP_DISABLED)) {
        parentHasDisabled = true;
        break;
      }

      parent = parent.parentElement;
    }

    if (parentHasDisabled) {
      this.renderer.removeClass(ele, 'top');
    } else if (ele.tagName.toLowerCase() !== 'mat-slider') {
      this.renderer.addClass(ele, 'top');
    }
  }
}
