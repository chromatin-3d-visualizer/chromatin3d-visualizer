/*****************************************************************************************************
 *
 * Copyright (c) 2022.  Hochschule Stralsund – University of Applied Sciences
 * The following Code is licensed under the GNU General Public License (GPL) v3.0.
 * https://gitlab.com/wedemann-group/chromatin3d-visualizer/-/blob/master/LICENSE
 *
 * Corresponding Person:
 *     Prof. Gero Wedemann <gero.wedemann@hochschule-stralsund.de>
 *
 * Contributors:
 *     Leif-Kristof Schultz <contact@lui-studio.net>
 *     Frederic Bauer
 *     Janne Wernecken
 *
 ****************************************************************************************************/

import BasePairObject from '../../modules/dockable-workspace/viewer/beads/base-pair-object.model';
import { Vector3 } from 'three';
import { IDoubleStrand } from '../../modules/dockable-workspace/viewer/beads/double-strand.interface';
import InstancedObjectBuilder from '../../modules/dockable-workspace/viewer/beads/instanced-object.builder';
import { OneTimeBuilder } from '../models/one-time-builder.model';

export function getBasePairs(
  multipleUseBuilders: InstancedObjectBuilder<any, any>[],
  oneTimeUseBuilders: OneTimeBuilder[]
): BasePairObject[] {
  const baseBindingBeads: BasePairObject[] = [];
  let lastPoints: Vector3[] = [];
  let builderIndex = 0;
  for (const builder of [...multipleUseBuilders, ...oneTimeUseBuilders]) {
    if (
      (builder as unknown as IDoubleStrand).getDoubleStrandPoints === undefined
    )
      continue;

    const test = builder as unknown as IDoubleStrand;
    if (builderIndex % 2 === 0) {
      lastPoints = test.getDoubleStrandPoints();
    } else {
      const counterPoints = test.getDoubleStrandPoints();
      if (counterPoints.length !== lastPoints.length)
        throw new Error('Invalid length of points and counter points!');

      for (let i = 0; i < counterPoints.length; ++i)
        baseBindingBeads.push(
          new BasePairObject(
            baseBindingBeads.length,
            lastPoints[i],
            counterPoints[i]
          )
        );
    }

    ++builderIndex;
  }

  return baseBindingBeads;
}
